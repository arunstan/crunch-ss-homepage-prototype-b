! function() {
    window.onerror = function(n, i, e, s, t) {
        var c, r = {
            message: n,
            script: i,
            line: e,
            column: s,
            stack: t && t.stack
        };
        try {
            c = JSON.stringify(r)
        } catch (a) {
            c = JSON.stringify({
                message: n
            })
        } finally {
            ga("send", "exception", {
                exDescription: c
            })
        }
    }
}();
! function(e, t) {
    "object" == typeof module && "object" == typeof module.exports ? module.exports = e.document ? t(e, !0) : function(e) {
        if (!e.document) throw new Error("jQuery requires a window with a document");
        return t(e)
    } : t(e)
}("undefined" != typeof window ? window : this, function(e, t) {
    function n(e) {
        var t = e.length,
            n = ie.type(e);
        return "function" === n || ie.isWindow(e) ? !1 : 1 === e.nodeType && t ? !0 : "array" === n || 0 === t || "number" == typeof t && t > 0 && t - 1 in e
    }

    function r(e, t, n) {
        if (ie.isFunction(t)) return ie.grep(e, function(e, r) {
            return !!t.call(e, r, e) !== n
        });
        if (t.nodeType) return ie.grep(e, function(e) {
            return e === t !== n
        });
        if ("string" == typeof t) {
            if (fe.test(t)) return ie.filter(t, e, n);
            t = ie.filter(t, e)
        }
        return ie.grep(e, function(e) {
            return ie.inArray(e, t) >= 0 !== n
        })
    }

    function i(e, t) {
        do e = e[t]; while (e && 1 !== e.nodeType);
        return e
    }

    function o(e) {
        var t = xe[e] = {};
        return ie.each(e.match(be) || [], function(e, n) {
            t[n] = !0
        }), t
    }

    function a() {
        he.addEventListener ? (he.removeEventListener("DOMContentLoaded", s, !1), e.removeEventListener("load", s, !1)) : (he.detachEvent("onreadystatechange", s), e.detachEvent("onload", s))
    }

    function s() {
        (he.addEventListener || "load" === event.type || "complete" === he.readyState) && (a(), ie.ready())
    }

    function u(e, t, n) {
        if (void 0 === n && 1 === e.nodeType) {
            var r = "data-" + t.replace(Ee, "-$1").toLowerCase();
            if (n = e.getAttribute(r), "string" == typeof n) {
                try {
                    n = "true" === n ? !0 : "false" === n ? !1 : "null" === n ? null : +n + "" === n ? +n : Ne.test(n) ? ie.parseJSON(n) : n
                } catch (i) {}
                ie.data(e, t, n)
            } else n = void 0
        }
        return n
    }

    function l(e) {
        var t;
        for (t in e)
            if (("data" !== t || !ie.isEmptyObject(e[t])) && "toJSON" !== t) return !1;
        return !0
    }

    function c(e, t, n, r) {
        if (ie.acceptData(e)) {
            var i, o, a = ie.expando,
                s = e.nodeType,
                u = s ? ie.cache : e,
                l = s ? e[a] : e[a] && a;
            if (l && u[l] && (r || u[l].data) || void 0 !== n || "string" != typeof t) return l || (l = s ? e[a] = J.pop() || ie.guid++ : a), u[l] || (u[l] = s ? {} : {
                toJSON: ie.noop
            }), ("object" == typeof t || "function" == typeof t) && (r ? u[l] = ie.extend(u[l], t) : u[l].data = ie.extend(u[l].data, t)), o = u[l], r || (o.data || (o.data = {}), o = o.data), void 0 !== n && (o[ie.camelCase(t)] = n), "string" == typeof t ? (i = o[t], null == i && (i = o[ie.camelCase(t)])) : i = o, i
        }
    }

    function d(e, t, n) {
        if (ie.acceptData(e)) {
            var r, i, o = e.nodeType,
                a = o ? ie.cache : e,
                s = o ? e[ie.expando] : ie.expando;
            if (a[s]) {
                if (t && (r = n ? a[s] : a[s].data)) {
                    ie.isArray(t) ? t = t.concat(ie.map(t, ie.camelCase)) : t in r ? t = [t] : (t = ie.camelCase(t), t = t in r ? [t] : t.split(" ")), i = t.length;
                    for (; i--;) delete r[t[i]];
                    if (n ? !l(r) : !ie.isEmptyObject(r)) return
                }(n || (delete a[s].data, l(a[s]))) && (o ? ie.cleanData([e], !0) : ne.deleteExpando || a != a.window ? delete a[s] : a[s] = null)
            }
        }
    }

    function f() {
        return !0
    }

    function p() {
        return !1
    }

    function h() {
        try {
            return he.activeElement
        } catch (e) {}
    }

    function m(e) {
        var t = Fe.split("|"),
            n = e.createDocumentFragment();
        if (n.createElement)
            for (; t.length;) n.createElement(t.pop());
        return n
    }

    function g(e, t) {
        var n, r, i = 0,
            o = typeof e.getElementsByTagName !== Ce ? e.getElementsByTagName(t || "*") : typeof e.querySelectorAll !== Ce ? e.querySelectorAll(t || "*") : void 0;
        if (!o)
            for (o = [], n = e.childNodes || e; null != (r = n[i]); i++) !t || ie.nodeName(r, t) ? o.push(r) : ie.merge(o, g(r, t));
        return void 0 === t || t && ie.nodeName(e, t) ? ie.merge([e], o) : o
    }

    function v(e) {
        je.test(e.type) && (e.defaultChecked = e.checked)
    }

    function y(e, t) {
        return ie.nodeName(e, "table") && ie.nodeName(11 !== t.nodeType ? t : t.firstChild, "tr") ? e.getElementsByTagName("tbody")[0] || e.appendChild(e.ownerDocument.createElement("tbody")) : e
    }

    function b(e) {
        return e.type = (null !== ie.find.attr(e, "type")) + "/" + e.type, e
    }

    function x(e) {
        var t = Ve.exec(e.type);
        return t ? e.type = t[1] : e.removeAttribute("type"), e
    }

    function w(e, t) {
        for (var n, r = 0; null != (n = e[r]); r++) ie._data(n, "globalEval", !t || ie._data(t[r], "globalEval"))
    }

    function T(e, t) {
        if (1 === t.nodeType && ie.hasData(e)) {
            var n, r, i, o = ie._data(e),
                a = ie._data(t, o),
                s = o.events;
            if (s) {
                delete a.handle, a.events = {};
                for (n in s)
                    for (r = 0, i = s[n].length; i > r; r++) ie.event.add(t, n, s[n][r])
            }
            a.data && (a.data = ie.extend({}, a.data))
        }
    }

    function C(e, t) {
        var n, r, i;
        if (1 === t.nodeType) {
            if (n = t.nodeName.toLowerCase(), !ne.noCloneEvent && t[ie.expando]) {
                i = ie._data(t);
                for (r in i.events) ie.removeEvent(t, r, i.handle);
                t.removeAttribute(ie.expando)
            }
            "script" === n && t.text !== e.text ? (b(t).text = e.text, x(t)) : "object" === n ? (t.parentNode && (t.outerHTML = e.outerHTML), ne.html5Clone && e.innerHTML && !ie.trim(t.innerHTML) && (t.innerHTML = e.innerHTML)) : "input" === n && je.test(e.type) ? (t.defaultChecked = t.checked = e.checked, t.value !== e.value && (t.value = e.value)) : "option" === n ? t.defaultSelected = t.selected = e.defaultSelected : ("input" === n || "textarea" === n) && (t.defaultValue = e.defaultValue)
        }
    }

    function N(t, n) {
        var r, i = ie(n.createElement(t)).appendTo(n.body),
            o = e.getDefaultComputedStyle && (r = e.getDefaultComputedStyle(i[0])) ? r.display : ie.css(i[0], "display");
        return i.detach(), o
    }

    function E(e) {
        var t = he,
            n = Ze[e];
        return n || (n = N(e, t), "none" !== n && n || (Ke = (Ke || ie("<iframe frameborder='0' width='0' height='0'/>")).appendTo(t.documentElement), t = (Ke[0].contentWindow || Ke[0].contentDocument).document, t.write(), t.close(), n = N(e, t), Ke.detach()), Ze[e] = n), n
    }

    function k(e, t) {
        return {
            get: function() {
                var n = e();
                if (null != n) return n ? void delete this.get : (this.get = t).apply(this, arguments)
            }
        }
    }

    function S(e, t) {
        if (t in e) return t;
        for (var n = t.charAt(0).toUpperCase() + t.slice(1), r = t, i = ft.length; i--;)
            if (t = ft[i] + n, t in e) return t;
        return r
    }

    function A(e, t) {
        for (var n, r, i, o = [], a = 0, s = e.length; s > a; a++) r = e[a], r.style && (o[a] = ie._data(r, "olddisplay"), n = r.style.display, t ? (o[a] || "none" !== n || (r.style.display = ""), "" === r.style.display && Ae(r) && (o[a] = ie._data(r, "olddisplay", E(r.nodeName)))) : (i = Ae(r), (n && "none" !== n || !i) && ie._data(r, "olddisplay", i ? n : ie.css(r, "display"))));
        for (a = 0; s > a; a++) r = e[a], r.style && (t && "none" !== r.style.display && "" !== r.style.display || (r.style.display = t ? o[a] || "" : "none"));
        return e
    }

    function D(e, t, n) {
        var r = ut.exec(t);
        return r ? Math.max(0, r[1] - (n || 0)) + (r[2] || "px") : t
    }

    function j(e, t, n, r, i) {
        for (var o = n === (r ? "border" : "content") ? 4 : "width" === t ? 1 : 0, a = 0; 4 > o; o += 2) "margin" === n && (a += ie.css(e, n + Se[o], !0, i)), r ? ("content" === n && (a -= ie.css(e, "padding" + Se[o], !0, i)), "margin" !== n && (a -= ie.css(e, "border" + Se[o] + "Width", !0, i))) : (a += ie.css(e, "padding" + Se[o], !0, i), "padding" !== n && (a += ie.css(e, "border" + Se[o] + "Width", !0, i)));
        return a
    }

    function L(e, t, n) {
        var r = !0,
            i = "width" === t ? e.offsetWidth : e.offsetHeight,
            o = et(e),
            a = ne.boxSizing && "border-box" === ie.css(e, "boxSizing", !1, o);
        if (0 >= i || null == i) {
            if (i = tt(e, t, o), (0 > i || null == i) && (i = e.style[t]), rt.test(i)) return i;
            r = a && (ne.boxSizingReliable() || i === e.style[t]), i = parseFloat(i) || 0
        }
        return i + j(e, t, n || (a ? "border" : "content"), r, o) + "px"
    }

    function H(e, t, n, r, i) {
        return new H.prototype.init(e, t, n, r, i)
    }

    function q() {
        return setTimeout(function() {
            pt = void 0
        }), pt = ie.now()
    }

    function _(e, t) {
        var n, r = {
                height: e
            },
            i = 0;
        for (t = t ? 1 : 0; 4 > i; i += 2 - t) n = Se[i], r["margin" + n] = r["padding" + n] = e;
        return t && (r.opacity = r.width = e), r
    }

    function M(e, t, n) {
        for (var r, i = (bt[t] || []).concat(bt["*"]), o = 0, a = i.length; a > o; o++)
            if (r = i[o].call(n, t, e)) return r
    }

    function F(e, t, n) {
        var r, i, o, a, s, u, l, c, d = this,
            f = {},
            p = e.style,
            h = e.nodeType && Ae(e),
            m = ie._data(e, "fxshow");
        n.queue || (s = ie._queueHooks(e, "fx"), null == s.unqueued && (s.unqueued = 0, u = s.empty.fire, s.empty.fire = function() {
            s.unqueued || u()
        }), s.unqueued++, d.always(function() {
            d.always(function() {
                s.unqueued--, ie.queue(e, "fx").length || s.empty.fire()
            })
        })), 1 === e.nodeType && ("height" in t || "width" in t) && (n.overflow = [p.overflow, p.overflowX, p.overflowY], l = ie.css(e, "display"), c = "none" === l ? ie._data(e, "olddisplay") || E(e.nodeName) : l, "inline" === c && "none" === ie.css(e, "float") && (ne.inlineBlockNeedsLayout && "inline" !== E(e.nodeName) ? p.zoom = 1 : p.display = "inline-block")), n.overflow && (p.overflow = "hidden", ne.shrinkWrapBlocks() || d.always(function() {
            p.overflow = n.overflow[0], p.overflowX = n.overflow[1], p.overflowY = n.overflow[2]
        }));
        for (r in t)
            if (i = t[r], mt.exec(i)) {
                if (delete t[r], o = o || "toggle" === i, i === (h ? "hide" : "show")) {
                    if ("show" !== i || !m || void 0 === m[r]) continue;
                    h = !0
                }
                f[r] = m && m[r] || ie.style(e, r)
            } else l = void 0;
        if (ie.isEmptyObject(f)) "inline" === ("none" === l ? E(e.nodeName) : l) && (p.display = l);
        else {
            m ? "hidden" in m && (h = m.hidden) : m = ie._data(e, "fxshow", {}), o && (m.hidden = !h), h ? ie(e).show() : d.done(function() {
                ie(e).hide()
            }), d.done(function() {
                var t;
                ie._removeData(e, "fxshow");
                for (t in f) ie.style(e, t, f[t])
            });
            for (r in f) a = M(h ? m[r] : 0, r, d), r in m || (m[r] = a.start, h && (a.end = a.start, a.start = "width" === r || "height" === r ? 1 : 0))
        }
    }

    function O(e, t) {
        var n, r, i, o, a;
        for (n in e)
            if (r = ie.camelCase(n), i = t[r], o = e[n], ie.isArray(o) && (i = o[1], o = e[n] = o[0]), n !== r && (e[r] = o, delete e[n]), a = ie.cssHooks[r], a && "expand" in a) {
                o = a.expand(o), delete e[r];
                for (n in o) n in e || (e[n] = o[n], t[n] = i)
            } else t[r] = i
    }

    function B(e, t, n) {
        var r, i, o = 0,
            a = yt.length,
            s = ie.Deferred().always(function() {
                delete u.elem
            }),
            u = function() {
                if (i) return !1;
                for (var t = pt || q(), n = Math.max(0, l.startTime + l.duration - t), r = n / l.duration || 0, o = 1 - r, a = 0, u = l.tweens.length; u > a; a++) l.tweens[a].run(o);
                return s.notifyWith(e, [l, o, n]), 1 > o && u ? n : (s.resolveWith(e, [l]), !1)
            },
            l = s.promise({
                elem: e,
                props: ie.extend({}, t),
                opts: ie.extend(!0, {
                    specialEasing: {}
                }, n),
                originalProperties: t,
                originalOptions: n,
                startTime: pt || q(),
                duration: n.duration,
                tweens: [],
                createTween: function(t, n) {
                    var r = ie.Tween(e, l.opts, t, n, l.opts.specialEasing[t] || l.opts.easing);
                    return l.tweens.push(r), r
                },
                stop: function(t) {
                    var n = 0,
                        r = t ? l.tweens.length : 0;
                    if (i) return this;
                    for (i = !0; r > n; n++) l.tweens[n].run(1);
                    return t ? s.resolveWith(e, [l, t]) : s.rejectWith(e, [l, t]), this
                }
            }),
            c = l.props;
        for (O(c, l.opts.specialEasing); a > o; o++)
            if (r = yt[o].call(l, e, c, l.opts)) return r;
        return ie.map(c, M, l), ie.isFunction(l.opts.start) && l.opts.start.call(e, l), ie.fx.timer(ie.extend(u, {
            elem: e,
            anim: l,
            queue: l.opts.queue
        })), l.progress(l.opts.progress).done(l.opts.done, l.opts.complete).fail(l.opts.fail).always(l.opts.always)
    }

    function P(e) {
        return function(t, n) {
            "string" != typeof t && (n = t, t = "*");
            var r, i = 0,
                o = t.toLowerCase().match(be) || [];
            if (ie.isFunction(n))
                for (; r = o[i++];) "+" === r.charAt(0) ? (r = r.slice(1) || "*", (e[r] = e[r] || []).unshift(n)) : (e[r] = e[r] || []).push(n)
        }
    }

    function R(e, t, n, r) {
        function i(s) {
            var u;
            return o[s] = !0, ie.each(e[s] || [], function(e, s) {
                var l = s(t, n, r);
                return "string" != typeof l || a || o[l] ? a ? !(u = l) : void 0 : (t.dataTypes.unshift(l), i(l), !1)
            }), u
        }
        var o = {},
            a = e === zt;
        return i(t.dataTypes[0]) || !o["*"] && i("*")
    }

    function W(e, t) {
        var n, r, i = ie.ajaxSettings.flatOptions || {};
        for (r in t) void 0 !== t[r] && ((i[r] ? e : n || (n = {}))[r] = t[r]);
        return n && ie.extend(!0, e, n), e
    }

    function $(e, t, n) {
        for (var r, i, o, a, s = e.contents, u = e.dataTypes;
            "*" === u[0];) u.shift(), void 0 === i && (i = e.mimeType || t.getResponseHeader("Content-Type"));
        if (i)
            for (a in s)
                if (s[a] && s[a].test(i)) {
                    u.unshift(a);
                    break
                }
        if (u[0] in n) o = u[0];
        else {
            for (a in n) {
                if (!u[0] || e.converters[a + " " + u[0]]) {
                    o = a;
                    break
                }
                r || (r = a)
            }
            o = o || r
        }
        return o ? (o !== u[0] && u.unshift(o), n[o]) : void 0
    }

    function z(e, t, n, r) {
        var i, o, a, s, u, l = {},
            c = e.dataTypes.slice();
        if (c[1])
            for (a in e.converters) l[a.toLowerCase()] = e.converters[a];
        for (o = c.shift(); o;)
            if (e.responseFields[o] && (n[e.responseFields[o]] = t), !u && r && e.dataFilter && (t = e.dataFilter(t, e.dataType)), u = o, o = c.shift())
                if ("*" === o) o = u;
                else if ("*" !== u && u !== o) {
            if (a = l[u + " " + o] || l["* " + o], !a)
                for (i in l)
                    if (s = i.split(" "), s[1] === o && (a = l[u + " " + s[0]] || l["* " + s[0]])) {
                        a === !0 ? a = l[i] : l[i] !== !0 && (o = s[0], c.unshift(s[1]));
                        break
                    }
            if (a !== !0)
                if (a && e["throws"]) t = a(t);
                else try {
                    t = a(t)
                } catch (d) {
                    return {
                        state: "parsererror",
                        error: a ? d : "No conversion from " + u + " to " + o
                    }
                }
        }
        return {
            state: "success",
            data: t
        }
    }

    function I(e, t, n, r) {
        var i;
        if (ie.isArray(t)) ie.each(t, function(t, i) {
            n || Vt.test(e) ? r(e, i) : I(e + "[" + ("object" == typeof i ? t : "") + "]", i, n, r)
        });
        else if (n || "object" !== ie.type(t)) r(e, t);
        else
            for (i in t) I(e + "[" + i + "]", t[i], n, r)
    }

    function X() {
        try {
            return new e.XMLHttpRequest
        } catch (t) {}
    }

    function U() {
        try {
            return new e.ActiveXObject("Microsoft.XMLHTTP")
        } catch (t) {}
    }

    function V(e) {
        return ie.isWindow(e) ? e : 9 === e.nodeType ? e.defaultView || e.parentWindow : !1
    }
    var J = [],
        Y = J.slice,
        G = J.concat,
        Q = J.push,
        K = J.indexOf,
        Z = {},
        ee = Z.toString,
        te = Z.hasOwnProperty,
        ne = {},
        re = "1.11.2",
        ie = function(e, t) {
            return new ie.fn.init(e, t)
        },
        oe = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
        ae = /^-ms-/,
        se = /-([\da-z])/gi,
        ue = function(e, t) {
            return t.toUpperCase()
        };
    ie.fn = ie.prototype = {
        jquery: re,
        constructor: ie,
        selector: "",
        length: 0,
        toArray: function() {
            return Y.call(this)
        },
        get: function(e) {
            return null != e ? 0 > e ? this[e + this.length] : this[e] : Y.call(this)
        },
        pushStack: function(e) {
            var t = ie.merge(this.constructor(), e);
            return t.prevObject = this, t.context = this.context, t
        },
        each: function(e, t) {
            return ie.each(this, e, t)
        },
        map: function(e) {
            return this.pushStack(ie.map(this, function(t, n) {
                return e.call(t, n, t)
            }))
        },
        slice: function() {
            return this.pushStack(Y.apply(this, arguments))
        },
        first: function() {
            return this.eq(0)
        },
        last: function() {
            return this.eq(-1)
        },
        eq: function(e) {
            var t = this.length,
                n = +e + (0 > e ? t : 0);
            return this.pushStack(n >= 0 && t > n ? [this[n]] : [])
        },
        end: function() {
            return this.prevObject || this.constructor(null)
        },
        push: Q,
        sort: J.sort,
        splice: J.splice
    }, ie.extend = ie.fn.extend = function() {
        var e, t, n, r, i, o, a = arguments[0] || {},
            s = 1,
            u = arguments.length,
            l = !1;
        for ("boolean" == typeof a && (l = a, a = arguments[s] || {}, s++), "object" == typeof a || ie.isFunction(a) || (a = {}), s === u && (a = this, s--); u > s; s++)
            if (null != (i = arguments[s]))
                for (r in i) e = a[r], n = i[r], a !== n && (l && n && (ie.isPlainObject(n) || (t = ie.isArray(n))) ? (t ? (t = !1, o = e && ie.isArray(e) ? e : []) : o = e && ie.isPlainObject(e) ? e : {}, a[r] = ie.extend(l, o, n)) : void 0 !== n && (a[r] = n));
        return a
    }, ie.extend({
        expando: "jQuery" + (re + Math.random()).replace(/\D/g, ""),
        isReady: !0,
        error: function(e) {
            throw new Error(e)
        },
        noop: function() {},
        isFunction: function(e) {
            return "function" === ie.type(e)
        },
        isArray: Array.isArray || function(e) {
            return "array" === ie.type(e)
        },
        isWindow: function(e) {
            return null != e && e == e.window
        },
        isNumeric: function(e) {
            return !ie.isArray(e) && e - parseFloat(e) + 1 >= 0
        },
        isEmptyObject: function(e) {
            var t;
            for (t in e) return !1;
            return !0
        },
        isPlainObject: function(e) {
            var t;
            if (!e || "object" !== ie.type(e) || e.nodeType || ie.isWindow(e)) return !1;
            try {
                if (e.constructor && !te.call(e, "constructor") && !te.call(e.constructor.prototype, "isPrototypeOf")) return !1
            } catch (n) {
                return !1
            }
            if (ne.ownLast)
                for (t in e) return te.call(e, t);
            for (t in e);
            return void 0 === t || te.call(e, t)
        },
        type: function(e) {
            return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? Z[ee.call(e)] || "object" : typeof e
        },
        globalEval: function(t) {
            t && ie.trim(t) && (e.execScript || function(t) {
                e.eval.call(e, t)
            })(t)
        },
        camelCase: function(e) {
            return e.replace(ae, "ms-").replace(se, ue)
        },
        nodeName: function(e, t) {
            return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
        },
        each: function(e, t, r) {
            var i, o = 0,
                a = e.length,
                s = n(e);
            if (r) {
                if (s)
                    for (; a > o && (i = t.apply(e[o], r), i !== !1); o++);
                else
                    for (o in e)
                        if (i = t.apply(e[o], r), i === !1) break
            } else if (s)
                for (; a > o && (i = t.call(e[o], o, e[o]), i !== !1); o++);
            else
                for (o in e)
                    if (i = t.call(e[o], o, e[o]), i === !1) break; return e
        },
        trim: function(e) {
            return null == e ? "" : (e + "").replace(oe, "")
        },
        makeArray: function(e, t) {
            var r = t || [];
            return null != e && (n(Object(e)) ? ie.merge(r, "string" == typeof e ? [e] : e) : Q.call(r, e)), r
        },
        inArray: function(e, t, n) {
            var r;
            if (t) {
                if (K) return K.call(t, e, n);
                for (r = t.length, n = n ? 0 > n ? Math.max(0, r + n) : n : 0; r > n; n++)
                    if (n in t && t[n] === e) return n
            }
            return -1
        },
        merge: function(e, t) {
            for (var n = +t.length, r = 0, i = e.length; n > r;) e[i++] = t[r++];
            if (n !== n)
                for (; void 0 !== t[r];) e[i++] = t[r++];
            return e.length = i, e
        },
        grep: function(e, t, n) {
            for (var r, i = [], o = 0, a = e.length, s = !n; a > o; o++) r = !t(e[o], o), r !== s && i.push(e[o]);
            return i
        },
        map: function(e, t, r) {
            var i, o = 0,
                a = e.length,
                s = n(e),
                u = [];
            if (s)
                for (; a > o; o++) i = t(e[o], o, r), null != i && u.push(i);
            else
                for (o in e) i = t(e[o], o, r), null != i && u.push(i);
            return G.apply([], u)
        },
        guid: 1,
        proxy: function(e, t) {
            var n, r, i;
            return "string" == typeof t && (i = e[t], t = e, e = i), ie.isFunction(e) ? (n = Y.call(arguments, 2), r = function() {
                return e.apply(t || this, n.concat(Y.call(arguments)))
            }, r.guid = e.guid = e.guid || ie.guid++, r) : void 0
        },
        now: function() {
            return +new Date
        },
        support: ne
    }), ie.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(e, t) {
        Z["[object " + t + "]"] = t.toLowerCase()
    });
    var le = function(e) {
        function t(e, t, n, r) {
            var i, o, a, s, u, l, d, p, h, m;
            if ((t ? t.ownerDocument || t : R) !== H && L(t), t = t || H, n = n || [], s = t.nodeType, "string" != typeof e || !e || 1 !== s && 9 !== s && 11 !== s) return n;
            if (!r && _) {
                if (11 !== s && (i = ye.exec(e)))
                    if (a = i[1]) {
                        if (9 === s) {
                            if (o = t.getElementById(a), !o || !o.parentNode) return n;
                            if (o.id === a) return n.push(o), n
                        } else if (t.ownerDocument && (o = t.ownerDocument.getElementById(a)) && B(t, o) && o.id === a) return n.push(o), n
                    } else {
                        if (i[2]) return K.apply(n, t.getElementsByTagName(e)), n;
                        if ((a = i[3]) && w.getElementsByClassName) return K.apply(n, t.getElementsByClassName(a)), n
                    }
                if (w.qsa && (!M || !M.test(e))) {
                    if (p = d = P, h = t, m = 1 !== s && e, 1 === s && "object" !== t.nodeName.toLowerCase()) {
                        for (l = E(e), (d = t.getAttribute("id")) ? p = d.replace(xe, "\\$&") : t.setAttribute("id", p), p = "[id='" + p + "'] ", u = l.length; u--;) l[u] = p + f(l[u]);
                        h = be.test(e) && c(t.parentNode) || t, m = l.join(",")
                    }
                    if (m) try {
                        return K.apply(n, h.querySelectorAll(m)), n
                    } catch (g) {} finally {
                        d || t.removeAttribute("id")
                    }
                }
            }
            return S(e.replace(ue, "$1"), t, n, r)
        }

        function n() {
            function e(n, r) {
                return t.push(n + " ") > T.cacheLength && delete e[t.shift()], e[n + " "] = r
            }
            var t = [];
            return e
        }

        function r(e) {
            return e[P] = !0, e
        }

        function i(e) {
            var t = H.createElement("div");
            try {
                return !!e(t)
            } catch (n) {
                return !1
            } finally {
                t.parentNode && t.parentNode.removeChild(t), t = null
            }
        }

        function o(e, t) {
            for (var n = e.split("|"), r = e.length; r--;) T.attrHandle[n[r]] = t
        }

        function a(e, t) {
            var n = t && e,
                r = n && 1 === e.nodeType && 1 === t.nodeType && (~t.sourceIndex || V) - (~e.sourceIndex || V);
            if (r) return r;
            if (n)
                for (; n = n.nextSibling;)
                    if (n === t) return -1;
            return e ? 1 : -1
        }

        function s(e) {
            return function(t) {
                var n = t.nodeName.toLowerCase();
                return "input" === n && t.type === e
            }
        }

        function u(e) {
            return function(t) {
                var n = t.nodeName.toLowerCase();
                return ("input" === n || "button" === n) && t.type === e
            }
        }

        function l(e) {
            return r(function(t) {
                return t = +t, r(function(n, r) {
                    for (var i, o = e([], n.length, t), a = o.length; a--;) n[i = o[a]] && (n[i] = !(r[i] = n[i]))
                })
            })
        }

        function c(e) {
            return e && "undefined" != typeof e.getElementsByTagName && e
        }

        function d() {}

        function f(e) {
            for (var t = 0, n = e.length, r = ""; n > t; t++) r += e[t].value;
            return r
        }

        function p(e, t, n) {
            var r = t.dir,
                i = n && "parentNode" === r,
                o = $++;
            return t.first ? function(t, n, o) {
                for (; t = t[r];)
                    if (1 === t.nodeType || i) return e(t, n, o)
            } : function(t, n, a) {
                var s, u, l = [W, o];
                if (a) {
                    for (; t = t[r];)
                        if ((1 === t.nodeType || i) && e(t, n, a)) return !0
                } else
                    for (; t = t[r];)
                        if (1 === t.nodeType || i) {
                            if (u = t[P] || (t[P] = {}), (s = u[r]) && s[0] === W && s[1] === o) return l[2] = s[2];
                            if (u[r] = l, l[2] = e(t, n, a)) return !0
                        }
            }
        }

        function h(e) {
            return e.length > 1 ? function(t, n, r) {
                for (var i = e.length; i--;)
                    if (!e[i](t, n, r)) return !1;
                return !0
            } : e[0]
        }

        function m(e, n, r) {
            for (var i = 0, o = n.length; o > i; i++) t(e, n[i], r);
            return r
        }

        function g(e, t, n, r, i) {
            for (var o, a = [], s = 0, u = e.length, l = null != t; u > s; s++)(o = e[s]) && (!n || n(o, r, i)) && (a.push(o), l && t.push(s));
            return a
        }

        function v(e, t, n, i, o, a) {
            return i && !i[P] && (i = v(i)), o && !o[P] && (o = v(o, a)), r(function(r, a, s, u) {
                var l, c, d, f = [],
                    p = [],
                    h = a.length,
                    v = r || m(t || "*", s.nodeType ? [s] : s, []),
                    y = !e || !r && t ? v : g(v, f, e, s, u),
                    b = n ? o || (r ? e : h || i) ? [] : a : y;
                if (n && n(y, b, s, u), i)
                    for (l = g(b, p), i(l, [], s, u), c = l.length; c--;)(d = l[c]) && (b[p[c]] = !(y[p[c]] = d));
                if (r) {
                    if (o || e) {
                        if (o) {
                            for (l = [], c = b.length; c--;)(d = b[c]) && l.push(y[c] = d);
                            o(null, b = [], l, u)
                        }
                        for (c = b.length; c--;)(d = b[c]) && (l = o ? ee(r, d) : f[c]) > -1 && (r[l] = !(a[l] = d))
                    }
                } else b = g(b === a ? b.splice(h, b.length) : b), o ? o(null, a, b, u) : K.apply(a, b)
            })
        }

        function y(e) {
            for (var t, n, r, i = e.length, o = T.relative[e[0].type], a = o || T.relative[" "], s = o ? 1 : 0, u = p(function(e) {
                    return e === t
                }, a, !0), l = p(function(e) {
                    return ee(t, e) > -1
                }, a, !0), c = [function(e, n, r) {
                    var i = !o && (r || n !== A) || ((t = n).nodeType ? u(e, n, r) : l(e, n, r));
                    return t = null, i
                }]; i > s; s++)
                if (n = T.relative[e[s].type]) c = [p(h(c), n)];
                else {
                    if (n = T.filter[e[s].type].apply(null, e[s].matches), n[P]) {
                        for (r = ++s; i > r && !T.relative[e[r].type]; r++);
                        return v(s > 1 && h(c), s > 1 && f(e.slice(0, s - 1).concat({
                            value: " " === e[s - 2].type ? "*" : ""
                        })).replace(ue, "$1"), n, r > s && y(e.slice(s, r)), i > r && y(e = e.slice(r)), i > r && f(e))
                    }
                    c.push(n)
                }
            return h(c)
        }

        function b(e, n) {
            var i = n.length > 0,
                o = e.length > 0,
                a = function(r, a, s, u, l) {
                    var c, d, f, p = 0,
                        h = "0",
                        m = r && [],
                        v = [],
                        y = A,
                        b = r || o && T.find.TAG("*", l),
                        x = W += null == y ? 1 : Math.random() || .1,
                        w = b.length;
                    for (l && (A = a !== H && a); h !== w && null != (c = b[h]); h++) {
                        if (o && c) {
                            for (d = 0; f = e[d++];)
                                if (f(c, a, s)) {
                                    u.push(c);
                                    break
                                }
                            l && (W = x)
                        }
                        i && ((c = !f && c) && p--, r && m.push(c))
                    }
                    if (p += h, i && h !== p) {
                        for (d = 0; f = n[d++];) f(m, v, a, s);
                        if (r) {
                            if (p > 0)
                                for (; h--;) m[h] || v[h] || (v[h] = G.call(u));
                            v = g(v)
                        }
                        K.apply(u, v), l && !r && v.length > 0 && p + n.length > 1 && t.uniqueSort(u)
                    }
                    return l && (W = x, A = y), m
                };
            return i ? r(a) : a
        }
        var x, w, T, C, N, E, k, S, A, D, j, L, H, q, _, M, F, O, B, P = "sizzle" + 1 * new Date,
            R = e.document,
            W = 0,
            $ = 0,
            z = n(),
            I = n(),
            X = n(),
            U = function(e, t) {
                return e === t && (j = !0), 0
            },
            V = 1 << 31,
            J = {}.hasOwnProperty,
            Y = [],
            G = Y.pop,
            Q = Y.push,
            K = Y.push,
            Z = Y.slice,
            ee = function(e, t) {
                for (var n = 0, r = e.length; r > n; n++)
                    if (e[n] === t) return n;
                return -1
            },
            te = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
            ne = "[\\x20\\t\\r\\n\\f]",
            re = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",
            ie = re.replace("w", "w#"),
            oe = "\\[" + ne + "*(" + re + ")(?:" + ne + "*([*^$|!~]?=)" + ne + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + ie + "))|)" + ne + "*\\]",
            ae = ":(" + re + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + oe + ")*)|.*)\\)|)",
            se = new RegExp(ne + "+", "g"),
            ue = new RegExp("^" + ne + "+|((?:^|[^\\\\])(?:\\\\.)*)" + ne + "+$", "g"),
            le = new RegExp("^" + ne + "*," + ne + "*"),
            ce = new RegExp("^" + ne + "*([>+~]|" + ne + ")" + ne + "*"),
            de = new RegExp("=" + ne + "*([^\\]'\"]*?)" + ne + "*\\]", "g"),
            fe = new RegExp(ae),
            pe = new RegExp("^" + ie + "$"),
            he = {
                ID: new RegExp("^#(" + re + ")"),
                CLASS: new RegExp("^\\.(" + re + ")"),
                TAG: new RegExp("^(" + re.replace("w", "w*") + ")"),
                ATTR: new RegExp("^" + oe),
                PSEUDO: new RegExp("^" + ae),
                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + ne + "*(even|odd|(([+-]|)(\\d*)n|)" + ne + "*(?:([+-]|)" + ne + "*(\\d+)|))" + ne + "*\\)|)", "i"),
                bool: new RegExp("^(?:" + te + ")$", "i"),
                needsContext: new RegExp("^" + ne + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + ne + "*((?:-\\d)?\\d*)" + ne + "*\\)|)(?=[^-]|$)", "i")
            },
            me = /^(?:input|select|textarea|button)$/i,
            ge = /^h\d$/i,
            ve = /^[^{]+\{\s*\[native \w/,
            ye = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
            be = /[+~]/,
            xe = /'|\\/g,
            we = new RegExp("\\\\([\\da-f]{1,6}" + ne + "?|(" + ne + ")|.)", "ig"),
            Te = function(e, t, n) {
                var r = "0x" + t - 65536;
                return r !== r || n ? t : 0 > r ? String.fromCharCode(r + 65536) : String.fromCharCode(r >> 10 | 55296, 1023 & r | 56320)
            },
            Ce = function() {
                L()
            };
        try {
            K.apply(Y = Z.call(R.childNodes), R.childNodes), Y[R.childNodes.length].nodeType
        } catch (Ne) {
            K = {
                apply: Y.length ? function(e, t) {
                    Q.apply(e, Z.call(t))
                } : function(e, t) {
                    for (var n = e.length, r = 0; e[n++] = t[r++];);
                    e.length = n - 1
                }
            }
        }
        w = t.support = {}, N = t.isXML = function(e) {
            var t = e && (e.ownerDocument || e).documentElement;
            return t ? "HTML" !== t.nodeName : !1
        }, L = t.setDocument = function(e) {
            var t, n, r = e ? e.ownerDocument || e : R;
            return r !== H && 9 === r.nodeType && r.documentElement ? (H = r, q = r.documentElement, n = r.defaultView, n && n !== n.top && (n.addEventListener ? n.addEventListener("unload", Ce, !1) : n.attachEvent && n.attachEvent("onunload", Ce)), _ = !N(r), w.attributes = i(function(e) {
                return e.className = "i", !e.getAttribute("className")
            }), w.getElementsByTagName = i(function(e) {
                return e.appendChild(r.createComment("")), !e.getElementsByTagName("*").length
            }), w.getElementsByClassName = ve.test(r.getElementsByClassName), w.getById = i(function(e) {
                return q.appendChild(e).id = P, !r.getElementsByName || !r.getElementsByName(P).length
            }), w.getById ? (T.find.ID = function(e, t) {
                if ("undefined" != typeof t.getElementById && _) {
                    var n = t.getElementById(e);
                    return n && n.parentNode ? [n] : []
                }
            }, T.filter.ID = function(e) {
                var t = e.replace(we, Te);
                return function(e) {
                    return e.getAttribute("id") === t
                }
            }) : (delete T.find.ID, T.filter.ID = function(e) {
                var t = e.replace(we, Te);
                return function(e) {
                    var n = "undefined" != typeof e.getAttributeNode && e.getAttributeNode("id");
                    return n && n.value === t
                }
            }), T.find.TAG = w.getElementsByTagName ? function(e, t) {
                return "undefined" != typeof t.getElementsByTagName ? t.getElementsByTagName(e) : w.qsa ? t.querySelectorAll(e) : void 0
            } : function(e, t) {
                var n, r = [],
                    i = 0,
                    o = t.getElementsByTagName(e);
                if ("*" === e) {
                    for (; n = o[i++];) 1 === n.nodeType && r.push(n);
                    return r
                }
                return o
            }, T.find.CLASS = w.getElementsByClassName && function(e, t) {
                return _ ? t.getElementsByClassName(e) : void 0
            }, F = [], M = [], (w.qsa = ve.test(r.querySelectorAll)) && (i(function(e) {
                q.appendChild(e).innerHTML = "<a id='" + P + "'></a><select id='" + P + "-\f]' msallowcapture=''><option selected=''></option></select>", e.querySelectorAll("[msallowcapture^='']").length && M.push("[*^$]=" + ne + "*(?:''|\"\")"), e.querySelectorAll("[selected]").length || M.push("\\[" + ne + "*(?:value|" + te + ")"), e.querySelectorAll("[id~=" + P + "-]").length || M.push("~="), e.querySelectorAll(":checked").length || M.push(":checked"), e.querySelectorAll("a#" + P + "+*").length || M.push(".#.+[+~]")
            }), i(function(e) {
                var t = r.createElement("input");
                t.setAttribute("type", "hidden"), e.appendChild(t).setAttribute("name", "D"), e.querySelectorAll("[name=d]").length && M.push("name" + ne + "*[*^$|!~]?="), e.querySelectorAll(":enabled").length || M.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), M.push(",.*:")
            })), (w.matchesSelector = ve.test(O = q.matches || q.webkitMatchesSelector || q.mozMatchesSelector || q.oMatchesSelector || q.msMatchesSelector)) && i(function(e) {
                w.disconnectedMatch = O.call(e, "div"), O.call(e, "[s!='']:x"), F.push("!=", ae)
            }), M = M.length && new RegExp(M.join("|")), F = F.length && new RegExp(F.join("|")), t = ve.test(q.compareDocumentPosition), B = t || ve.test(q.contains) ? function(e, t) {
                var n = 9 === e.nodeType ? e.documentElement : e,
                    r = t && t.parentNode;
                return e === r || !(!r || 1 !== r.nodeType || !(n.contains ? n.contains(r) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(r)))
            } : function(e, t) {
                if (t)
                    for (; t = t.parentNode;)
                        if (t === e) return !0;
                return !1
            }, U = t ? function(e, t) {
                if (e === t) return j = !0, 0;
                var n = !e.compareDocumentPosition - !t.compareDocumentPosition;
                return n ? n : (n = (e.ownerDocument || e) === (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1, 1 & n || !w.sortDetached && t.compareDocumentPosition(e) === n ? e === r || e.ownerDocument === R && B(R, e) ? -1 : t === r || t.ownerDocument === R && B(R, t) ? 1 : D ? ee(D, e) - ee(D, t) : 0 : 4 & n ? -1 : 1)
            } : function(e, t) {
                if (e === t) return j = !0, 0;
                var n, i = 0,
                    o = e.parentNode,
                    s = t.parentNode,
                    u = [e],
                    l = [t];
                if (!o || !s) return e === r ? -1 : t === r ? 1 : o ? -1 : s ? 1 : D ? ee(D, e) - ee(D, t) : 0;
                if (o === s) return a(e, t);
                for (n = e; n = n.parentNode;) u.unshift(n);
                for (n = t; n = n.parentNode;) l.unshift(n);
                for (; u[i] === l[i];) i++;
                return i ? a(u[i], l[i]) : u[i] === R ? -1 : l[i] === R ? 1 : 0
            }, r) : H
        }, t.matches = function(e, n) {
            return t(e, null, null, n)
        }, t.matchesSelector = function(e, n) {
            if ((e.ownerDocument || e) !== H && L(e), n = n.replace(de, "='$1']"), !(!w.matchesSelector || !_ || F && F.test(n) || M && M.test(n))) try {
                var r = O.call(e, n);
                if (r || w.disconnectedMatch || e.document && 11 !== e.document.nodeType) return r
            } catch (i) {}
            return t(n, H, null, [e]).length > 0
        }, t.contains = function(e, t) {
            return (e.ownerDocument || e) !== H && L(e), B(e, t)
        }, t.attr = function(e, t) {
            (e.ownerDocument || e) !== H && L(e);
            var n = T.attrHandle[t.toLowerCase()],
                r = n && J.call(T.attrHandle, t.toLowerCase()) ? n(e, t, !_) : void 0;
            return void 0 !== r ? r : w.attributes || !_ ? e.getAttribute(t) : (r = e.getAttributeNode(t)) && r.specified ? r.value : null
        }, t.error = function(e) {
            throw new Error("Syntax error, unrecognized expression: " + e)
        }, t.uniqueSort = function(e) {
            var t, n = [],
                r = 0,
                i = 0;
            if (j = !w.detectDuplicates, D = !w.sortStable && e.slice(0), e.sort(U), j) {
                for (; t = e[i++];) t === e[i] && (r = n.push(i));
                for (; r--;) e.splice(n[r], 1)
            }
            return D = null, e
        }, C = t.getText = function(e) {
            var t, n = "",
                r = 0,
                i = e.nodeType;
            if (i) {
                if (1 === i || 9 === i || 11 === i) {
                    if ("string" == typeof e.textContent) return e.textContent;
                    for (e = e.firstChild; e; e = e.nextSibling) n += C(e)
                } else if (3 === i || 4 === i) return e.nodeValue
            } else
                for (; t = e[r++];) n += C(t);
            return n
        }, T = t.selectors = {
            cacheLength: 50,
            createPseudo: r,
            match: he,
            attrHandle: {},
            find: {},
            relative: {
                ">": {
                    dir: "parentNode",
                    first: !0
                },
                " ": {
                    dir: "parentNode"
                },
                "+": {
                    dir: "previousSibling",
                    first: !0
                },
                "~": {
                    dir: "previousSibling"
                }
            },
            preFilter: {
                ATTR: function(e) {
                    return e[1] = e[1].replace(we, Te), e[3] = (e[3] || e[4] || e[5] || "").replace(we, Te), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4)
                },
                CHILD: function(e) {
                    return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || t.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && t.error(e[0]), e
                },
                PSEUDO: function(e) {
                    var t, n = !e[6] && e[2];
                    return he.CHILD.test(e[0]) ? null : (e[3] ? e[2] = e[4] || e[5] || "" : n && fe.test(n) && (t = E(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t), e[2] = n.slice(0, t)), e.slice(0, 3))
                }
            },
            filter: {
                TAG: function(e) {
                    var t = e.replace(we, Te).toLowerCase();
                    return "*" === e ? function() {
                        return !0
                    } : function(e) {
                        return e.nodeName && e.nodeName.toLowerCase() === t
                    }
                },
                CLASS: function(e) {
                    var t = z[e + " "];
                    return t || (t = new RegExp("(^|" + ne + ")" + e + "(" + ne + "|$)")) && z(e, function(e) {
                        return t.test("string" == typeof e.className && e.className || "undefined" != typeof e.getAttribute && e.getAttribute("class") || "")
                    })
                },
                ATTR: function(e, n, r) {
                    return function(i) {
                        var o = t.attr(i, e);
                        return null == o ? "!=" === n : n ? (o += "", "=" === n ? o === r : "!=" === n ? o !== r : "^=" === n ? r && 0 === o.indexOf(r) : "*=" === n ? r && o.indexOf(r) > -1 : "$=" === n ? r && o.slice(-r.length) === r : "~=" === n ? (" " + o.replace(se, " ") + " ").indexOf(r) > -1 : "|=" === n ? o === r || o.slice(0, r.length + 1) === r + "-" : !1) : !0
                    }
                },
                CHILD: function(e, t, n, r, i) {
                    var o = "nth" !== e.slice(0, 3),
                        a = "last" !== e.slice(-4),
                        s = "of-type" === t;
                    return 1 === r && 0 === i ? function(e) {
                        return !!e.parentNode
                    } : function(t, n, u) {
                        var l, c, d, f, p, h, m = o !== a ? "nextSibling" : "previousSibling",
                            g = t.parentNode,
                            v = s && t.nodeName.toLowerCase(),
                            y = !u && !s;
                        if (g) {
                            if (o) {
                                for (; m;) {
                                    for (d = t; d = d[m];)
                                        if (s ? d.nodeName.toLowerCase() === v : 1 === d.nodeType) return !1;
                                    h = m = "only" === e && !h && "nextSibling"
                                }
                                return !0
                            }
                            if (h = [a ? g.firstChild : g.lastChild], a && y) {
                                for (c = g[P] || (g[P] = {}), l = c[e] || [], p = l[0] === W && l[1], f = l[0] === W && l[2], d = p && g.childNodes[p]; d = ++p && d && d[m] || (f = p = 0) || h.pop();)
                                    if (1 === d.nodeType && ++f && d === t) {
                                        c[e] = [W, p, f];
                                        break
                                    }
                            } else if (y && (l = (t[P] || (t[P] = {}))[e]) && l[0] === W) f = l[1];
                            else
                                for (;
                                    (d = ++p && d && d[m] || (f = p = 0) || h.pop()) && ((s ? d.nodeName.toLowerCase() !== v : 1 !== d.nodeType) || !++f || (y && ((d[P] || (d[P] = {}))[e] = [W, f]), d !== t)););
                            return f -= i, f === r || f % r === 0 && f / r >= 0
                        }
                    }
                },
                PSEUDO: function(e, n) {
                    var i, o = T.pseudos[e] || T.setFilters[e.toLowerCase()] || t.error("unsupported pseudo: " + e);
                    return o[P] ? o(n) : o.length > 1 ? (i = [e, e, "", n], T.setFilters.hasOwnProperty(e.toLowerCase()) ? r(function(e, t) {
                        for (var r, i = o(e, n), a = i.length; a--;) r = ee(e, i[a]), e[r] = !(t[r] = i[a])
                    }) : function(e) {
                        return o(e, 0, i)
                    }) : o
                }
            },
            pseudos: {
                not: r(function(e) {
                    var t = [],
                        n = [],
                        i = k(e.replace(ue, "$1"));
                    return i[P] ? r(function(e, t, n, r) {
                        for (var o, a = i(e, null, r, []), s = e.length; s--;)(o = a[s]) && (e[s] = !(t[s] = o))
                    }) : function(e, r, o) {
                        return t[0] = e, i(t, null, o, n), t[0] = null, !n.pop()
                    }
                }),
                has: r(function(e) {
                    return function(n) {
                        return t(e, n).length > 0
                    }
                }),
                contains: r(function(e) {
                    return e = e.replace(we, Te),
                        function(t) {
                            return (t.textContent || t.innerText || C(t)).indexOf(e) > -1
                        }
                }),
                lang: r(function(e) {
                    return pe.test(e || "") || t.error("unsupported lang: " + e), e = e.replace(we, Te).toLowerCase(),
                        function(t) {
                            var n;
                            do
                                if (n = _ ? t.lang : t.getAttribute("xml:lang") || t.getAttribute("lang")) return n = n.toLowerCase(), n === e || 0 === n.indexOf(e + "-");
                            while ((t = t.parentNode) && 1 === t.nodeType);
                            return !1
                        }
                }),
                target: function(t) {
                    var n = e.location && e.location.hash;
                    return n && n.slice(1) === t.id
                },
                root: function(e) {
                    return e === q
                },
                focus: function(e) {
                    return e === H.activeElement && (!H.hasFocus || H.hasFocus()) && !!(e.type || e.href || ~e.tabIndex)
                },
                enabled: function(e) {
                    return e.disabled === !1
                },
                disabled: function(e) {
                    return e.disabled === !0
                },
                checked: function(e) {
                    var t = e.nodeName.toLowerCase();
                    return "input" === t && !!e.checked || "option" === t && !!e.selected
                },
                selected: function(e) {
                    return e.parentNode && e.parentNode.selectedIndex, e.selected === !0
                },
                empty: function(e) {
                    for (e = e.firstChild; e; e = e.nextSibling)
                        if (e.nodeType < 6) return !1;
                    return !0
                },
                parent: function(e) {
                    return !T.pseudos.empty(e)
                },
                header: function(e) {
                    return ge.test(e.nodeName)
                },
                input: function(e) {
                    return me.test(e.nodeName)
                },
                button: function(e) {
                    var t = e.nodeName.toLowerCase();
                    return "input" === t && "button" === e.type || "button" === t
                },
                text: function(e) {
                    var t;
                    return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase())
                },
                first: l(function() {
                    return [0]
                }),
                last: l(function(e, t) {
                    return [t - 1]
                }),
                eq: l(function(e, t, n) {
                    return [0 > n ? n + t : n]
                }),
                even: l(function(e, t) {
                    for (var n = 0; t > n; n += 2) e.push(n);
                    return e
                }),
                odd: l(function(e, t) {
                    for (var n = 1; t > n; n += 2) e.push(n);
                    return e
                }),
                lt: l(function(e, t, n) {
                    for (var r = 0 > n ? n + t : n; --r >= 0;) e.push(r);
                    return e
                }),
                gt: l(function(e, t, n) {
                    for (var r = 0 > n ? n + t : n; ++r < t;) e.push(r);
                    return e
                })
            }
        }, T.pseudos.nth = T.pseudos.eq;
        for (x in {
                radio: !0,
                checkbox: !0,
                file: !0,
                password: !0,
                image: !0
            }) T.pseudos[x] = s(x);
        for (x in {
                submit: !0,
                reset: !0
            }) T.pseudos[x] = u(x);
        return d.prototype = T.filters = T.pseudos, T.setFilters = new d, E = t.tokenize = function(e, n) {
            var r, i, o, a, s, u, l, c = I[e + " "];
            if (c) return n ? 0 : c.slice(0);
            for (s = e, u = [], l = T.preFilter; s;) {
                (!r || (i = le.exec(s))) && (i && (s = s.slice(i[0].length) || s), u.push(o = [])), r = !1, (i = ce.exec(s)) && (r = i.shift(), o.push({
                    value: r,
                    type: i[0].replace(ue, " ")
                }), s = s.slice(r.length));
                for (a in T.filter) !(i = he[a].exec(s)) || l[a] && !(i = l[a](i)) || (r = i.shift(),
                    o.push({
                        value: r,
                        type: a,
                        matches: i
                    }), s = s.slice(r.length));
                if (!r) break
            }
            return n ? s.length : s ? t.error(e) : I(e, u).slice(0)
        }, k = t.compile = function(e, t) {
            var n, r = [],
                i = [],
                o = X[e + " "];
            if (!o) {
                for (t || (t = E(e)), n = t.length; n--;) o = y(t[n]), o[P] ? r.push(o) : i.push(o);
                o = X(e, b(i, r)), o.selector = e
            }
            return o
        }, S = t.select = function(e, t, n, r) {
            var i, o, a, s, u, l = "function" == typeof e && e,
                d = !r && E(e = l.selector || e);
            if (n = n || [], 1 === d.length) {
                if (o = d[0] = d[0].slice(0), o.length > 2 && "ID" === (a = o[0]).type && w.getById && 9 === t.nodeType && _ && T.relative[o[1].type]) {
                    if (t = (T.find.ID(a.matches[0].replace(we, Te), t) || [])[0], !t) return n;
                    l && (t = t.parentNode), e = e.slice(o.shift().value.length)
                }
                for (i = he.needsContext.test(e) ? 0 : o.length; i-- && (a = o[i], !T.relative[s = a.type]);)
                    if ((u = T.find[s]) && (r = u(a.matches[0].replace(we, Te), be.test(o[0].type) && c(t.parentNode) || t))) {
                        if (o.splice(i, 1), e = r.length && f(o), !e) return K.apply(n, r), n;
                        break
                    }
            }
            return (l || k(e, d))(r, t, !_, n, be.test(e) && c(t.parentNode) || t), n
        }, w.sortStable = P.split("").sort(U).join("") === P, w.detectDuplicates = !!j, L(), w.sortDetached = i(function(e) {
            return 1 & e.compareDocumentPosition(H.createElement("div"))
        }), i(function(e) {
            return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href")
        }) || o("type|href|height|width", function(e, t, n) {
            return n ? void 0 : e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2)
        }), w.attributes && i(function(e) {
            return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value")
        }) || o("value", function(e, t, n) {
            return n || "input" !== e.nodeName.toLowerCase() ? void 0 : e.defaultValue
        }), i(function(e) {
            return null == e.getAttribute("disabled")
        }) || o(te, function(e, t, n) {
            var r;
            return n ? void 0 : e[t] === !0 ? t.toLowerCase() : (r = e.getAttributeNode(t)) && r.specified ? r.value : null
        }), t
    }(e);
    ie.find = le, ie.expr = le.selectors, ie.expr[":"] = ie.expr.pseudos, ie.unique = le.uniqueSort, ie.text = le.getText, ie.isXMLDoc = le.isXML, ie.contains = le.contains;
    var ce = ie.expr.match.needsContext,
        de = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,
        fe = /^.[^:#\[\.,]*$/;
    ie.filter = function(e, t, n) {
        var r = t[0];
        return n && (e = ":not(" + e + ")"), 1 === t.length && 1 === r.nodeType ? ie.find.matchesSelector(r, e) ? [r] : [] : ie.find.matches(e, ie.grep(t, function(e) {
            return 1 === e.nodeType
        }))
    }, ie.fn.extend({
        find: function(e) {
            var t, n = [],
                r = this,
                i = r.length;
            if ("string" != typeof e) return this.pushStack(ie(e).filter(function() {
                for (t = 0; i > t; t++)
                    if (ie.contains(r[t], this)) return !0
            }));
            for (t = 0; i > t; t++) ie.find(e, r[t], n);
            return n = this.pushStack(i > 1 ? ie.unique(n) : n), n.selector = this.selector ? this.selector + " " + e : e, n
        },
        filter: function(e) {
            return this.pushStack(r(this, e || [], !1))
        },
        not: function(e) {
            return this.pushStack(r(this, e || [], !0))
        },
        is: function(e) {
            return !!r(this, "string" == typeof e && ce.test(e) ? ie(e) : e || [], !1).length
        }
    });
    var pe, he = e.document,
        me = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,
        ge = ie.fn.init = function(e, t) {
            var n, r;
            if (!e) return this;
            if ("string" == typeof e) {
                if (n = "<" === e.charAt(0) && ">" === e.charAt(e.length - 1) && e.length >= 3 ? [null, e, null] : me.exec(e), !n || !n[1] && t) return !t || t.jquery ? (t || pe).find(e) : this.constructor(t).find(e);
                if (n[1]) {
                    if (t = t instanceof ie ? t[0] : t, ie.merge(this, ie.parseHTML(n[1], t && t.nodeType ? t.ownerDocument || t : he, !0)), de.test(n[1]) && ie.isPlainObject(t))
                        for (n in t) ie.isFunction(this[n]) ? this[n](t[n]) : this.attr(n, t[n]);
                    return this
                }
                if (r = he.getElementById(n[2]), r && r.parentNode) {
                    if (r.id !== n[2]) return pe.find(e);
                    this.length = 1, this[0] = r
                }
                return this.context = he, this.selector = e, this
            }
            return e.nodeType ? (this.context = this[0] = e, this.length = 1, this) : ie.isFunction(e) ? "undefined" != typeof pe.ready ? pe.ready(e) : e(ie) : (void 0 !== e.selector && (this.selector = e.selector, this.context = e.context), ie.makeArray(e, this))
        };
    ge.prototype = ie.fn, pe = ie(he);
    var ve = /^(?:parents|prev(?:Until|All))/,
        ye = {
            children: !0,
            contents: !0,
            next: !0,
            prev: !0
        };
    ie.extend({
        dir: function(e, t, n) {
            for (var r = [], i = e[t]; i && 9 !== i.nodeType && (void 0 === n || 1 !== i.nodeType || !ie(i).is(n));) 1 === i.nodeType && r.push(i), i = i[t];
            return r
        },
        sibling: function(e, t) {
            for (var n = []; e; e = e.nextSibling) 1 === e.nodeType && e !== t && n.push(e);
            return n
        }
    }), ie.fn.extend({
        has: function(e) {
            var t, n = ie(e, this),
                r = n.length;
            return this.filter(function() {
                for (t = 0; r > t; t++)
                    if (ie.contains(this, n[t])) return !0
            })
        },
        closest: function(e, t) {
            for (var n, r = 0, i = this.length, o = [], a = ce.test(e) || "string" != typeof e ? ie(e, t || this.context) : 0; i > r; r++)
                for (n = this[r]; n && n !== t; n = n.parentNode)
                    if (n.nodeType < 11 && (a ? a.index(n) > -1 : 1 === n.nodeType && ie.find.matchesSelector(n, e))) {
                        o.push(n);
                        break
                    }
            return this.pushStack(o.length > 1 ? ie.unique(o) : o)
        },
        index: function(e) {
            return e ? "string" == typeof e ? ie.inArray(this[0], ie(e)) : ie.inArray(e.jquery ? e[0] : e, this) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        },
        add: function(e, t) {
            return this.pushStack(ie.unique(ie.merge(this.get(), ie(e, t))))
        },
        addBack: function(e) {
            return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
        }
    }), ie.each({
        parent: function(e) {
            var t = e.parentNode;
            return t && 11 !== t.nodeType ? t : null
        },
        parents: function(e) {
            return ie.dir(e, "parentNode")
        },
        parentsUntil: function(e, t, n) {
            return ie.dir(e, "parentNode", n)
        },
        next: function(e) {
            return i(e, "nextSibling")
        },
        prev: function(e) {
            return i(e, "previousSibling")
        },
        nextAll: function(e) {
            return ie.dir(e, "nextSibling")
        },
        prevAll: function(e) {
            return ie.dir(e, "previousSibling")
        },
        nextUntil: function(e, t, n) {
            return ie.dir(e, "nextSibling", n)
        },
        prevUntil: function(e, t, n) {
            return ie.dir(e, "previousSibling", n)
        },
        siblings: function(e) {
            return ie.sibling((e.parentNode || {}).firstChild, e)
        },
        children: function(e) {
            return ie.sibling(e.firstChild)
        },
        contents: function(e) {
            return ie.nodeName(e, "iframe") ? e.contentDocument || e.contentWindow.document : ie.merge([], e.childNodes)
        }
    }, function(e, t) {
        ie.fn[e] = function(n, r) {
            var i = ie.map(this, t, n);
            return "Until" !== e.slice(-5) && (r = n), r && "string" == typeof r && (i = ie.filter(r, i)), this.length > 1 && (ye[e] || (i = ie.unique(i)), ve.test(e) && (i = i.reverse())), this.pushStack(i)
        }
    });
    var be = /\S+/g,
        xe = {};
    ie.Callbacks = function(e) {
        e = "string" == typeof e ? xe[e] || o(e) : ie.extend({}, e);
        var t, n, r, i, a, s, u = [],
            l = !e.once && [],
            c = function(o) {
                for (n = e.memory && o, r = !0, a = s || 0, s = 0, i = u.length, t = !0; u && i > a; a++)
                    if (u[a].apply(o[0], o[1]) === !1 && e.stopOnFalse) {
                        n = !1;
                        break
                    }
                t = !1, u && (l ? l.length && c(l.shift()) : n ? u = [] : d.disable())
            },
            d = {
                add: function() {
                    if (u) {
                        var r = u.length;
                        ! function o(t) {
                            ie.each(t, function(t, n) {
                                var r = ie.type(n);
                                "function" === r ? e.unique && d.has(n) || u.push(n) : n && n.length && "string" !== r && o(n)
                            })
                        }(arguments), t ? i = u.length : n && (s = r, c(n))
                    }
                    return this
                },
                remove: function() {
                    return u && ie.each(arguments, function(e, n) {
                        for (var r;
                            (r = ie.inArray(n, u, r)) > -1;) u.splice(r, 1), t && (i >= r && i--, a >= r && a--)
                    }), this
                },
                has: function(e) {
                    return e ? ie.inArray(e, u) > -1 : !(!u || !u.length)
                },
                empty: function() {
                    return u = [], i = 0, this
                },
                disable: function() {
                    return u = l = n = void 0, this
                },
                disabled: function() {
                    return !u
                },
                lock: function() {
                    return l = void 0, n || d.disable(), this
                },
                locked: function() {
                    return !l
                },
                fireWith: function(e, n) {
                    return !u || r && !l || (n = n || [], n = [e, n.slice ? n.slice() : n], t ? l.push(n) : c(n)), this
                },
                fire: function() {
                    return d.fireWith(this, arguments), this
                },
                fired: function() {
                    return !!r
                }
            };
        return d
    }, ie.extend({
        Deferred: function(e) {
            var t = [
                    ["resolve", "done", ie.Callbacks("once memory"), "resolved"],
                    ["reject", "fail", ie.Callbacks("once memory"), "rejected"],
                    ["notify", "progress", ie.Callbacks("memory")]
                ],
                n = "pending",
                r = {
                    state: function() {
                        return n
                    },
                    always: function() {
                        return i.done(arguments).fail(arguments), this
                    },
                    then: function() {
                        var e = arguments;
                        return ie.Deferred(function(n) {
                            ie.each(t, function(t, o) {
                                var a = ie.isFunction(e[t]) && e[t];
                                i[o[1]](function() {
                                    var e = a && a.apply(this, arguments);
                                    e && ie.isFunction(e.promise) ? e.promise().done(n.resolve).fail(n.reject).progress(n.notify) : n[o[0] + "With"](this === r ? n.promise() : this, a ? [e] : arguments)
                                })
                            }), e = null
                        }).promise()
                    },
                    promise: function(e) {
                        return null != e ? ie.extend(e, r) : r
                    }
                },
                i = {};
            return r.pipe = r.then, ie.each(t, function(e, o) {
                var a = o[2],
                    s = o[3];
                r[o[1]] = a.add, s && a.add(function() {
                    n = s
                }, t[1 ^ e][2].disable, t[2][2].lock), i[o[0]] = function() {
                    return i[o[0] + "With"](this === i ? r : this, arguments), this
                }, i[o[0] + "With"] = a.fireWith
            }), r.promise(i), e && e.call(i, i), i
        },
        when: function(e) {
            var t, n, r, i = 0,
                o = Y.call(arguments),
                a = o.length,
                s = 1 !== a || e && ie.isFunction(e.promise) ? a : 0,
                u = 1 === s ? e : ie.Deferred(),
                l = function(e, n, r) {
                    return function(i) {
                        n[e] = this, r[e] = arguments.length > 1 ? Y.call(arguments) : i, r === t ? u.notifyWith(n, r) : --s || u.resolveWith(n, r)
                    }
                };
            if (a > 1)
                for (t = new Array(a), n = new Array(a), r = new Array(a); a > i; i++) o[i] && ie.isFunction(o[i].promise) ? o[i].promise().done(l(i, r, o)).fail(u.reject).progress(l(i, n, t)) : --s;
            return s || u.resolveWith(r, o), u.promise()
        }
    });
    var we;
    ie.fn.ready = function(e) {
        return ie.ready.promise().done(e), this
    }, ie.extend({
        isReady: !1,
        readyWait: 1,
        holdReady: function(e) {
            e ? ie.readyWait++ : ie.ready(!0)
        },
        ready: function(e) {
            if (e === !0 ? !--ie.readyWait : !ie.isReady) {
                if (!he.body) return setTimeout(ie.ready);
                ie.isReady = !0, e !== !0 && --ie.readyWait > 0 || (we.resolveWith(he, [ie]), ie.fn.triggerHandler && (ie(he).triggerHandler("ready"), ie(he).off("ready")))
            }
        }
    }), ie.ready.promise = function(t) {
        if (!we)
            if (we = ie.Deferred(), "complete" === he.readyState) setTimeout(ie.ready);
            else if (he.addEventListener) he.addEventListener("DOMContentLoaded", s, !1), e.addEventListener("load", s, !1);
        else {
            he.attachEvent("onreadystatechange", s), e.attachEvent("onload", s);
            var n = !1;
            try {
                n = null == e.frameElement && he.documentElement
            } catch (r) {}
            n && n.doScroll && ! function i() {
                if (!ie.isReady) {
                    try {
                        n.doScroll("left")
                    } catch (e) {
                        return setTimeout(i, 50)
                    }
                    a(), ie.ready()
                }
            }()
        }
        return we.promise(t)
    };
    var Te, Ce = "undefined";
    for (Te in ie(ne)) break;
    ne.ownLast = "0" !== Te, ne.inlineBlockNeedsLayout = !1, ie(function() {
            var e, t, n, r;
            n = he.getElementsByTagName("body")[0], n && n.style && (t = he.createElement("div"), r = he.createElement("div"), r.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", n.appendChild(r).appendChild(t), typeof t.style.zoom !== Ce && (t.style.cssText = "display:inline;margin:0;border:0;padding:1px;width:1px;zoom:1", ne.inlineBlockNeedsLayout = e = 3 === t.offsetWidth, e && (n.style.zoom = 1)), n.removeChild(r))
        }),
        function() {
            var e = he.createElement("div");
            if (null == ne.deleteExpando) {
                ne.deleteExpando = !0;
                try {
                    delete e.test
                } catch (t) {
                    ne.deleteExpando = !1
                }
            }
            e = null
        }(), ie.acceptData = function(e) {
            var t = ie.noData[(e.nodeName + " ").toLowerCase()],
                n = +e.nodeType || 1;
            return 1 !== n && 9 !== n ? !1 : !t || t !== !0 && e.getAttribute("classid") === t
        };
    var Ne = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
        Ee = /([A-Z])/g;
    ie.extend({
        cache: {},
        noData: {
            "applet ": !0,
            "embed ": !0,
            "object ": "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
        },
        hasData: function(e) {
            return e = e.nodeType ? ie.cache[e[ie.expando]] : e[ie.expando], !!e && !l(e)
        },
        data: function(e, t, n) {
            return c(e, t, n)
        },
        removeData: function(e, t) {
            return d(e, t)
        },
        _data: function(e, t, n) {
            return c(e, t, n, !0)
        },
        _removeData: function(e, t) {
            return d(e, t, !0)
        }
    }), ie.fn.extend({
        data: function(e, t) {
            var n, r, i, o = this[0],
                a = o && o.attributes;
            if (void 0 === e) {
                if (this.length && (i = ie.data(o), 1 === o.nodeType && !ie._data(o, "parsedAttrs"))) {
                    for (n = a.length; n--;) a[n] && (r = a[n].name, 0 === r.indexOf("data-") && (r = ie.camelCase(r.slice(5)), u(o, r, i[r])));
                    ie._data(o, "parsedAttrs", !0)
                }
                return i
            }
            return "object" == typeof e ? this.each(function() {
                ie.data(this, e)
            }) : arguments.length > 1 ? this.each(function() {
                ie.data(this, e, t)
            }) : o ? u(o, e, ie.data(o, e)) : void 0
        },
        removeData: function(e) {
            return this.each(function() {
                ie.removeData(this, e)
            })
        }
    }), ie.extend({
        queue: function(e, t, n) {
            var r;
            return e ? (t = (t || "fx") + "queue", r = ie._data(e, t), n && (!r || ie.isArray(n) ? r = ie._data(e, t, ie.makeArray(n)) : r.push(n)), r || []) : void 0
        },
        dequeue: function(e, t) {
            t = t || "fx";
            var n = ie.queue(e, t),
                r = n.length,
                i = n.shift(),
                o = ie._queueHooks(e, t),
                a = function() {
                    ie.dequeue(e, t)
                };
            "inprogress" === i && (i = n.shift(), r--), i && ("fx" === t && n.unshift("inprogress"), delete o.stop, i.call(e, a, o)), !r && o && o.empty.fire()
        },
        _queueHooks: function(e, t) {
            var n = t + "queueHooks";
            return ie._data(e, n) || ie._data(e, n, {
                empty: ie.Callbacks("once memory").add(function() {
                    ie._removeData(e, t + "queue"), ie._removeData(e, n)
                })
            })
        }
    }), ie.fn.extend({
        queue: function(e, t) {
            var n = 2;
            return "string" != typeof e && (t = e, e = "fx", n--), arguments.length < n ? ie.queue(this[0], e) : void 0 === t ? this : this.each(function() {
                var n = ie.queue(this, e, t);
                ie._queueHooks(this, e), "fx" === e && "inprogress" !== n[0] && ie.dequeue(this, e)
            })
        },
        dequeue: function(e) {
            return this.each(function() {
                ie.dequeue(this, e)
            })
        },
        clearQueue: function(e) {
            return this.queue(e || "fx", [])
        },
        promise: function(e, t) {
            var n, r = 1,
                i = ie.Deferred(),
                o = this,
                a = this.length,
                s = function() {
                    --r || i.resolveWith(o, [o])
                };
            for ("string" != typeof e && (t = e, e = void 0), e = e || "fx"; a--;) n = ie._data(o[a], e + "queueHooks"), n && n.empty && (r++, n.empty.add(s));
            return s(), i.promise(t)
        }
    });
    var ke = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
        Se = ["Top", "Right", "Bottom", "Left"],
        Ae = function(e, t) {
            return e = t || e, "none" === ie.css(e, "display") || !ie.contains(e.ownerDocument, e)
        },
        De = ie.access = function(e, t, n, r, i, o, a) {
            var s = 0,
                u = e.length,
                l = null == n;
            if ("object" === ie.type(n)) {
                i = !0;
                for (s in n) ie.access(e, t, s, n[s], !0, o, a)
            } else if (void 0 !== r && (i = !0, ie.isFunction(r) || (a = !0), l && (a ? (t.call(e, r), t = null) : (l = t, t = function(e, t, n) {
                    return l.call(ie(e), n)
                })), t))
                for (; u > s; s++) t(e[s], n, a ? r : r.call(e[s], s, t(e[s], n)));
            return i ? e : l ? t.call(e) : u ? t(e[0], n) : o
        },
        je = /^(?:checkbox|radio)$/i;
    ! function() {
        var e = he.createElement("input"),
            t = he.createElement("div"),
            n = he.createDocumentFragment();
        if (t.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", ne.leadingWhitespace = 3 === t.firstChild.nodeType, ne.tbody = !t.getElementsByTagName("tbody").length, ne.htmlSerialize = !!t.getElementsByTagName("link").length, ne.html5Clone = "<:nav></:nav>" !== he.createElement("nav").cloneNode(!0).outerHTML, e.type = "checkbox", e.checked = !0, n.appendChild(e), ne.appendChecked = e.checked, t.innerHTML = "<textarea>x</textarea>", ne.noCloneChecked = !!t.cloneNode(!0).lastChild.defaultValue, n.appendChild(t), t.innerHTML = "<input type='radio' checked='checked' name='t'/>", ne.checkClone = t.cloneNode(!0).cloneNode(!0).lastChild.checked, ne.noCloneEvent = !0, t.attachEvent && (t.attachEvent("onclick", function() {
                ne.noCloneEvent = !1
            }), t.cloneNode(!0).click()), null == ne.deleteExpando) {
            ne.deleteExpando = !0;
            try {
                delete t.test
            } catch (r) {
                ne.deleteExpando = !1
            }
        }
    }(),
    function() {
        var t, n, r = he.createElement("div");
        for (t in {
                submit: !0,
                change: !0,
                focusin: !0
            }) n = "on" + t, (ne[t + "Bubbles"] = n in e) || (r.setAttribute(n, "t"), ne[t + "Bubbles"] = r.attributes[n].expando === !1);
        r = null
    }();
    var Le = /^(?:input|select|textarea)$/i,
        He = /^key/,
        qe = /^(?:mouse|pointer|contextmenu)|click/,
        _e = /^(?:focusinfocus|focusoutblur)$/,
        Me = /^([^.]*)(?:\.(.+)|)$/;
    ie.event = {
        global: {},
        add: function(e, t, n, r, i) {
            var o, a, s, u, l, c, d, f, p, h, m, g = ie._data(e);
            if (g) {
                for (n.handler && (u = n, n = u.handler, i = u.selector), n.guid || (n.guid = ie.guid++), (a = g.events) || (a = g.events = {}), (c = g.handle) || (c = g.handle = function(e) {
                        return typeof ie === Ce || e && ie.event.triggered === e.type ? void 0 : ie.event.dispatch.apply(c.elem, arguments)
                    }, c.elem = e), t = (t || "").match(be) || [""], s = t.length; s--;) o = Me.exec(t[s]) || [], p = m = o[1], h = (o[2] || "").split(".").sort(), p && (l = ie.event.special[p] || {}, p = (i ? l.delegateType : l.bindType) || p, l = ie.event.special[p] || {}, d = ie.extend({
                    type: p,
                    origType: m,
                    data: r,
                    handler: n,
                    guid: n.guid,
                    selector: i,
                    needsContext: i && ie.expr.match.needsContext.test(i),
                    namespace: h.join(".")
                }, u), (f = a[p]) || (f = a[p] = [], f.delegateCount = 0, l.setup && l.setup.call(e, r, h, c) !== !1 || (e.addEventListener ? e.addEventListener(p, c, !1) : e.attachEvent && e.attachEvent("on" + p, c))), l.add && (l.add.call(e, d), d.handler.guid || (d.handler.guid = n.guid)), i ? f.splice(f.delegateCount++, 0, d) : f.push(d), ie.event.global[p] = !0);
                e = null
            }
        },
        remove: function(e, t, n, r, i) {
            var o, a, s, u, l, c, d, f, p, h, m, g = ie.hasData(e) && ie._data(e);
            if (g && (c = g.events)) {
                for (t = (t || "").match(be) || [""], l = t.length; l--;)
                    if (s = Me.exec(t[l]) || [], p = m = s[1], h = (s[2] || "").split(".").sort(), p) {
                        for (d = ie.event.special[p] || {}, p = (r ? d.delegateType : d.bindType) || p, f = c[p] || [], s = s[2] && new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)"), u = o = f.length; o--;) a = f[o], !i && m !== a.origType || n && n.guid !== a.guid || s && !s.test(a.namespace) || r && r !== a.selector && ("**" !== r || !a.selector) || (f.splice(o, 1), a.selector && f.delegateCount--, d.remove && d.remove.call(e, a));
                        u && !f.length && (d.teardown && d.teardown.call(e, h, g.handle) !== !1 || ie.removeEvent(e, p, g.handle), delete c[p])
                    } else
                        for (p in c) ie.event.remove(e, p + t[l], n, r, !0);
                ie.isEmptyObject(c) && (delete g.handle, ie._removeData(e, "events"))
            }
        },
        trigger: function(t, n, r, i) {
            var o, a, s, u, l, c, d, f = [r || he],
                p = te.call(t, "type") ? t.type : t,
                h = te.call(t, "namespace") ? t.namespace.split(".") : [];
            if (s = c = r = r || he, 3 !== r.nodeType && 8 !== r.nodeType && !_e.test(p + ie.event.triggered) && (p.indexOf(".") >= 0 && (h = p.split("."), p = h.shift(), h.sort()), a = p.indexOf(":") < 0 && "on" + p, t = t[ie.expando] ? t : new ie.Event(p, "object" == typeof t && t), t.isTrigger = i ? 2 : 3, t.namespace = h.join("."), t.namespace_re = t.namespace ? new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, t.result = void 0, t.target || (t.target = r), n = null == n ? [t] : ie.makeArray(n, [t]), l = ie.event.special[p] || {}, i || !l.trigger || l.trigger.apply(r, n) !== !1)) {
                if (!i && !l.noBubble && !ie.isWindow(r)) {
                    for (u = l.delegateType || p, _e.test(u + p) || (s = s.parentNode); s; s = s.parentNode) f.push(s), c = s;
                    c === (r.ownerDocument || he) && f.push(c.defaultView || c.parentWindow || e)
                }
                for (d = 0;
                    (s = f[d++]) && !t.isPropagationStopped();) t.type = d > 1 ? u : l.bindType || p, o = (ie._data(s, "events") || {})[t.type] && ie._data(s, "handle"), o && o.apply(s, n), o = a && s[a], o && o.apply && ie.acceptData(s) && (t.result = o.apply(s, n), t.result === !1 && t.preventDefault());
                if (t.type = p, !i && !t.isDefaultPrevented() && (!l._default || l._default.apply(f.pop(), n) === !1) && ie.acceptData(r) && a && r[p] && !ie.isWindow(r)) {
                    c = r[a], c && (r[a] = null), ie.event.triggered = p;
                    try {
                        r[p]()
                    } catch (m) {}
                    ie.event.triggered = void 0, c && (r[a] = c)
                }
                return t.result
            }
        },
        dispatch: function(e) {
            e = ie.event.fix(e);
            var t, n, r, i, o, a = [],
                s = Y.call(arguments),
                u = (ie._data(this, "events") || {})[e.type] || [],
                l = ie.event.special[e.type] || {};
            if (s[0] = e, e.delegateTarget = this, !l.preDispatch || l.preDispatch.call(this, e) !== !1) {
                for (a = ie.event.handlers.call(this, e, u), t = 0;
                    (i = a[t++]) && !e.isPropagationStopped();)
                    for (e.currentTarget = i.elem, o = 0;
                        (r = i.handlers[o++]) && !e.isImmediatePropagationStopped();)(!e.namespace_re || e.namespace_re.test(r.namespace)) && (e.handleObj = r, e.data = r.data, n = ((ie.event.special[r.origType] || {}).handle || r.handler).apply(i.elem, s), void 0 !== n && (e.result = n) === !1 && (e.preventDefault(), e.stopPropagation()));
                return l.postDispatch && l.postDispatch.call(this, e), e.result
            }
        },
        handlers: function(e, t) {
            var n, r, i, o, a = [],
                s = t.delegateCount,
                u = e.target;
            if (s && u.nodeType && (!e.button || "click" !== e.type))
                for (; u != this; u = u.parentNode || this)
                    if (1 === u.nodeType && (u.disabled !== !0 || "click" !== e.type)) {
                        for (i = [], o = 0; s > o; o++) r = t[o], n = r.selector + " ", void 0 === i[n] && (i[n] = r.needsContext ? ie(n, this).index(u) >= 0 : ie.find(n, this, null, [u]).length), i[n] && i.push(r);
                        i.length && a.push({
                            elem: u,
                            handlers: i
                        })
                    }
            return s < t.length && a.push({
                elem: this,
                handlers: t.slice(s)
            }), a
        },
        fix: function(e) {
            if (e[ie.expando]) return e;
            var t, n, r, i = e.type,
                o = e,
                a = this.fixHooks[i];
            for (a || (this.fixHooks[i] = a = qe.test(i) ? this.mouseHooks : He.test(i) ? this.keyHooks : {}), r = a.props ? this.props.concat(a.props) : this.props, e = new ie.Event(o), t = r.length; t--;) n = r[t], e[n] = o[n];
            return e.target || (e.target = o.srcElement || he), 3 === e.target.nodeType && (e.target = e.target.parentNode), e.metaKey = !!e.metaKey, a.filter ? a.filter(e, o) : e
        },
        props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
        fixHooks: {},
        keyHooks: {
            props: "char charCode key keyCode".split(" "),
            filter: function(e, t) {
                return null == e.which && (e.which = null != t.charCode ? t.charCode : t.keyCode), e
            }
        },
        mouseHooks: {
            props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
            filter: function(e, t) {
                var n, r, i, o = t.button,
                    a = t.fromElement;
                return null == e.pageX && null != t.clientX && (r = e.target.ownerDocument || he, i = r.documentElement, n = r.body, e.pageX = t.clientX + (i && i.scrollLeft || n && n.scrollLeft || 0) - (i && i.clientLeft || n && n.clientLeft || 0), e.pageY = t.clientY + (i && i.scrollTop || n && n.scrollTop || 0) - (i && i.clientTop || n && n.clientTop || 0)), !e.relatedTarget && a && (e.relatedTarget = a === e.target ? t.toElement : a), e.which || void 0 === o || (e.which = 1 & o ? 1 : 2 & o ? 3 : 4 & o ? 2 : 0), e
            }
        },
        special: {
            load: {
                noBubble: !0
            },
            focus: {
                trigger: function() {
                    if (this !== h() && this.focus) try {
                        return this.focus(), !1
                    } catch (e) {}
                },
                delegateType: "focusin"
            },
            blur: {
                trigger: function() {
                    return this === h() && this.blur ? (this.blur(), !1) : void 0
                },
                delegateType: "focusout"
            },
            click: {
                trigger: function() {
                    return ie.nodeName(this, "input") && "checkbox" === this.type && this.click ? (this.click(), !1) : void 0
                },
                _default: function(e) {
                    return ie.nodeName(e.target, "a")
                }
            },
            beforeunload: {
                postDispatch: function(e) {
                    void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result)
                }
            }
        },
        simulate: function(e, t, n, r) {
            var i = ie.extend(new ie.Event, n, {
                type: e,
                isSimulated: !0,
                originalEvent: {}
            });
            r ? ie.event.trigger(i, null, t) : ie.event.dispatch.call(t, i), i.isDefaultPrevented() && n.preventDefault()
        }
    }, ie.removeEvent = he.removeEventListener ? function(e, t, n) {
        e.removeEventListener && e.removeEventListener(t, n, !1)
    } : function(e, t, n) {
        var r = "on" + t;
        e.detachEvent && (typeof e[r] === Ce && (e[r] = null), e.detachEvent(r, n))
    }, ie.Event = function(e, t) {
        return this instanceof ie.Event ? (e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && e.returnValue === !1 ? f : p) : this.type = e, t && ie.extend(this, t), this.timeStamp = e && e.timeStamp || ie.now(), void(this[ie.expando] = !0)) : new ie.Event(e, t)
    }, ie.Event.prototype = {
        isDefaultPrevented: p,
        isPropagationStopped: p,
        isImmediatePropagationStopped: p,
        preventDefault: function() {
            var e = this.originalEvent;
            this.isDefaultPrevented = f, e && (e.preventDefault ? e.preventDefault() : e.returnValue = !1)
        },
        stopPropagation: function() {
            var e = this.originalEvent;
            this.isPropagationStopped = f, e && (e.stopPropagation && e.stopPropagation(), e.cancelBubble = !0)
        },
        stopImmediatePropagation: function() {
            var e = this.originalEvent;
            this.isImmediatePropagationStopped = f, e && e.stopImmediatePropagation && e.stopImmediatePropagation(), this.stopPropagation()
        }
    }, ie.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function(e, t) {
        ie.event.special[e] = {
            delegateType: t,
            bindType: t,
            handle: function(e) {
                var n, r = this,
                    i = e.relatedTarget,
                    o = e.handleObj;
                return (!i || i !== r && !ie.contains(r, i)) && (e.type = o.origType, n = o.handler.apply(this, arguments), e.type = t), n
            }
        }
    }), ne.submitBubbles || (ie.event.special.submit = {
        setup: function() {
            return ie.nodeName(this, "form") ? !1 : void ie.event.add(this, "click._submit keypress._submit", function(e) {
                var t = e.target,
                    n = ie.nodeName(t, "input") || ie.nodeName(t, "button") ? t.form : void 0;
                n && !ie._data(n, "submitBubbles") && (ie.event.add(n, "submit._submit", function(e) {
                    e._submit_bubble = !0
                }), ie._data(n, "submitBubbles", !0))
            })
        },
        postDispatch: function(e) {
            e._submit_bubble && (delete e._submit_bubble, this.parentNode && !e.isTrigger && ie.event.simulate("submit", this.parentNode, e, !0))
        },
        teardown: function() {
            return ie.nodeName(this, "form") ? !1 : void ie.event.remove(this, "._submit")
        }
    }), ne.changeBubbles || (ie.event.special.change = {
        setup: function() {
            return Le.test(this.nodeName) ? (("checkbox" === this.type || "radio" === this.type) && (ie.event.add(this, "propertychange._change", function(e) {
                "checked" === e.originalEvent.propertyName && (this._just_changed = !0)
            }), ie.event.add(this, "click._change", function(e) {
                this._just_changed && !e.isTrigger && (this._just_changed = !1), ie.event.simulate("change", this, e, !0)
            })), !1) : void ie.event.add(this, "beforeactivate._change", function(e) {
                var t = e.target;
                Le.test(t.nodeName) && !ie._data(t, "changeBubbles") && (ie.event.add(t, "change._change", function(e) {
                    !this.parentNode || e.isSimulated || e.isTrigger || ie.event.simulate("change", this.parentNode, e, !0)
                }), ie._data(t, "changeBubbles", !0))
            })
        },
        handle: function(e) {
            var t = e.target;
            return this !== t || e.isSimulated || e.isTrigger || "radio" !== t.type && "checkbox" !== t.type ? e.handleObj.handler.apply(this, arguments) : void 0
        },
        teardown: function() {
            return ie.event.remove(this, "._change"), !Le.test(this.nodeName)
        }
    }), ne.focusinBubbles || ie.each({
        focus: "focusin",
        blur: "focusout"
    }, function(e, t) {
        var n = function(e) {
            ie.event.simulate(t, e.target, ie.event.fix(e), !0)
        };
        ie.event.special[t] = {
            setup: function() {
                var r = this.ownerDocument || this,
                    i = ie._data(r, t);
                i || r.addEventListener(e, n, !0), ie._data(r, t, (i || 0) + 1)
            },
            teardown: function() {
                var r = this.ownerDocument || this,
                    i = ie._data(r, t) - 1;
                i ? ie._data(r, t, i) : (r.removeEventListener(e, n, !0), ie._removeData(r, t))
            }
        }
    }), ie.fn.extend({
        on: function(e, t, n, r, i) {
            var o, a;
            if ("object" == typeof e) {
                "string" != typeof t && (n = n || t, t = void 0);
                for (o in e) this.on(o, t, n, e[o], i);
                return this
            }
            if (null == n && null == r ? (r = t, n = t = void 0) : null == r && ("string" == typeof t ? (r = n, n = void 0) : (r = n, n = t, t = void 0)), r === !1) r = p;
            else if (!r) return this;
            return 1 === i && (a = r, r = function(e) {
                return ie().off(e), a.apply(this, arguments)
            }, r.guid = a.guid || (a.guid = ie.guid++)), this.each(function() {
                ie.event.add(this, e, r, n, t)
            })
        },
        one: function(e, t, n, r) {
            return this.on(e, t, n, r, 1)
        },
        off: function(e, t, n) {
            var r, i;
            if (e && e.preventDefault && e.handleObj) return r = e.handleObj, ie(e.delegateTarget).off(r.namespace ? r.origType + "." + r.namespace : r.origType, r.selector, r.handler), this;
            if ("object" == typeof e) {
                for (i in e) this.off(i, t, e[i]);
                return this
            }
            return (t === !1 || "function" == typeof t) && (n = t, t = void 0), n === !1 && (n = p), this.each(function() {
                ie.event.remove(this, e, n, t)
            })
        },
        trigger: function(e, t) {
            return this.each(function() {
                ie.event.trigger(e, t, this)
            })
        },
        triggerHandler: function(e, t) {
            var n = this[0];
            return n ? ie.event.trigger(e, t, n, !0) : void 0
        }
    });
    var Fe = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
        Oe = / jQuery\d+="(?:null|\d+)"/g,
        Be = new RegExp("<(?:" + Fe + ")[\\s/>]", "i"),
        Pe = /^\s+/,
        Re = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
        We = /<([\w:]+)/,
        $e = /<tbody/i,
        ze = /<|&#?\w+;/,
        Ie = /<(?:script|style|link)/i,
        Xe = /checked\s*(?:[^=]|=\s*.checked.)/i,
        Ue = /^$|\/(?:java|ecma)script/i,
        Ve = /^true\/(.*)/,
        Je = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,
        Ye = {
            option: [1, "<select multiple='multiple'>", "</select>"],
            legend: [1, "<fieldset>", "</fieldset>"],
            area: [1, "<map>", "</map>"],
            param: [1, "<object>", "</object>"],
            thead: [1, "<table>", "</table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            _default: ne.htmlSerialize ? [0, "", ""] : [1, "X<div>", "</div>"]
        },
        Ge = m(he),
        Qe = Ge.appendChild(he.createElement("div"));
    Ye.optgroup = Ye.option, Ye.tbody = Ye.tfoot = Ye.colgroup = Ye.caption = Ye.thead, Ye.th = Ye.td, ie.extend({
        clone: function(e, t, n) {
            var r, i, o, a, s, u = ie.contains(e.ownerDocument, e);
            if (ne.html5Clone || ie.isXMLDoc(e) || !Be.test("<" + e.nodeName + ">") ? o = e.cloneNode(!0) : (Qe.innerHTML = e.outerHTML, Qe.removeChild(o = Qe.firstChild)), !(ne.noCloneEvent && ne.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || ie.isXMLDoc(e)))
                for (r = g(o), s = g(e), a = 0; null != (i = s[a]); ++a) r[a] && C(i, r[a]);
            if (t)
                if (n)
                    for (s = s || g(e), r = r || g(o), a = 0; null != (i = s[a]); a++) T(i, r[a]);
                else T(e, o);
            return r = g(o, "script"), r.length > 0 && w(r, !u && g(e, "script")), r = s = i = null, o
        },
        buildFragment: function(e, t, n, r) {
            for (var i, o, a, s, u, l, c, d = e.length, f = m(t), p = [], h = 0; d > h; h++)
                if (o = e[h], o || 0 === o)
                    if ("object" === ie.type(o)) ie.merge(p, o.nodeType ? [o] : o);
                    else if (ze.test(o)) {
                for (s = s || f.appendChild(t.createElement("div")), u = (We.exec(o) || ["", ""])[1].toLowerCase(), c = Ye[u] || Ye._default, s.innerHTML = c[1] + o.replace(Re, "<$1></$2>") + c[2], i = c[0]; i--;) s = s.lastChild;
                if (!ne.leadingWhitespace && Pe.test(o) && p.push(t.createTextNode(Pe.exec(o)[0])), !ne.tbody)
                    for (o = "table" !== u || $e.test(o) ? "<table>" !== c[1] || $e.test(o) ? 0 : s : s.firstChild, i = o && o.childNodes.length; i--;) ie.nodeName(l = o.childNodes[i], "tbody") && !l.childNodes.length && o.removeChild(l);
                for (ie.merge(p, s.childNodes), s.textContent = ""; s.firstChild;) s.removeChild(s.firstChild);
                s = f.lastChild
            } else p.push(t.createTextNode(o));
            for (s && f.removeChild(s), ne.appendChecked || ie.grep(g(p, "input"), v), h = 0; o = p[h++];)
                if ((!r || -1 === ie.inArray(o, r)) && (a = ie.contains(o.ownerDocument, o), s = g(f.appendChild(o), "script"), a && w(s), n))
                    for (i = 0; o = s[i++];) Ue.test(o.type || "") && n.push(o);
            return s = null, f
        },
        cleanData: function(e, t) {
            for (var n, r, i, o, a = 0, s = ie.expando, u = ie.cache, l = ne.deleteExpando, c = ie.event.special; null != (n = e[a]); a++)
                if ((t || ie.acceptData(n)) && (i = n[s], o = i && u[i])) {
                    if (o.events)
                        for (r in o.events) c[r] ? ie.event.remove(n, r) : ie.removeEvent(n, r, o.handle);
                    u[i] && (delete u[i], l ? delete n[s] : typeof n.removeAttribute !== Ce ? n.removeAttribute(s) : n[s] = null, J.push(i))
                }
        }
    }), ie.fn.extend({
        text: function(e) {
            return De(this, function(e) {
                return void 0 === e ? ie.text(this) : this.empty().append((this[0] && this[0].ownerDocument || he).createTextNode(e))
            }, null, e, arguments.length)
        },
        append: function() {
            return this.domManip(arguments, function(e) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var t = y(this, e);
                    t.appendChild(e)
                }
            })
        },
        prepend: function() {
            return this.domManip(arguments, function(e) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var t = y(this, e);
                    t.insertBefore(e, t.firstChild)
                }
            })
        },
        before: function() {
            return this.domManip(arguments, function(e) {
                this.parentNode && this.parentNode.insertBefore(e, this)
            })
        },
        after: function() {
            return this.domManip(arguments, function(e) {
                this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
            })
        },
        remove: function(e, t) {
            for (var n, r = e ? ie.filter(e, this) : this, i = 0; null != (n = r[i]); i++) t || 1 !== n.nodeType || ie.cleanData(g(n)), n.parentNode && (t && ie.contains(n.ownerDocument, n) && w(g(n, "script")), n.parentNode.removeChild(n));
            return this
        },
        empty: function() {
            for (var e, t = 0; null != (e = this[t]); t++) {
                for (1 === e.nodeType && ie.cleanData(g(e, !1)); e.firstChild;) e.removeChild(e.firstChild);
                e.options && ie.nodeName(e, "select") && (e.options.length = 0)
            }
            return this
        },
        clone: function(e, t) {
            return e = null == e ? !1 : e, t = null == t ? e : t, this.map(function() {
                return ie.clone(this, e, t)
            })
        },
        html: function(e) {
            return De(this, function(e) {
                var t = this[0] || {},
                    n = 0,
                    r = this.length;
                if (void 0 === e) return 1 === t.nodeType ? t.innerHTML.replace(Oe, "") : void 0;
                if (!("string" != typeof e || Ie.test(e) || !ne.htmlSerialize && Be.test(e) || !ne.leadingWhitespace && Pe.test(e) || Ye[(We.exec(e) || ["", ""])[1].toLowerCase()])) {
                    e = e.replace(Re, "<$1></$2>");
                    try {
                        for (; r > n; n++) t = this[n] || {}, 1 === t.nodeType && (ie.cleanData(g(t, !1)), t.innerHTML = e);
                        t = 0
                    } catch (i) {}
                }
                t && this.empty().append(e)
            }, null, e, arguments.length)
        },
        replaceWith: function() {
            var e = arguments[0];
            return this.domManip(arguments, function(t) {
                e = this.parentNode, ie.cleanData(g(this)), e && e.replaceChild(t, this)
            }), e && (e.length || e.nodeType) ? this : this.remove()
        },
        detach: function(e) {
            return this.remove(e, !0)
        },
        domManip: function(e, t) {
            e = G.apply([], e);
            var n, r, i, o, a, s, u = 0,
                l = this.length,
                c = this,
                d = l - 1,
                f = e[0],
                p = ie.isFunction(f);
            if (p || l > 1 && "string" == typeof f && !ne.checkClone && Xe.test(f)) return this.each(function(n) {
                var r = c.eq(n);
                p && (e[0] = f.call(this, n, r.html())), r.domManip(e, t)
            });
            if (l && (s = ie.buildFragment(e, this[0].ownerDocument, !1, this), n = s.firstChild, 1 === s.childNodes.length && (s = n), n)) {
                for (o = ie.map(g(s, "script"), b), i = o.length; l > u; u++) r = s, u !== d && (r = ie.clone(r, !0, !0), i && ie.merge(o, g(r, "script"))), t.call(this[u], r, u);
                if (i)
                    for (a = o[o.length - 1].ownerDocument, ie.map(o, x), u = 0; i > u; u++) r = o[u], Ue.test(r.type || "") && !ie._data(r, "globalEval") && ie.contains(a, r) && (r.src ? ie._evalUrl && ie._evalUrl(r.src) : ie.globalEval((r.text || r.textContent || r.innerHTML || "").replace(Je, "")));
                s = n = null
            }
            return this
        }
    }), ie.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function(e, t) {
        ie.fn[e] = function(e) {
            for (var n, r = 0, i = [], o = ie(e), a = o.length - 1; a >= r; r++) n = r === a ? this : this.clone(!0), ie(o[r])[t](n), Q.apply(i, n.get());
            return this.pushStack(i)
        }
    });
    var Ke, Ze = {};
    ! function() {
        var e;
        ne.shrinkWrapBlocks = function() {
            if (null != e) return e;
            e = !1;
            var t, n, r;
            return n = he.getElementsByTagName("body")[0], n && n.style ? (t = he.createElement("div"), r = he.createElement("div"), r.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", n.appendChild(r).appendChild(t), typeof t.style.zoom !== Ce && (t.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:1px;width:1px;zoom:1", t.appendChild(he.createElement("div")).style.width = "5px", e = 3 !== t.offsetWidth), n.removeChild(r), e) : void 0
        }
    }();
    var et, tt, nt = /^margin/,
        rt = new RegExp("^(" + ke + ")(?!px)[a-z%]+$", "i"),
        it = /^(top|right|bottom|left)$/;
    e.getComputedStyle ? (et = function(t) {
            return t.ownerDocument.defaultView.opener ? t.ownerDocument.defaultView.getComputedStyle(t, null) : e.getComputedStyle(t, null)
        }, tt = function(e, t, n) {
            var r, i, o, a, s = e.style;
            return n = n || et(e), a = n ? n.getPropertyValue(t) || n[t] : void 0, n && ("" !== a || ie.contains(e.ownerDocument, e) || (a = ie.style(e, t)), rt.test(a) && nt.test(t) && (r = s.width, i = s.minWidth,
                o = s.maxWidth, s.minWidth = s.maxWidth = s.width = a, a = n.width, s.width = r, s.minWidth = i, s.maxWidth = o)), void 0 === a ? a : a + ""
        }) : he.documentElement.currentStyle && (et = function(e) {
            return e.currentStyle
        }, tt = function(e, t, n) {
            var r, i, o, a, s = e.style;
            return n = n || et(e), a = n ? n[t] : void 0, null == a && s && s[t] && (a = s[t]), rt.test(a) && !it.test(t) && (r = s.left, i = e.runtimeStyle, o = i && i.left, o && (i.left = e.currentStyle.left), s.left = "fontSize" === t ? "1em" : a, a = s.pixelLeft + "px", s.left = r, o && (i.left = o)), void 0 === a ? a : a + "" || "auto"
        }),
        function() {
            function t() {
                var t, n, r, i;
                n = he.getElementsByTagName("body")[0], n && n.style && (t = he.createElement("div"), r = he.createElement("div"), r.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", n.appendChild(r).appendChild(t), t.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;margin-top:1%;top:1%;border:1px;padding:1px;width:4px;position:absolute", o = a = !1, u = !0, e.getComputedStyle && (o = "1%" !== (e.getComputedStyle(t, null) || {}).top, a = "4px" === (e.getComputedStyle(t, null) || {
                    width: "4px"
                }).width, i = t.appendChild(he.createElement("div")), i.style.cssText = t.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0", i.style.marginRight = i.style.width = "0", t.style.width = "1px", u = !parseFloat((e.getComputedStyle(i, null) || {}).marginRight), t.removeChild(i)), t.innerHTML = "<table><tr><td></td><td>t</td></tr></table>", i = t.getElementsByTagName("td"), i[0].style.cssText = "margin:0;border:0;padding:0;display:none", s = 0 === i[0].offsetHeight, s && (i[0].style.display = "", i[1].style.display = "none", s = 0 === i[0].offsetHeight), n.removeChild(r))
            }
            var n, r, i, o, a, s, u;
            n = he.createElement("div"), n.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", i = n.getElementsByTagName("a")[0], r = i && i.style, r && (r.cssText = "float:left;opacity:.5", ne.opacity = "0.5" === r.opacity, ne.cssFloat = !!r.cssFloat, n.style.backgroundClip = "content-box", n.cloneNode(!0).style.backgroundClip = "", ne.clearCloneStyle = "content-box" === n.style.backgroundClip, ne.boxSizing = "" === r.boxSizing || "" === r.MozBoxSizing || "" === r.WebkitBoxSizing, ie.extend(ne, {
                reliableHiddenOffsets: function() {
                    return null == s && t(), s
                },
                boxSizingReliable: function() {
                    return null == a && t(), a
                },
                pixelPosition: function() {
                    return null == o && t(), o
                },
                reliableMarginRight: function() {
                    return null == u && t(), u
                }
            }))
        }(), ie.swap = function(e, t, n, r) {
            var i, o, a = {};
            for (o in t) a[o] = e.style[o], e.style[o] = t[o];
            i = n.apply(e, r || []);
            for (o in t) e.style[o] = a[o];
            return i
        };
    var ot = /alpha\([^)]*\)/i,
        at = /opacity\s*=\s*([^)]*)/,
        st = /^(none|table(?!-c[ea]).+)/,
        ut = new RegExp("^(" + ke + ")(.*)$", "i"),
        lt = new RegExp("^([+-])=(" + ke + ")", "i"),
        ct = {
            position: "absolute",
            visibility: "hidden",
            display: "block"
        },
        dt = {
            letterSpacing: "0",
            fontWeight: "400"
        },
        ft = ["Webkit", "O", "Moz", "ms"];
    ie.extend({
        cssHooks: {
            opacity: {
                get: function(e, t) {
                    if (t) {
                        var n = tt(e, "opacity");
                        return "" === n ? "1" : n
                    }
                }
            }
        },
        cssNumber: {
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {
            "float": ne.cssFloat ? "cssFloat" : "styleFloat"
        },
        style: function(e, t, n, r) {
            if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                var i, o, a, s = ie.camelCase(t),
                    u = e.style;
                if (t = ie.cssProps[s] || (ie.cssProps[s] = S(u, s)), a = ie.cssHooks[t] || ie.cssHooks[s], void 0 === n) return a && "get" in a && void 0 !== (i = a.get(e, !1, r)) ? i : u[t];
                if (o = typeof n, "string" === o && (i = lt.exec(n)) && (n = (i[1] + 1) * i[2] + parseFloat(ie.css(e, t)), o = "number"), null != n && n === n && ("number" !== o || ie.cssNumber[s] || (n += "px"), ne.clearCloneStyle || "" !== n || 0 !== t.indexOf("background") || (u[t] = "inherit"), !(a && "set" in a && void 0 === (n = a.set(e, n, r))))) try {
                    u[t] = n
                } catch (l) {}
            }
        },
        css: function(e, t, n, r) {
            var i, o, a, s = ie.camelCase(t);
            return t = ie.cssProps[s] || (ie.cssProps[s] = S(e.style, s)), a = ie.cssHooks[t] || ie.cssHooks[s], a && "get" in a && (o = a.get(e, !0, n)), void 0 === o && (o = tt(e, t, r)), "normal" === o && t in dt && (o = dt[t]), "" === n || n ? (i = parseFloat(o), n === !0 || ie.isNumeric(i) ? i || 0 : o) : o
        }
    }), ie.each(["height", "width"], function(e, t) {
        ie.cssHooks[t] = {
            get: function(e, n, r) {
                return n ? st.test(ie.css(e, "display")) && 0 === e.offsetWidth ? ie.swap(e, ct, function() {
                    return L(e, t, r)
                }) : L(e, t, r) : void 0
            },
            set: function(e, n, r) {
                var i = r && et(e);
                return D(e, n, r ? j(e, t, r, ne.boxSizing && "border-box" === ie.css(e, "boxSizing", !1, i), i) : 0)
            }
        }
    }), ne.opacity || (ie.cssHooks.opacity = {
        get: function(e, t) {
            return at.test((t && e.currentStyle ? e.currentStyle.filter : e.style.filter) || "") ? .01 * parseFloat(RegExp.$1) + "" : t ? "1" : ""
        },
        set: function(e, t) {
            var n = e.style,
                r = e.currentStyle,
                i = ie.isNumeric(t) ? "alpha(opacity=" + 100 * t + ")" : "",
                o = r && r.filter || n.filter || "";
            n.zoom = 1, (t >= 1 || "" === t) && "" === ie.trim(o.replace(ot, "")) && n.removeAttribute && (n.removeAttribute("filter"), "" === t || r && !r.filter) || (n.filter = ot.test(o) ? o.replace(ot, i) : o + " " + i)
        }
    }), ie.cssHooks.marginRight = k(ne.reliableMarginRight, function(e, t) {
        return t ? ie.swap(e, {
            display: "inline-block"
        }, tt, [e, "marginRight"]) : void 0
    }), ie.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function(e, t) {
        ie.cssHooks[e + t] = {
            expand: function(n) {
                for (var r = 0, i = {}, o = "string" == typeof n ? n.split(" ") : [n]; 4 > r; r++) i[e + Se[r] + t] = o[r] || o[r - 2] || o[0];
                return i
            }
        }, nt.test(e) || (ie.cssHooks[e + t].set = D)
    }), ie.fn.extend({
        css: function(e, t) {
            return De(this, function(e, t, n) {
                var r, i, o = {},
                    a = 0;
                if (ie.isArray(t)) {
                    for (r = et(e), i = t.length; i > a; a++) o[t[a]] = ie.css(e, t[a], !1, r);
                    return o
                }
                return void 0 !== n ? ie.style(e, t, n) : ie.css(e, t)
            }, e, t, arguments.length > 1)
        },
        show: function() {
            return A(this, !0)
        },
        hide: function() {
            return A(this)
        },
        toggle: function(e) {
            return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function() {
                Ae(this) ? ie(this).show() : ie(this).hide()
            })
        }
    }), ie.Tween = H, H.prototype = {
        constructor: H,
        init: function(e, t, n, r, i, o) {
            this.elem = e, this.prop = n, this.easing = i || "swing", this.options = t, this.start = this.now = this.cur(), this.end = r, this.unit = o || (ie.cssNumber[n] ? "" : "px")
        },
        cur: function() {
            var e = H.propHooks[this.prop];
            return e && e.get ? e.get(this) : H.propHooks._default.get(this)
        },
        run: function(e) {
            var t, n = H.propHooks[this.prop];
            return this.pos = t = this.options.duration ? ie.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : H.propHooks._default.set(this), this
        }
    }, H.prototype.init.prototype = H.prototype, H.propHooks = {
        _default: {
            get: function(e) {
                var t;
                return null == e.elem[e.prop] || e.elem.style && null != e.elem.style[e.prop] ? (t = ie.css(e.elem, e.prop, ""), t && "auto" !== t ? t : 0) : e.elem[e.prop]
            },
            set: function(e) {
                ie.fx.step[e.prop] ? ie.fx.step[e.prop](e) : e.elem.style && (null != e.elem.style[ie.cssProps[e.prop]] || ie.cssHooks[e.prop]) ? ie.style(e.elem, e.prop, e.now + e.unit) : e.elem[e.prop] = e.now
            }
        }
    }, H.propHooks.scrollTop = H.propHooks.scrollLeft = {
        set: function(e) {
            e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
        }
    }, ie.easing = {
        linear: function(e) {
            return e
        },
        swing: function(e) {
            return .5 - Math.cos(e * Math.PI) / 2
        }
    }, ie.fx = H.prototype.init, ie.fx.step = {};
    var pt, ht, mt = /^(?:toggle|show|hide)$/,
        gt = new RegExp("^(?:([+-])=|)(" + ke + ")([a-z%]*)$", "i"),
        vt = /queueHooks$/,
        yt = [F],
        bt = {
            "*": [function(e, t) {
                var n = this.createTween(e, t),
                    r = n.cur(),
                    i = gt.exec(t),
                    o = i && i[3] || (ie.cssNumber[e] ? "" : "px"),
                    a = (ie.cssNumber[e] || "px" !== o && +r) && gt.exec(ie.css(n.elem, e)),
                    s = 1,
                    u = 20;
                if (a && a[3] !== o) {
                    o = o || a[3], i = i || [], a = +r || 1;
                    do s = s || ".5", a /= s, ie.style(n.elem, e, a + o); while (s !== (s = n.cur() / r) && 1 !== s && --u)
                }
                return i && (a = n.start = +a || +r || 0, n.unit = o, n.end = i[1] ? a + (i[1] + 1) * i[2] : +i[2]), n
            }]
        };
    ie.Animation = ie.extend(B, {
            tweener: function(e, t) {
                ie.isFunction(e) ? (t = e, e = ["*"]) : e = e.split(" ");
                for (var n, r = 0, i = e.length; i > r; r++) n = e[r], bt[n] = bt[n] || [], bt[n].unshift(t)
            },
            prefilter: function(e, t) {
                t ? yt.unshift(e) : yt.push(e)
            }
        }), ie.speed = function(e, t, n) {
            var r = e && "object" == typeof e ? ie.extend({}, e) : {
                complete: n || !n && t || ie.isFunction(e) && e,
                duration: e,
                easing: n && t || t && !ie.isFunction(t) && t
            };
            return r.duration = ie.fx.off ? 0 : "number" == typeof r.duration ? r.duration : r.duration in ie.fx.speeds ? ie.fx.speeds[r.duration] : ie.fx.speeds._default, (null == r.queue || r.queue === !0) && (r.queue = "fx"), r.old = r.complete, r.complete = function() {
                ie.isFunction(r.old) && r.old.call(this), r.queue && ie.dequeue(this, r.queue)
            }, r
        }, ie.fn.extend({
            fadeTo: function(e, t, n, r) {
                return this.filter(Ae).css("opacity", 0).show().end().animate({
                    opacity: t
                }, e, n, r)
            },
            animate: function(e, t, n, r) {
                var i = ie.isEmptyObject(e),
                    o = ie.speed(t, n, r),
                    a = function() {
                        var t = B(this, ie.extend({}, e), o);
                        (i || ie._data(this, "finish")) && t.stop(!0)
                    };
                return a.finish = a, i || o.queue === !1 ? this.each(a) : this.queue(o.queue, a)
            },
            stop: function(e, t, n) {
                var r = function(e) {
                    var t = e.stop;
                    delete e.stop, t(n)
                };
                return "string" != typeof e && (n = t, t = e, e = void 0), t && e !== !1 && this.queue(e || "fx", []), this.each(function() {
                    var t = !0,
                        i = null != e && e + "queueHooks",
                        o = ie.timers,
                        a = ie._data(this);
                    if (i) a[i] && a[i].stop && r(a[i]);
                    else
                        for (i in a) a[i] && a[i].stop && vt.test(i) && r(a[i]);
                    for (i = o.length; i--;) o[i].elem !== this || null != e && o[i].queue !== e || (o[i].anim.stop(n), t = !1, o.splice(i, 1));
                    (t || !n) && ie.dequeue(this, e)
                })
            },
            finish: function(e) {
                return e !== !1 && (e = e || "fx"), this.each(function() {
                    var t, n = ie._data(this),
                        r = n[e + "queue"],
                        i = n[e + "queueHooks"],
                        o = ie.timers,
                        a = r ? r.length : 0;
                    for (n.finish = !0, ie.queue(this, e, []), i && i.stop && i.stop.call(this, !0), t = o.length; t--;) o[t].elem === this && o[t].queue === e && (o[t].anim.stop(!0), o.splice(t, 1));
                    for (t = 0; a > t; t++) r[t] && r[t].finish && r[t].finish.call(this);
                    delete n.finish
                })
            }
        }), ie.each(["toggle", "show", "hide"], function(e, t) {
            var n = ie.fn[t];
            ie.fn[t] = function(e, r, i) {
                return null == e || "boolean" == typeof e ? n.apply(this, arguments) : this.animate(_(t, !0), e, r, i)
            }
        }), ie.each({
            slideDown: _("show"),
            slideUp: _("hide"),
            slideToggle: _("toggle"),
            fadeIn: {
                opacity: "show"
            },
            fadeOut: {
                opacity: "hide"
            },
            fadeToggle: {
                opacity: "toggle"
            }
        }, function(e, t) {
            ie.fn[e] = function(e, n, r) {
                return this.animate(t, e, n, r)
            }
        }), ie.timers = [], ie.fx.tick = function() {
            var e, t = ie.timers,
                n = 0;
            for (pt = ie.now(); n < t.length; n++) e = t[n], e() || t[n] !== e || t.splice(n--, 1);
            t.length || ie.fx.stop(), pt = void 0
        }, ie.fx.timer = function(e) {
            ie.timers.push(e), e() ? ie.fx.start() : ie.timers.pop()
        }, ie.fx.interval = 13, ie.fx.start = function() {
            ht || (ht = setInterval(ie.fx.tick, ie.fx.interval))
        }, ie.fx.stop = function() {
            clearInterval(ht), ht = null
        }, ie.fx.speeds = {
            slow: 600,
            fast: 200,
            _default: 400
        }, ie.fn.delay = function(e, t) {
            return e = ie.fx ? ie.fx.speeds[e] || e : e, t = t || "fx", this.queue(t, function(t, n) {
                var r = setTimeout(t, e);
                n.stop = function() {
                    clearTimeout(r)
                }
            })
        },
        function() {
            var e, t, n, r, i;
            t = he.createElement("div"), t.setAttribute("className", "t"), t.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", r = t.getElementsByTagName("a")[0], n = he.createElement("select"), i = n.appendChild(he.createElement("option")), e = t.getElementsByTagName("input")[0], r.style.cssText = "top:1px", ne.getSetAttribute = "t" !== t.className, ne.style = /top/.test(r.getAttribute("style")), ne.hrefNormalized = "/a" === r.getAttribute("href"), ne.checkOn = !!e.value, ne.optSelected = i.selected, ne.enctype = !!he.createElement("form").enctype, n.disabled = !0, ne.optDisabled = !i.disabled, e = he.createElement("input"), e.setAttribute("value", ""), ne.input = "" === e.getAttribute("value"), e.value = "t", e.setAttribute("type", "radio"), ne.radioValue = "t" === e.value
        }();
    var xt = /\r/g;
    ie.fn.extend({
        val: function(e) {
            var t, n, r, i = this[0]; {
                if (arguments.length) return r = ie.isFunction(e), this.each(function(n) {
                    var i;
                    1 === this.nodeType && (i = r ? e.call(this, n, ie(this).val()) : e, null == i ? i = "" : "number" == typeof i ? i += "" : ie.isArray(i) && (i = ie.map(i, function(e) {
                        return null == e ? "" : e + ""
                    })), t = ie.valHooks[this.type] || ie.valHooks[this.nodeName.toLowerCase()], t && "set" in t && void 0 !== t.set(this, i, "value") || (this.value = i))
                });
                if (i) return t = ie.valHooks[i.type] || ie.valHooks[i.nodeName.toLowerCase()], t && "get" in t && void 0 !== (n = t.get(i, "value")) ? n : (n = i.value, "string" == typeof n ? n.replace(xt, "") : null == n ? "" : n)
            }
        }
    }), ie.extend({
        valHooks: {
            option: {
                get: function(e) {
                    var t = ie.find.attr(e, "value");
                    return null != t ? t : ie.trim(ie.text(e))
                }
            },
            select: {
                get: function(e) {
                    for (var t, n, r = e.options, i = e.selectedIndex, o = "select-one" === e.type || 0 > i, a = o ? null : [], s = o ? i + 1 : r.length, u = 0 > i ? s : o ? i : 0; s > u; u++)
                        if (n = r[u], !(!n.selected && u !== i || (ne.optDisabled ? n.disabled : null !== n.getAttribute("disabled")) || n.parentNode.disabled && ie.nodeName(n.parentNode, "optgroup"))) {
                            if (t = ie(n).val(), o) return t;
                            a.push(t)
                        }
                    return a
                },
                set: function(e, t) {
                    for (var n, r, i = e.options, o = ie.makeArray(t), a = i.length; a--;)
                        if (r = i[a], ie.inArray(ie.valHooks.option.get(r), o) >= 0) try {
                            r.selected = n = !0
                        } catch (s) {
                            r.scrollHeight
                        } else r.selected = !1;
                    return n || (e.selectedIndex = -1), i
                }
            }
        }
    }), ie.each(["radio", "checkbox"], function() {
        ie.valHooks[this] = {
            set: function(e, t) {
                return ie.isArray(t) ? e.checked = ie.inArray(ie(e).val(), t) >= 0 : void 0
            }
        }, ne.checkOn || (ie.valHooks[this].get = function(e) {
            return null === e.getAttribute("value") ? "on" : e.value
        })
    });
    var wt, Tt, Ct = ie.expr.attrHandle,
        Nt = /^(?:checked|selected)$/i,
        Et = ne.getSetAttribute,
        kt = ne.input;
    ie.fn.extend({
        attr: function(e, t) {
            return De(this, ie.attr, e, t, arguments.length > 1)
        },
        removeAttr: function(e) {
            return this.each(function() {
                ie.removeAttr(this, e)
            })
        }
    }), ie.extend({
        attr: function(e, t, n) {
            var r, i, o = e.nodeType;
            if (e && 3 !== o && 8 !== o && 2 !== o) return typeof e.getAttribute === Ce ? ie.prop(e, t, n) : (1 === o && ie.isXMLDoc(e) || (t = t.toLowerCase(), r = ie.attrHooks[t] || (ie.expr.match.bool.test(t) ? Tt : wt)), void 0 === n ? r && "get" in r && null !== (i = r.get(e, t)) ? i : (i = ie.find.attr(e, t), null == i ? void 0 : i) : null !== n ? r && "set" in r && void 0 !== (i = r.set(e, n, t)) ? i : (e.setAttribute(t, n + ""), n) : void ie.removeAttr(e, t))
        },
        removeAttr: function(e, t) {
            var n, r, i = 0,
                o = t && t.match(be);
            if (o && 1 === e.nodeType)
                for (; n = o[i++];) r = ie.propFix[n] || n, ie.expr.match.bool.test(n) ? kt && Et || !Nt.test(n) ? e[r] = !1 : e[ie.camelCase("default-" + n)] = e[r] = !1 : ie.attr(e, n, ""), e.removeAttribute(Et ? n : r)
        },
        attrHooks: {
            type: {
                set: function(e, t) {
                    if (!ne.radioValue && "radio" === t && ie.nodeName(e, "input")) {
                        var n = e.value;
                        return e.setAttribute("type", t), n && (e.value = n), t
                    }
                }
            }
        }
    }), Tt = {
        set: function(e, t, n) {
            return t === !1 ? ie.removeAttr(e, n) : kt && Et || !Nt.test(n) ? e.setAttribute(!Et && ie.propFix[n] || n, n) : e[ie.camelCase("default-" + n)] = e[n] = !0, n
        }
    }, ie.each(ie.expr.match.bool.source.match(/\w+/g), function(e, t) {
        var n = Ct[t] || ie.find.attr;
        Ct[t] = kt && Et || !Nt.test(t) ? function(e, t, r) {
            var i, o;
            return r || (o = Ct[t], Ct[t] = i, i = null != n(e, t, r) ? t.toLowerCase() : null, Ct[t] = o), i
        } : function(e, t, n) {
            return n ? void 0 : e[ie.camelCase("default-" + t)] ? t.toLowerCase() : null
        }
    }), kt && Et || (ie.attrHooks.value = {
        set: function(e, t, n) {
            return ie.nodeName(e, "input") ? void(e.defaultValue = t) : wt && wt.set(e, t, n)
        }
    }), Et || (wt = {
        set: function(e, t, n) {
            var r = e.getAttributeNode(n);
            return r || e.setAttributeNode(r = e.ownerDocument.createAttribute(n)), r.value = t += "", "value" === n || t === e.getAttribute(n) ? t : void 0
        }
    }, Ct.id = Ct.name = Ct.coords = function(e, t, n) {
        var r;
        return n ? void 0 : (r = e.getAttributeNode(t)) && "" !== r.value ? r.value : null
    }, ie.valHooks.button = {
        get: function(e, t) {
            var n = e.getAttributeNode(t);
            return n && n.specified ? n.value : void 0
        },
        set: wt.set
    }, ie.attrHooks.contenteditable = {
        set: function(e, t, n) {
            wt.set(e, "" === t ? !1 : t, n)
        }
    }, ie.each(["width", "height"], function(e, t) {
        ie.attrHooks[t] = {
            set: function(e, n) {
                return "" === n ? (e.setAttribute(t, "auto"), n) : void 0
            }
        }
    })), ne.style || (ie.attrHooks.style = {
        get: function(e) {
            return e.style.cssText || void 0
        },
        set: function(e, t) {
            return e.style.cssText = t + ""
        }
    });
    var St = /^(?:input|select|textarea|button|object)$/i,
        At = /^(?:a|area)$/i;
    ie.fn.extend({
        prop: function(e, t) {
            return De(this, ie.prop, e, t, arguments.length > 1)
        },
        removeProp: function(e) {
            return e = ie.propFix[e] || e, this.each(function() {
                try {
                    this[e] = void 0, delete this[e]
                } catch (t) {}
            })
        }
    }), ie.extend({
        propFix: {
            "for": "htmlFor",
            "class": "className"
        },
        prop: function(e, t, n) {
            var r, i, o, a = e.nodeType;
            if (e && 3 !== a && 8 !== a && 2 !== a) return o = 1 !== a || !ie.isXMLDoc(e), o && (t = ie.propFix[t] || t, i = ie.propHooks[t]), void 0 !== n ? i && "set" in i && void 0 !== (r = i.set(e, n, t)) ? r : e[t] = n : i && "get" in i && null !== (r = i.get(e, t)) ? r : e[t]
        },
        propHooks: {
            tabIndex: {
                get: function(e) {
                    var t = ie.find.attr(e, "tabindex");
                    return t ? parseInt(t, 10) : St.test(e.nodeName) || At.test(e.nodeName) && e.href ? 0 : -1
                }
            }
        }
    }), ne.hrefNormalized || ie.each(["href", "src"], function(e, t) {
        ie.propHooks[t] = {
            get: function(e) {
                return e.getAttribute(t, 4)
            }
        }
    }), ne.optSelected || (ie.propHooks.selected = {
        get: function(e) {
            var t = e.parentNode;
            return t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex), null
        }
    }), ie.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
        ie.propFix[this.toLowerCase()] = this
    }), ne.enctype || (ie.propFix.enctype = "encoding");
    var Dt = /[\t\r\n\f]/g;
    ie.fn.extend({
        addClass: function(e) {
            var t, n, r, i, o, a, s = 0,
                u = this.length,
                l = "string" == typeof e && e;
            if (ie.isFunction(e)) return this.each(function(t) {
                ie(this).addClass(e.call(this, t, this.className))
            });
            if (l)
                for (t = (e || "").match(be) || []; u > s; s++)
                    if (n = this[s], r = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(Dt, " ") : " ")) {
                        for (o = 0; i = t[o++];) r.indexOf(" " + i + " ") < 0 && (r += i + " ");
                        a = ie.trim(r), n.className !== a && (n.className = a)
                    }
            return this
        },
        removeClass: function(e) {
            var t, n, r, i, o, a, s = 0,
                u = this.length,
                l = 0 === arguments.length || "string" == typeof e && e;
            if (ie.isFunction(e)) return this.each(function(t) {
                ie(this).removeClass(e.call(this, t, this.className))
            });
            if (l)
                for (t = (e || "").match(be) || []; u > s; s++)
                    if (n = this[s], r = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(Dt, " ") : "")) {
                        for (o = 0; i = t[o++];)
                            for (; r.indexOf(" " + i + " ") >= 0;) r = r.replace(" " + i + " ", " ");
                        a = e ? ie.trim(r) : "", n.className !== a && (n.className = a)
                    }
            return this
        },
        toggleClass: function(e, t) {
            var n = typeof e;
            return "boolean" == typeof t && "string" === n ? t ? this.addClass(e) : this.removeClass(e) : this.each(ie.isFunction(e) ? function(n) {
                ie(this).toggleClass(e.call(this, n, this.className, t), t)
            } : function() {
                if ("string" === n)
                    for (var t, r = 0, i = ie(this), o = e.match(be) || []; t = o[r++];) i.hasClass(t) ? i.removeClass(t) : i.addClass(t);
                else(n === Ce || "boolean" === n) && (this.className && ie._data(this, "__className__", this.className), this.className = this.className || e === !1 ? "" : ie._data(this, "__className__") || "")
            })
        },
        hasClass: function(e) {
            for (var t = " " + e + " ", n = 0, r = this.length; r > n; n++)
                if (1 === this[n].nodeType && (" " + this[n].className + " ").replace(Dt, " ").indexOf(t) >= 0) return !0;
            return !1
        }
    }), ie.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function(e, t) {
        ie.fn[t] = function(e, n) {
            return arguments.length > 0 ? this.on(t, null, e, n) : this.trigger(t)
        }
    }), ie.fn.extend({
        hover: function(e, t) {
            return this.mouseenter(e).mouseleave(t || e)
        },
        bind: function(e, t, n) {
            return this.on(e, null, t, n)
        },
        unbind: function(e, t) {
            return this.off(e, null, t)
        },
        delegate: function(e, t, n, r) {
            return this.on(t, e, n, r)
        },
        undelegate: function(e, t, n) {
            return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n)
        }
    });
    var jt = ie.now(),
        Lt = /\?/,
        Ht = /(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;
    ie.parseJSON = function(t) {
        if (e.JSON && e.JSON.parse) return e.JSON.parse(t + "");
        var n, r = null,
            i = ie.trim(t + "");
        return i && !ie.trim(i.replace(Ht, function(e, t, i, o) {
            return n && t && (r = 0), 0 === r ? e : (n = i || t, r += !o - !i, "")
        })) ? Function("return " + i)() : ie.error("Invalid JSON: " + t)
    }, ie.parseXML = function(t) {
        var n, r;
        if (!t || "string" != typeof t) return null;
        try {
            e.DOMParser ? (r = new DOMParser, n = r.parseFromString(t, "text/xml")) : (n = new ActiveXObject("Microsoft.XMLDOM"), n.async = "false", n.loadXML(t))
        } catch (i) {
            n = void 0
        }
        return n && n.documentElement && !n.getElementsByTagName("parsererror").length || ie.error("Invalid XML: " + t), n
    };
    var qt, _t, Mt = /#.*$/,
        Ft = /([?&])_=[^&]*/,
        Ot = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm,
        Bt = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
        Pt = /^(?:GET|HEAD)$/,
        Rt = /^\/\//,
        Wt = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,
        $t = {},
        zt = {},
        It = "*/".concat("*");
    try {
        _t = location.href
    } catch (Xt) {
        _t = he.createElement("a"), _t.href = "", _t = _t.href
    }
    qt = Wt.exec(_t.toLowerCase()) || [], ie.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: _t,
            type: "GET",
            isLocal: Bt.test(qt[1]),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": It,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /xml/,
                html: /html/,
                json: /json/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText",
                json: "responseJSON"
            },
            converters: {
                "* text": String,
                "text html": !0,
                "text json": ie.parseJSON,
                "text xml": ie.parseXML
            },
            flatOptions: {
                url: !0,
                context: !0
            }
        },
        ajaxSetup: function(e, t) {
            return t ? W(W(e, ie.ajaxSettings), t) : W(ie.ajaxSettings, e)
        },
        ajaxPrefilter: P($t),
        ajaxTransport: P(zt),
        ajax: function(e, t) {
            function n(e, t, n, r) {
                var i, c, v, y, x, T = t;
                2 !== b && (b = 2, s && clearTimeout(s), l = void 0, a = r || "", w.readyState = e > 0 ? 4 : 0, i = e >= 200 && 300 > e || 304 === e, n && (y = $(d, w, n)), y = z(d, y, w, i), i ? (d.ifModified && (x = w.getResponseHeader("Last-Modified"), x && (ie.lastModified[o] = x), x = w.getResponseHeader("etag"), x && (ie.etag[o] = x)), 204 === e || "HEAD" === d.type ? T = "nocontent" : 304 === e ? T = "notmodified" : (T = y.state, c = y.data, v = y.error, i = !v)) : (v = T, (e || !T) && (T = "error", 0 > e && (e = 0))), w.status = e, w.statusText = (t || T) + "", i ? h.resolveWith(f, [c, T, w]) : h.rejectWith(f, [w, T, v]), w.statusCode(g), g = void 0, u && p.trigger(i ? "ajaxSuccess" : "ajaxError", [w, d, i ? c : v]), m.fireWith(f, [w, T]), u && (p.trigger("ajaxComplete", [w, d]), --ie.active || ie.event.trigger("ajaxStop")))
            }
            "object" == typeof e && (t = e, e = void 0), t = t || {};
            var r, i, o, a, s, u, l, c, d = ie.ajaxSetup({}, t),
                f = d.context || d,
                p = d.context && (f.nodeType || f.jquery) ? ie(f) : ie.event,
                h = ie.Deferred(),
                m = ie.Callbacks("once memory"),
                g = d.statusCode || {},
                v = {},
                y = {},
                b = 0,
                x = "canceled",
                w = {
                    readyState: 0,
                    getResponseHeader: function(e) {
                        var t;
                        if (2 === b) {
                            if (!c)
                                for (c = {}; t = Ot.exec(a);) c[t[1].toLowerCase()] = t[2];
                            t = c[e.toLowerCase()]
                        }
                        return null == t ? null : t
                    },
                    getAllResponseHeaders: function() {
                        return 2 === b ? a : null
                    },
                    setRequestHeader: function(e, t) {
                        var n = e.toLowerCase();
                        return b || (e = y[n] = y[n] || e, v[e] = t), this
                    },
                    overrideMimeType: function(e) {
                        return b || (d.mimeType = e), this
                    },
                    statusCode: function(e) {
                        var t;
                        if (e)
                            if (2 > b)
                                for (t in e) g[t] = [g[t], e[t]];
                            else w.always(e[w.status]);
                        return this
                    },
                    abort: function(e) {
                        var t = e || x;
                        return l && l.abort(t), n(0, t), this
                    }
                };
            if (h.promise(w).complete = m.add, w.success = w.done, w.error = w.fail, d.url = ((e || d.url || _t) + "").replace(Mt, "").replace(Rt, qt[1] + "//"), d.type = t.method || t.type || d.method || d.type, d.dataTypes = ie.trim(d.dataType || "*").toLowerCase().match(be) || [""], null == d.crossDomain && (r = Wt.exec(d.url.toLowerCase()), d.crossDomain = !(!r || r[1] === qt[1] && r[2] === qt[2] && (r[3] || ("http:" === r[1] ? "80" : "443")) === (qt[3] || ("http:" === qt[1] ? "80" : "443")))), d.data && d.processData && "string" != typeof d.data && (d.data = ie.param(d.data, d.traditional)), R($t, d, t, w), 2 === b) return w;
            u = ie.event && d.global, u && 0 === ie.active++ && ie.event.trigger("ajaxStart"), d.type = d.type.toUpperCase(), d.hasContent = !Pt.test(d.type), o = d.url, d.hasContent || (d.data && (o = d.url += (Lt.test(o) ? "&" : "?") + d.data, delete d.data), d.cache === !1 && (d.url = Ft.test(o) ? o.replace(Ft, "$1_=" + jt++) : o + (Lt.test(o) ? "&" : "?") + "_=" + jt++)), d.ifModified && (ie.lastModified[o] && w.setRequestHeader("If-Modified-Since", ie.lastModified[o]), ie.etag[o] && w.setRequestHeader("If-None-Match", ie.etag[o])), (d.data && d.hasContent && d.contentType !== !1 || t.contentType) && w.setRequestHeader("Content-Type", d.contentType), w.setRequestHeader("Accept", d.dataTypes[0] && d.accepts[d.dataTypes[0]] ? d.accepts[d.dataTypes[0]] + ("*" !== d.dataTypes[0] ? ", " + It + "; q=0.01" : "") : d.accepts["*"]);
            for (i in d.headers) w.setRequestHeader(i, d.headers[i]);
            if (d.beforeSend && (d.beforeSend.call(f, w, d) === !1 || 2 === b)) return w.abort();
            x = "abort";
            for (i in {
                    success: 1,
                    error: 1,
                    complete: 1
                }) w[i](d[i]);
            if (l = R(zt, d, t, w)) {
                w.readyState = 1, u && p.trigger("ajaxSend", [w, d]), d.async && d.timeout > 0 && (s = setTimeout(function() {
                    w.abort("timeout")
                }, d.timeout));
                try {
                    b = 1, l.send(v, n)
                } catch (T) {
                    if (!(2 > b)) throw T;
                    n(-1, T)
                }
            } else n(-1, "No Transport");
            return w
        },
        getJSON: function(e, t, n) {
            return ie.get(e, t, n, "json")
        },
        getScript: function(e, t) {
            return ie.get(e, void 0, t, "script")
        }
    }), ie.each(["get", "post"], function(e, t) {
        ie[t] = function(e, n, r, i) {
            return ie.isFunction(n) && (i = i || r, r = n, n = void 0), ie.ajax({
                url: e,
                type: t,
                dataType: i,
                data: n,
                success: r
            })
        }
    }), ie._evalUrl = function(e) {
        return ie.ajax({
            url: e,
            type: "GET",
            dataType: "script",
            async: !1,
            global: !1,
            "throws": !0
        })
    }, ie.fn.extend({
        wrapAll: function(e) {
            if (ie.isFunction(e)) return this.each(function(t) {
                ie(this).wrapAll(e.call(this, t))
            });
            if (this[0]) {
                var t = ie(e, this[0].ownerDocument).eq(0).clone(!0);
                this[0].parentNode && t.insertBefore(this[0]), t.map(function() {
                    for (var e = this; e.firstChild && 1 === e.firstChild.nodeType;) e = e.firstChild;
                    return e
                }).append(this)
            }
            return this
        },
        wrapInner: function(e) {
            return this.each(ie.isFunction(e) ? function(t) {
                ie(this).wrapInner(e.call(this, t))
            } : function() {
                var t = ie(this),
                    n = t.contents();
                n.length ? n.wrapAll(e) : t.append(e)
            })
        },
        wrap: function(e) {
            var t = ie.isFunction(e);
            return this.each(function(n) {
                ie(this).wrapAll(t ? e.call(this, n) : e)
            })
        },
        unwrap: function() {
            return this.parent().each(function() {
                ie.nodeName(this, "body") || ie(this).replaceWith(this.childNodes)
            }).end()
        }
    }), ie.expr.filters.hidden = function(e) {
        return e.offsetWidth <= 0 && e.offsetHeight <= 0 || !ne.reliableHiddenOffsets() && "none" === (e.style && e.style.display || ie.css(e, "display"))
    }, ie.expr.filters.visible = function(e) {
        return !ie.expr.filters.hidden(e)
    };
    var Ut = /%20/g,
        Vt = /\[\]$/,
        Jt = /\r?\n/g,
        Yt = /^(?:submit|button|image|reset|file)$/i,
        Gt = /^(?:input|select|textarea|keygen)/i;
    ie.param = function(e, t) {
        var n, r = [],
            i = function(e, t) {
                t = ie.isFunction(t) ? t() : null == t ? "" : t, r[r.length] = encodeURIComponent(e) + "=" + encodeURIComponent(t)
            };
        if (void 0 === t && (t = ie.ajaxSettings && ie.ajaxSettings.traditional), ie.isArray(e) || e.jquery && !ie.isPlainObject(e)) ie.each(e, function() {
            i(this.name, this.value)
        });
        else
            for (n in e) I(n, e[n], t, i);
        return r.join("&").replace(Ut, "+")
    }, ie.fn.extend({
        serialize: function() {
            return ie.param(this.serializeArray())
        },
        serializeArray: function() {
            return this.map(function() {
                var e = ie.prop(this, "elements");
                return e ? ie.makeArray(e) : this
            }).filter(function() {
                var e = this.type;
                return this.name && !ie(this).is(":disabled") && Gt.test(this.nodeName) && !Yt.test(e) && (this.checked || !je.test(e))
            }).map(function(e, t) {
                var n = ie(this).val();
                return null == n ? null : ie.isArray(n) ? ie.map(n, function(e) {
                    return {
                        name: t.name,
                        value: e.replace(Jt, "\r\n")
                    }
                }) : {
                    name: t.name,
                    value: n.replace(Jt, "\r\n")
                }
            }).get()
        }
    }), ie.ajaxSettings.xhr = void 0 !== e.ActiveXObject ? function() {
        return !this.isLocal && /^(get|post|head|put|delete|options)$/i.test(this.type) && X() || U()
    } : X;
    var Qt = 0,
        Kt = {},
        Zt = ie.ajaxSettings.xhr();
    e.attachEvent && e.attachEvent("onunload", function() {
        for (var e in Kt) Kt[e](void 0, !0)
    }), ne.cors = !!Zt && "withCredentials" in Zt, Zt = ne.ajax = !!Zt, Zt && ie.ajaxTransport(function(e) {
        if (!e.crossDomain || ne.cors) {
            var t;
            return {
                send: function(n, r) {
                    var i, o = e.xhr(),
                        a = ++Qt;
                    if (o.open(e.type, e.url, e.async, e.username, e.password), e.xhrFields)
                        for (i in e.xhrFields) o[i] = e.xhrFields[i];
                    e.mimeType && o.overrideMimeType && o.overrideMimeType(e.mimeType), e.crossDomain || n["X-Requested-With"] || (n["X-Requested-With"] = "XMLHttpRequest");
                    for (i in n) void 0 !== n[i] && o.setRequestHeader(i, n[i] + "");
                    o.send(e.hasContent && e.data || null), t = function(n, i) {
                        var s, u, l;
                        if (t && (i || 4 === o.readyState))
                            if (delete Kt[a], t = void 0, o.onreadystatechange = ie.noop, i) 4 !== o.readyState && o.abort();
                            else {
                                l = {}, s = o.status, "string" == typeof o.responseText && (l.text = o.responseText);
                                try {
                                    u = o.statusText
                                } catch (c) {
                                    u = ""
                                }
                                s || !e.isLocal || e.crossDomain ? 1223 === s && (s = 204) : s = l.text ? 200 : 404
                            }
                        l && r(s, u, l, o.getAllResponseHeaders())
                    }, e.async ? 4 === o.readyState ? setTimeout(t) : o.onreadystatechange = Kt[a] = t : t()
                },
                abort: function() {
                    t && t(void 0, !0)
                }
            }
        }
    }), ie.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /(?:java|ecma)script/
        },
        converters: {
            "text script": function(e) {
                return ie.globalEval(e), e
            }
        }
    }), ie.ajaxPrefilter("script", function(e) {
        void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET", e.global = !1)
    }), ie.ajaxTransport("script", function(e) {
        if (e.crossDomain) {
            var t, n = he.head || ie("head")[0] || he.documentElement;
            return {
                send: function(r, i) {
                    t = he.createElement("script"), t.async = !0, e.scriptCharset && (t.charset = e.scriptCharset), t.src = e.url, t.onload = t.onreadystatechange = function(e, n) {
                        (n || !t.readyState || /loaded|complete/.test(t.readyState)) && (t.onload = t.onreadystatechange = null, t.parentNode && t.parentNode.removeChild(t), t = null, n || i(200, "success"))
                    }, n.insertBefore(t, n.firstChild)
                },
                abort: function() {
                    t && t.onload(void 0, !0)
                }
            }
        }
    });
    var en = [],
        tn = /(=)\?(?=&|$)|\?\?/;
    ie.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var e = en.pop() || ie.expando + "_" + jt++;
            return this[e] = !0, e
        }
    }), ie.ajaxPrefilter("json jsonp", function(t, n, r) {
        var i, o, a, s = t.jsonp !== !1 && (tn.test(t.url) ? "url" : "string" == typeof t.data && !(t.contentType || "").indexOf("application/x-www-form-urlencoded") && tn.test(t.data) && "data");
        return s || "jsonp" === t.dataTypes[0] ? (i = t.jsonpCallback = ie.isFunction(t.jsonpCallback) ? t.jsonpCallback() : t.jsonpCallback, s ? t[s] = t[s].replace(tn, "$1" + i) : t.jsonp !== !1 && (t.url += (Lt.test(t.url) ? "&" : "?") + t.jsonp + "=" + i), t.converters["script json"] = function() {
            return a || ie.error(i + " was not called"), a[0]
        }, t.dataTypes[0] = "json", o = e[i], e[i] = function() {
            a = arguments
        }, r.always(function() {
            e[i] = o, t[i] && (t.jsonpCallback = n.jsonpCallback, en.push(i)), a && ie.isFunction(o) && o(a[0]), a = o = void 0
        }), "script") : void 0
    }), ie.parseHTML = function(e, t, n) {
        if (!e || "string" != typeof e) return null;
        "boolean" == typeof t && (n = t, t = !1), t = t || he;
        var r = de.exec(e),
            i = !n && [];
        return r ? [t.createElement(r[1])] : (r = ie.buildFragment([e], t, i), i && i.length && ie(i).remove(), ie.merge([], r.childNodes))
    };
    var nn = ie.fn.load;
    ie.fn.load = function(e, t, n) {
        if ("string" != typeof e && nn) return nn.apply(this, arguments);
        var r, i, o, a = this,
            s = e.indexOf(" ");
        return s >= 0 && (r = ie.trim(e.slice(s, e.length)), e = e.slice(0, s)), ie.isFunction(t) ? (n = t, t = void 0) : t && "object" == typeof t && (o = "POST"), a.length > 0 && ie.ajax({
            url: e,
            type: o,
            dataType: "html",
            data: t
        }).done(function(e) {
            i = arguments, a.html(r ? ie("<div>").append(ie.parseHTML(e)).find(r) : e)
        }).complete(n && function(e, t) {
            a.each(n, i || [e.responseText, t, e])
        }), this
    }, ie.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(e, t) {
        ie.fn[t] = function(e) {
            return this.on(t, e)
        }
    }), ie.expr.filters.animated = function(e) {
        return ie.grep(ie.timers, function(t) {
            return e === t.elem
        }).length
    };
    var rn = e.document.documentElement;
    ie.offset = {
        setOffset: function(e, t, n) {
            var r, i, o, a, s, u, l, c = ie.css(e, "position"),
                d = ie(e),
                f = {};
            "static" === c && (e.style.position = "relative"), s = d.offset(), o = ie.css(e, "top"), u = ie.css(e, "left"), l = ("absolute" === c || "fixed" === c) && ie.inArray("auto", [o, u]) > -1, l ? (r = d.position(), a = r.top, i = r.left) : (a = parseFloat(o) || 0, i = parseFloat(u) || 0), ie.isFunction(t) && (t = t.call(e, n, s)), null != t.top && (f.top = t.top - s.top + a), null != t.left && (f.left = t.left - s.left + i), "using" in t ? t.using.call(e, f) : d.css(f)
        }
    }, ie.fn.extend({
        offset: function(e) {
            if (arguments.length) return void 0 === e ? this : this.each(function(t) {
                ie.offset.setOffset(this, e, t)
            });
            var t, n, r = {
                    top: 0,
                    left: 0
                },
                i = this[0],
                o = i && i.ownerDocument;
            if (o) return t = o.documentElement, ie.contains(t, i) ? (typeof i.getBoundingClientRect !== Ce && (r = i.getBoundingClientRect()), n = V(o), {
                top: r.top + (n.pageYOffset || t.scrollTop) - (t.clientTop || 0),
                left: r.left + (n.pageXOffset || t.scrollLeft) - (t.clientLeft || 0)
            }) : r
        },
        position: function() {
            if (this[0]) {
                var e, t, n = {
                        top: 0,
                        left: 0
                    },
                    r = this[0];
                return "fixed" === ie.css(r, "position") ? t = r.getBoundingClientRect() : (e = this.offsetParent(), t = this.offset(), ie.nodeName(e[0], "html") || (n = e.offset()), n.top += ie.css(e[0], "borderTopWidth", !0), n.left += ie.css(e[0], "borderLeftWidth", !0)), {
                    top: t.top - n.top - ie.css(r, "marginTop", !0),
                    left: t.left - n.left - ie.css(r, "marginLeft", !0)
                }
            }
        },
        offsetParent: function() {
            return this.map(function() {
                for (var e = this.offsetParent || rn; e && !ie.nodeName(e, "html") && "static" === ie.css(e, "position");) e = e.offsetParent;
                return e || rn
            })
        }
    }), ie.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function(e, t) {
        var n = /Y/.test(t);
        ie.fn[e] = function(r) {
            return De(this, function(e, r, i) {
                var o = V(e);
                return void 0 === i ? o ? t in o ? o[t] : o.document.documentElement[r] : e[r] : void(o ? o.scrollTo(n ? ie(o).scrollLeft() : i, n ? i : ie(o).scrollTop()) : e[r] = i)
            }, e, r, arguments.length, null)
        }
    }), ie.each(["top", "left"], function(e, t) {
        ie.cssHooks[t] = k(ne.pixelPosition, function(e, n) {
            return n ? (n = tt(e, t), rt.test(n) ? ie(e).position()[t] + "px" : n) : void 0
        })
    }), ie.each({
        Height: "height",
        Width: "width"
    }, function(e, t) {
        ie.each({
            padding: "inner" + e,
            content: t,
            "": "outer" + e
        }, function(n, r) {
            ie.fn[r] = function(r, i) {
                var o = arguments.length && (n || "boolean" != typeof r),
                    a = n || (r === !0 || i === !0 ? "margin" : "border");
                return De(this, function(t, n, r) {
                    var i;

                    return ie.isWindow(t) ? t.document.documentElement["client" + e] : 9 === t.nodeType ? (i = t.documentElement, Math.max(t.body["scroll" + e], i["scroll" + e], t.body["offset" + e], i["offset" + e], i["client" + e])) : void 0 === r ? ie.css(t, n, a) : ie.style(t, n, r, a)
                }, t, o ? r : void 0, o, null)
            }
        })
    }), ie.fn.size = function() {
        return this.length
    }, ie.fn.andSelf = ie.fn.addBack, "function" == typeof define && define.amd && define("jquery", [], function() {
        return ie
    });
    var on = e.jQuery,
        an = e.$;
    return ie.noConflict = function(t) {
        return e.$ === ie && (e.$ = an), t && e.jQuery === ie && (e.jQuery = on), ie
    }, typeof t === Ce && (e.jQuery = e.$ = ie), ie
});
! function(t) {
    "function" == typeof define && define.amd ? define(["jquery"], t) : t(jQuery)
}(function(t) {
    function e(e, i) {
        var r, o, u, s = e.nodeName.toLowerCase();
        return "area" === s ? (r = e.parentNode, o = r.name, e.href && o && "map" === r.nodeName.toLowerCase() ? (u = t("img[usemap='#" + o + "']")[0], !!u && n(u)) : !1) : (/input|select|textarea|button|object/.test(s) ? !e.disabled : "a" === s ? e.href || i : i) && n(e)
    }

    function n(e) {
        return t.expr.filters.visible(e) && !t(e).parents().addBack().filter(function() {
            return "hidden" === t.css(this, "visibility")
        }).length
    }
    t.ui = t.ui || {}, t.extend(t.ui, {
        version: "1.11.2",
        keyCode: {
            BACKSPACE: 8,
            COMMA: 188,
            DELETE: 46,
            DOWN: 40,
            END: 35,
            ENTER: 13,
            ESCAPE: 27,
            HOME: 36,
            LEFT: 37,
            PAGE_DOWN: 34,
            PAGE_UP: 33,
            PERIOD: 190,
            RIGHT: 39,
            SPACE: 32,
            TAB: 9,
            UP: 38
        }
    }), t.fn.extend({
        scrollParent: function(e) {
            var n = this.css("position"),
                i = "absolute" === n,
                r = e ? /(auto|scroll|hidden)/ : /(auto|scroll)/,
                o = this.parents().filter(function() {
                    var e = t(this);
                    return i && "static" === e.css("position") ? !1 : r.test(e.css("overflow") + e.css("overflow-y") + e.css("overflow-x"))
                }).eq(0);
            return "fixed" !== n && o.length ? o : t(this[0].ownerDocument || document)
        },
        uniqueId: function() {
            var t = 0;
            return function() {
                return this.each(function() {
                    this.id || (this.id = "ui-id-" + ++t)
                })
            }
        }(),
        removeUniqueId: function() {
            return this.each(function() {
                /^ui-id-\d+$/.test(this.id) && t(this).removeAttr("id")
            })
        }
    }), t.extend(t.expr[":"], {
        data: t.expr.createPseudo ? t.expr.createPseudo(function(e) {
            return function(n) {
                return !!t.data(n, e)
            }
        }) : function(e, n, i) {
            return !!t.data(e, i[3])
        },
        focusable: function(n) {
            return e(n, !isNaN(t.attr(n, "tabindex")))
        },
        tabbable: function(n) {
            var i = t.attr(n, "tabindex"),
                r = isNaN(i);
            return (r || i >= 0) && e(n, !r)
        }
    }), t("<a>").outerWidth(1).jquery || t.each(["Width", "Height"], function(e, n) {
        function i(e, n, i, o) {
            return t.each(r, function() {
                n -= parseFloat(t.css(e, "padding" + this)) || 0, i && (n -= parseFloat(t.css(e, "border" + this + "Width")) || 0), o && (n -= parseFloat(t.css(e, "margin" + this)) || 0)
            }), n
        }
        var r = "Width" === n ? ["Left", "Right"] : ["Top", "Bottom"],
            o = n.toLowerCase(),
            u = {
                innerWidth: t.fn.innerWidth,
                innerHeight: t.fn.innerHeight,
                outerWidth: t.fn.outerWidth,
                outerHeight: t.fn.outerHeight
            };
        t.fn["inner" + n] = function(e) {
            return void 0 === e ? u["inner" + n].call(this) : this.each(function() {
                t(this).css(o, i(this, e) + "px")
            })
        }, t.fn["outer" + n] = function(e, r) {
            return "number" != typeof e ? u["outer" + n].call(this, e) : this.each(function() {
                t(this).css(o, i(this, e, !0, r) + "px")
            })
        }
    }), t.fn.addBack || (t.fn.addBack = function(t) {
        return this.add(null == t ? this.prevObject : this.prevObject.filter(t))
    }), t("<a>").data("a-b", "a").removeData("a-b").data("a-b") && (t.fn.removeData = function(e) {
        return function(n) {
            return arguments.length ? e.call(this, t.camelCase(n)) : e.call(this)
        }
    }(t.fn.removeData)), t.ui.ie = !!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase()), t.fn.extend({
        focus: function(e) {
            return function(n, i) {
                return "number" == typeof n ? this.each(function() {
                    var e = this;
                    setTimeout(function() {
                        t(e).focus(), i && i.call(e)
                    }, n)
                }) : e.apply(this, arguments)
            }
        }(t.fn.focus),
        disableSelection: function() {
            var t = "onselectstart" in document.createElement("div") ? "selectstart" : "mousedown";
            return function() {
                return this.bind(t + ".ui-disableSelection", function(t) {
                    t.preventDefault()
                })
            }
        }(),
        enableSelection: function() {
            return this.unbind(".ui-disableSelection")
        },
        zIndex: function(e) {
            if (void 0 !== e) return this.css("zIndex", e);
            if (this.length)
                for (var n, i, r = t(this[0]); r.length && r[0] !== document;) {
                    if (n = r.css("position"), ("absolute" === n || "relative" === n || "fixed" === n) && (i = parseInt(r.css("zIndex"), 10), !isNaN(i) && 0 !== i)) return i;
                    r = r.parent()
                }
            return 0
        }
    }), t.ui.plugin = {
        add: function(e, n, i) {
            var r, o = t.ui[e].prototype;
            for (r in i) o.plugins[r] = o.plugins[r] || [], o.plugins[r].push([n, i[r]])
        },
        call: function(t, e, n, i) {
            var r, o = t.plugins[e];
            if (o && (i || t.element[0].parentNode && 11 !== t.element[0].parentNode.nodeType))
                for (r = 0; r < o.length; r++) t.options[o[r][0]] && o[r][1].apply(t.element, n)
        }
    }
});
! function(t) {
    "function" == typeof define && define.amd ? define(["jquery"], t) : t(jQuery)
}(function(t) {
    var e = 0,
        i = Array.prototype.slice;
    return t.cleanData = function(e) {
        return function(i) {
            var n, s, o;
            for (o = 0; null != (s = i[o]); o++) try {
                n = t._data(s, "events"), n && n.remove && t(s).triggerHandler("remove")
            } catch (r) {}
            e(i)
        }
    }(t.cleanData), t.widget = function(e, i, n) {
        var s, o, r, a, u = {},
            d = e.split(".")[0];
        return e = e.split(".")[1], s = d + "-" + e, n || (n = i, i = t.Widget), t.expr[":"][s.toLowerCase()] = function(e) {
            return !!t.data(e, s)
        }, t[d] = t[d] || {}, o = t[d][e], r = t[d][e] = function(t, e) {
            return this._createWidget ? void(arguments.length && this._createWidget(t, e)) : new r(t, e)
        }, t.extend(r, o, {
            version: n.version,
            _proto: t.extend({}, n),
            _childConstructors: []
        }), a = new i, a.options = t.widget.extend({}, a.options), t.each(n, function(e, n) {
            return t.isFunction(n) ? void(u[e] = function() {
                var t = function() {
                        return i.prototype[e].apply(this, arguments)
                    },
                    s = function(t) {
                        return i.prototype[e].apply(this, t)
                    };
                return function() {
                    var e, i = this._super,
                        o = this._superApply;
                    return this._super = t, this._superApply = s, e = n.apply(this, arguments), this._super = i, this._superApply = o, e
                }
            }()) : void(u[e] = n)
        }), r.prototype = t.widget.extend(a, {
            widgetEventPrefix: o ? a.widgetEventPrefix || e : e
        }, u, {
            constructor: r,
            namespace: d,
            widgetName: e,
            widgetFullName: s
        }), o ? (t.each(o._childConstructors, function(e, i) {
            var n = i.prototype;
            t.widget(n.namespace + "." + n.widgetName, r, i._proto)
        }), delete o._childConstructors) : i._childConstructors.push(r), t.widget.bridge(e, r), r
    }, t.widget.extend = function(e) {
        for (var n, s, o = i.call(arguments, 1), r = 0, a = o.length; a > r; r++)
            for (n in o[r]) s = o[r][n], o[r].hasOwnProperty(n) && void 0 !== s && (e[n] = t.isPlainObject(s) ? t.isPlainObject(e[n]) ? t.widget.extend({}, e[n], s) : t.widget.extend({}, s) : s);
        return e
    }, t.widget.bridge = function(e, n) {
        var s = n.prototype.widgetFullName || e;
        t.fn[e] = function(o) {
            var r = "string" == typeof o,
                a = i.call(arguments, 1),
                u = this;
            return o = !r && a.length ? t.widget.extend.apply(null, [o].concat(a)) : o, this.each(r ? function() {
                var i, n = t.data(this, s);
                return "instance" === o ? (u = n, !1) : n ? t.isFunction(n[o]) && "_" !== o.charAt(0) ? (i = n[o].apply(n, a), i !== n && void 0 !== i ? (u = i && i.jquery ? u.pushStack(i.get()) : i, !1) : void 0) : t.error("no such method '" + o + "' for " + e + " widget instance") : t.error("cannot call methods on " + e + " prior to initialization; attempted to call method '" + o + "'")
            } : function() {
                var e = t.data(this, s);
                e ? (e.option(o || {}), e._init && e._init()) : t.data(this, s, new n(o, this))
            }), u
        }
    }, t.Widget = function() {}, t.Widget._childConstructors = [], t.Widget.prototype = {
        widgetName: "widget",
        widgetEventPrefix: "",
        defaultElement: "<div>",
        options: {
            disabled: !1,
            create: null
        },
        _createWidget: function(i, n) {
            n = t(n || this.defaultElement || this)[0], this.element = t(n), this.uuid = e++, this.eventNamespace = "." + this.widgetName + this.uuid, this.bindings = t(), this.hoverable = t(), this.focusable = t(), n !== this && (t.data(n, this.widgetFullName, this), this._on(!0, this.element, {
                remove: function(t) {
                    t.target === n && this.destroy()
                }
            }), this.document = t(n.style ? n.ownerDocument : n.document || n), this.window = t(this.document[0].defaultView || this.document[0].parentWindow)), this.options = t.widget.extend({}, this.options, this._getCreateOptions(), i), this._create(), this._trigger("create", null, this._getCreateEventData()), this._init()
        },
        _getCreateOptions: t.noop,
        _getCreateEventData: t.noop,
        _create: t.noop,
        _init: t.noop,
        destroy: function() {
            this._destroy(), this.element.unbind(this.eventNamespace).removeData(this.widgetFullName).removeData(t.camelCase(this.widgetFullName)), this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName + "-disabled ui-state-disabled"), this.bindings.unbind(this.eventNamespace), this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus")
        },
        _destroy: t.noop,
        widget: function() {
            return this.element
        },
        option: function(e, i) {
            var n, s, o, r = e;
            if (0 === arguments.length) return t.widget.extend({}, this.options);
            if ("string" == typeof e)
                if (r = {}, n = e.split("."), e = n.shift(), n.length) {
                    for (s = r[e] = t.widget.extend({}, this.options[e]), o = 0; o < n.length - 1; o++) s[n[o]] = s[n[o]] || {}, s = s[n[o]];
                    if (e = n.pop(), 1 === arguments.length) return void 0 === s[e] ? null : s[e];
                    s[e] = i
                } else {
                    if (1 === arguments.length) return void 0 === this.options[e] ? null : this.options[e];
                    r[e] = i
                }
            return this._setOptions(r), this
        },
        _setOptions: function(t) {
            var e;
            for (e in t) this._setOption(e, t[e]);
            return this
        },
        _setOption: function(t, e) {
            return this.options[t] = e, "disabled" === t && (this.widget().toggleClass(this.widgetFullName + "-disabled", !!e), e && (this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus"))), this
        },
        enable: function() {
            return this._setOptions({
                disabled: !1
            })
        },
        disable: function() {
            return this._setOptions({
                disabled: !0
            })
        },
        _on: function(e, i, n) {
            var s, o = this;
            "boolean" != typeof e && (n = i, i = e, e = !1), n ? (i = s = t(i), this.bindings = this.bindings.add(i)) : (n = i, i = this.element, s = this.widget()), t.each(n, function(n, r) {
                function a() {
                    return e || o.options.disabled !== !0 && !t(this).hasClass("ui-state-disabled") ? ("string" == typeof r ? o[r] : r).apply(o, arguments) : void 0
                }
                "string" != typeof r && (a.guid = r.guid = r.guid || a.guid || t.guid++);
                var u = n.match(/^([\w:-]*)\s*(.*)$/),
                    d = u[1] + o.eventNamespace,
                    h = u[2];
                h ? s.delegate(h, d, a) : i.bind(d, a)
            })
        },
        _off: function(e, i) {
            i = (i || "").split(" ").join(this.eventNamespace + " ") + this.eventNamespace, e.unbind(i).undelegate(i), this.bindings = t(this.bindings.not(e).get()), this.focusable = t(this.focusable.not(e).get()), this.hoverable = t(this.hoverable.not(e).get())
        },
        _delay: function(t, e) {
            function i() {
                return ("string" == typeof t ? n[t] : t).apply(n, arguments)
            }
            var n = this;
            return setTimeout(i, e || 0)
        },
        _hoverable: function(e) {
            this.hoverable = this.hoverable.add(e), this._on(e, {
                mouseenter: function(e) {
                    t(e.currentTarget).addClass("ui-state-hover")
                },
                mouseleave: function(e) {
                    t(e.currentTarget).removeClass("ui-state-hover")
                }
            })
        },
        _focusable: function(e) {
            this.focusable = this.focusable.add(e), this._on(e, {
                focusin: function(e) {
                    t(e.currentTarget).addClass("ui-state-focus")
                },
                focusout: function(e) {
                    t(e.currentTarget).removeClass("ui-state-focus")
                }
            })
        },
        _trigger: function(e, i, n) {
            var s, o, r = this.options[e];
            if (n = n || {}, i = t.Event(i), i.type = (e === this.widgetEventPrefix ? e : this.widgetEventPrefix + e).toLowerCase(), i.target = this.element[0], o = i.originalEvent)
                for (s in o) s in i || (i[s] = o[s]);
            return this.element.trigger(i, n), !(t.isFunction(r) && r.apply(this.element[0], [i].concat(n)) === !1 || i.isDefaultPrevented())
        }
    }, t.each({
        show: "fadeIn",
        hide: "fadeOut"
    }, function(e, i) {
        t.Widget.prototype["_" + e] = function(n, s, o) {
            "string" == typeof s && (s = {
                effect: s
            });
            var r, a = s ? s === !0 || "number" == typeof s ? i : s.effect || i : e;
            s = s || {}, "number" == typeof s && (s = {
                duration: s
            }), r = !t.isEmptyObject(s), s.complete = o, s.delay && n.delay(s.delay), r && t.effects && t.effects.effect[a] ? n[e](s) : a !== e && n[a] ? n[a](s.duration, s.easing, o) : n.queue(function(i) {
                t(this)[e](), o && o.call(n[0]), i()
            })
        }
    }), t.widget
});
jQuery.fn.caret = function(e) {
    var t = this[0],
        n = "true" === t.contentEditable;
    if (0 === arguments.length) {
        if (window.getSelection) {
            if (n) {
                t.focus();
                var o = window.getSelection().getRangeAt(0),
                    r = o.cloneRange();
                return r.selectNodeContents(t), r.setEnd(o.endContainer, o.endOffset), r.toString().length
            }
            return t.selectionStart
        }
        if (document.selection) {
            if (t.focus(), n) {
                var o = document.selection.createRange(),
                    r = document.body.createTextRange();
                return r.moveToElementText(t), r.setEndPoint("EndToEnd", o), r.text.length
            }
            var e = 0,
                a = t.createTextRange(),
                r = document.selection.createRange().duplicate(),
                c = r.getBookmark();
            for (a.moveToBookmark(c); 0 !== a.moveStart("character", -1);) e++;
            return e
        }
        return 0
    }
    if (-1 === e && (e = this[n ? "text" : "val"]().length), window.getSelection) n ? (t.focus(), window.getSelection().collapse(t.firstChild, e)) : t.setSelectionRange(e, e);
    else if (document.body.createTextRange)
        if (n) {
            var a = document.body.createTextRange();
            a.moveToElementText(t), a.moveStart("character", e), a.collapse(!0), a.select()
        } else {
            var a = t.createTextRange();
            a.move("character", e), a.select()
        }
    return n || t.focus(), e
};
! function(e) {
    function d(e, d) {
        var t = e.data("ddslick"),
            o = e.find(".dd-selected"),
            s = o.siblings(".dd-selected-value"),
            l = (e.find(".dd-options"), o.siblings(".dd-pointer"), e.find(".dd-option").eq(d)),
            a = l.closest("li"),
            c = t.settings,
            r = t.settings.data[d];
        e.find(".dd-option").removeClass("dd-option-selected"), l.addClass("dd-option-selected"), t.selectedIndex = d, t.selectedItem = a, t.selectedData = r, o.html(c.showSelectedHTML ? (r.imageSrc ? '<img class="dd-selected-image' + ("right" == c.imagePosition ? " dd-image-right" : "") + '" src="' + r.imageSrc + '" />' : "") + (r.text ? '<label class="dd-selected-text">' + r.text + "</label>" : "") + (r.description ? '<small class="dd-selected-description dd-desc' + (c.truncateDescription ? " dd-selected-description-truncated" : "") + '" >' + r.description + "</small>" : "") : r.text), s.val(r.value), t.original.val(r.value), e.data("ddslick", t), i(e), n(e), "function" == typeof c.onSelected && c.onSelected.call(this, t)
    }

    function t(d) {
        var t = d.find(".dd-select"),
            i = t.siblings(".dd-options"),
            n = t.find(".dd-pointer"),
            s = i.is(":visible");
        e(".dd-click-off-close").not(i).slideUp(50), e(".dd-pointer").removeClass("dd-pointer-up"), s ? (i.slideUp("fast"), n.removeClass("dd-pointer-up")) : (i.slideDown("fast"), n.addClass("dd-pointer-up")), o(d)
    }

    function i(e) {
        e.find(".dd-options").slideUp(50), e.find(".dd-pointer").removeClass("dd-pointer-up").removeClass("dd-pointer-up")
    }

    function n(e) {
        var d = e.find(".dd-select").css("height"),
            t = e.find(".dd-selected-description"),
            i = e.find(".dd-selected-image");
        t.length <= 0 && i.length > 0 && e.find(".dd-selected-text").css("lineHeight", d)
    }

    function o(d) {
        d.find(".dd-option").each(function() {
            var t = e(this),
                i = t.css("height"),
                n = t.find(".dd-option-description"),
                o = d.find(".dd-option-image");
            n.length <= 0 && o.length > 0 && t.find(".dd-option-text").css("lineHeight", i)
        })
    }
    e.fn.ddslick = function(d) {
        return s[d] ? s[d].apply(this, Array.prototype.slice.call(arguments, 1)) : "object" != typeof d && d ? void e.error("Method " + d + " does not exists.") : s.init.apply(this, arguments)
    };
    var s = {},
        l = {
            data: [],
            keepJSONItemsOnTop: !1,
            width: 260,
            height: null,
            background: "#eee",
            selectText: "",
            defaultSelectedIndex: null,
            truncateDescription: !0,
            imagePosition: "left",
            showSelectedHTML: !0,
            clickOffToClose: !0,
            onSelected: function() {}
        },
        a = '<div class="dd-select"><input class="dd-selected-value" type="hidden" /><a class="dd-selected"></a><span class="dd-pointer dd-pointer-down"></span></div>',
        c = '<ul class="dd-options"></ul>',
        r = '<style id="css-ddslick" type="text/css">.dd-select{ border-radius:2px; border:solid 1px #ccc; position:relative; cursor:pointer;}.dd-desc { color:#aaa; display:block; overflow: hidden; font-weight:normal; line-height: 1.4em; }.dd-selected{ overflow:hidden; display:block; padding:10px; font-weight:bold;}.dd-pointer{ width:0; height:0; position:absolute; right:10px; top:50%; margin-top:-3px;}.dd-pointer-down{ border:solid 5px transparent; border-top:solid 5px #000; }.dd-pointer-up{border:solid 5px transparent !important; border-bottom:solid 5px #000 !important; margin-top:-8px;}.dd-options{ border:solid 1px #ccc; border-top:none; list-style:none; box-shadow:0px 1px 5px #ddd; display:none; position:absolute; z-index:2000; margin:0; padding:0;background:#fff; overflow:auto;}.dd-option{ padding:10px; display:block; border-bottom:solid 1px #ddd; overflow:hidden; text-decoration:none; color:#333; cursor:pointer;-webkit-transition: all 0.25s ease-in-out; -moz-transition: all 0.25s ease-in-out;-o-transition: all 0.25s ease-in-out;-ms-transition: all 0.25s ease-in-out; }.dd-options > li:last-child > .dd-option{ border-bottom:none;}.dd-option:hover{ background:#f3f3f3; color:#000;}.dd-selected-description-truncated { text-overflow: ellipsis; white-space:nowrap; }.dd-option-selected { background:#f6f6f6; }.dd-option-image, .dd-selected-image { vertical-align:middle; float:left; margin-right:5px; max-width:64px;}.dd-image-right { float:right; margin-right:15px; margin-left:5px;}.dd-container{ position:relative;}​ .dd-selected-text { font-weight:bold}​</style>';
    e("#css-ddslick").length <= 0 && e(r).appendTo("head"), s.init = function(i) {
        var i = e.extend({}, l, i);
        return this.each(function() {
            var n = e(this),
                o = n.data("ddslick");
            if (!o) {
                {
                    var s = [];
                    i.data
                }
                n.find("option").each(function() {
                    var d = e(this),
                        t = d.data();
                    s.push({
                        text: e.trim(d.text()),
                        value: d.val(),
                        selected: d.is(":selected"),
                        description: t.description,
                        imageSrc: t.imagesrc
                    })
                }), i.keepJSONItemsOnTop ? e.merge(i.data, s) : i.data = e.merge(s, i.data);
                var l = n,
                    r = e('<div id="' + n.attr("id") + '"></div>');
                n.replaceWith(r), n = r, n.addClass("dd-container").append(a).append(c);
                var s = n.find(".dd-select"),
                    p = n.find(".dd-options");
                p.css({
                    width: i.width
                }), s.css({
                    width: i.width,
                    background: i.background
                }), n.css({
                    width: i.width
                }), null != i.height && p.css({
                    height: i.height,
                    overflow: "auto"
                }), e.each(i.data, function(e, d) {
                    d.selected && (i.defaultSelectedIndex = e), p.append('<li><a class="dd-option">' + (d.value ? ' <input class="dd-option-value" type="hidden" value="' + d.value + '" />' : "") + (d.imageSrc ? ' <img class="dd-option-image' + ("right" == i.imagePosition ? " dd-image-right" : "") + '" src="' + d.imageSrc + '" />' : "") + (d.text ? ' <label class="dd-option-text">' + d.text + "</label>" : "") + (d.description ? ' <small class="dd-option-description dd-desc">' + d.description + "</small>" : "") + "</a></li>")
                });
                var f = {
                    settings: i,
                    original: l,
                    selectedIndex: -1,
                    selectedItem: null,
                    selectedData: null
                };
                if (n.data("ddslick", f), i.selectText.length > 0 && null == i.defaultSelectedIndex) n.find(".dd-selected").html(i.selectText);
                else {
                    var u = null != i.defaultSelectedIndex && i.defaultSelectedIndex >= 0 && i.defaultSelectedIndex < i.data.length ? i.defaultSelectedIndex : 0;
                    d(n, u)
                }
                n.find(".dd-select").on("click.ddslick", function() {
                    t(n)
                }), n.find(".dd-option").on("click.ddslick", function() {
                    d(n, e(this).closest("li").index())
                }), i.clickOffToClose && (p.addClass("dd-click-off-close"), n.on("click.ddslick", function(e) {
                    e.stopPropagation()
                }), e("body").on("click", function() {
                    e(".dd-click-off-close").slideUp(50).siblings(".dd-select").find(".dd-pointer").removeClass("dd-pointer-up")
                }))
            }
        })
    }, s.select = function(t) {
        return this.each(function() {
            t.index && d(e(this), t.index)
        })
    }, s.open = function() {
        return this.each(function() {
            var d = e(this),
                i = d.data("ddslick");
            i && t(d)
        })
    }, s.close = function() {
        return this.each(function() {
            var d = e(this),
                t = d.data("ddslick");
            t && i(d)
        })
    }, s.destroy = function() {
        return this.each(function() {
            var d = e(this),
                t = d.data("ddslick");
            if (t) {
                var i = t.original;
                d.removeData("ddslick").unbind(".ddslick").replaceWith(i)
            }
        })
    }
}(jQuery);
! function(t) {
    "use strict";
    t(window.jQuery, window, document)
}(function(t, e, o, s) {
    "use strict";
    t.widget("selectBox.selectBoxIt", {
        VERSION: "3.8.1",
        options: {
            showEffect: "none",
            showEffectOptions: {},
            showEffectSpeed: "medium",
            hideEffect: "none",
            hideEffectOptions: {},
            hideEffectSpeed: "medium",
            showFirstOption: !0,
            defaultText: "",
            defaultIcon: "",
            downArrowIcon: "",
            theme: "default",
            keydownOpen: !0,
            isMobile: function() {
                var t = navigator.userAgent || navigator.vendor || e.opera;
                return /iPhone|iPod|iPad|Silk|Android|BlackBerry|Opera Mini|IEMobile/.test(t)
            },
            "native": !1,
            aggressiveChange: !1,
            selectWhenHidden: !0,
            viewport: t(e),
            similarSearch: !1,
            copyAttributes: ["title", "rel"],
            copyClasses: "button",
            nativeMousedown: !1,
            customShowHideEvent: !1,
            autoWidth: !0,
            html: !0,
            populate: "",
            dynamicPositioning: !0,
            hideCurrent: !1
        },
        getThemes: function() {
            var e = this,
                o = t(e.element).attr("data-theme") || "c";
            return {
                bootstrap: {
                    focus: "active",
                    hover: "",
                    enabled: "enabled",
                    disabled: "disabled",
                    arrow: "caret",
                    button: "btn",
                    list: "dropdown-menu",
                    container: "bootstrap",
                    open: "open"
                },
                jqueryui: {
                    focus: "ui-state-focus",
                    hover: "ui-state-hover",
                    enabled: "ui-state-enabled",
                    disabled: "ui-state-disabled",
                    arrow: "ui-icon ui-icon-triangle-1-s",
                    button: "ui-widget ui-state-default",
                    list: "ui-widget ui-widget-content",
                    container: "jqueryui",
                    open: "selectboxit-open"
                },
                jquerymobile: {
                    focus: "ui-btn-down-" + o,
                    hover: "ui-btn-hover-" + o,
                    enabled: "ui-enabled",
                    disabled: "ui-disabled",
                    arrow: "ui-icon ui-icon-arrow-d ui-icon-shadow",
                    button: "ui-btn ui-btn-icon-right ui-btn-corner-all ui-shadow ui-btn-up-" + o,
                    list: "ui-btn ui-btn-icon-right ui-btn-corner-all ui-shadow ui-btn-up-" + o,
                    container: "jquerymobile",
                    open: "selectboxit-open"
                },
                "default": {
                    focus: "selectboxit-focus",
                    hover: "selectboxit-hover",
                    enabled: "selectboxit-enabled",
                    disabled: "selectboxit-disabled",
                    arrow: "selectboxit-default-arrow",
                    button: "selectboxit-btn",
                    list: "selectboxit-list",
                    container: "selectboxit-container",
                    open: "selectboxit-open"
                }
            }
        },
        isDeferred: function(e) {
            return t.isPlainObject(e) && e.promise && e.done
        },
        _create: function(e) {
            var s = this,
                i = s.options.populate,
                n = s.options.theme;
            return s.element.is("select") ? (s.widgetProto = t.Widget.prototype, s.originalElem = s.element[0], s.selectBox = s.element, s.options.populate && s.add && !e && s.add(i), s.selectItems = s.element.find("option"), s.firstSelectItem = s.selectItems.slice(0, 1), s.documentHeight = t(o).height(), s.theme = t.isPlainObject(n) ? t.extend({}, s.getThemes()["default"], n) : s.getThemes()[n] ? s.getThemes()[n] : s.getThemes()["default"], s.currentFocus = 0, s.blur = !0, s.textArray = [], s.currentIndex = 0, s.currentText = "", s.flipped = !1, e || (s.selectBoxStyles = s.selectBox.attr("style")), s._createDropdownButton()._createUnorderedList()._copyAttributes()._replaceSelectBox()._addClasses(s.theme)._eventHandlers(), s.originalElem.disabled && s.disable && s.disable(), s._ariaAccessibility && s._ariaAccessibility(), s.isMobile = s.options.isMobile(), s._mobile && s._mobile(), s.options["native"] && this._applyNativeSelect(), s.triggerEvent("create"), s) : void 0
        },
        _createDropdownButton: function() {
            var e = this,
                o = e.originalElemId = e.originalElem.id || "",
                s = e.originalElemValue = e.originalElem.value || "",
                i = e.originalElemName = e.originalElem.name || "",
                n = e.options.copyClasses,
                r = e.selectBox.attr("class") || "";
            return e.dropdownText = t("<span/>", {
                id: o && o + "SelectBoxItText",
                "class": "selectboxit-text",
                unselectable: "on",
                text: e.firstSelectItem.text()
            }).attr("data-val", s), e.dropdownImageContainer = t("<span/>", {
                "class": "selectboxit-option-icon-container"
            }), e.dropdownImage = t("<i/>", {
                id: o && o + "SelectBoxItDefaultIcon",
                "class": "selectboxit-default-icon",
                unselectable: "on"
            }), e.dropdown = t("<span/>", {
                id: o && o + "SelectBoxIt",
                "class": "selectboxit " + ("button" === n ? r : "") + " " + (e.selectBox.prop("disabled") ? e.theme.disabled : e.theme.enabled),
                name: i,
                tabindex: e.selectBox.attr("tabindex") || "0",
                unselectable: "on"
            }).append(e.dropdownImageContainer.append(e.dropdownImage)).append(e.dropdownText), e.dropdownContainer = t("<span/>", {
                id: o && o + "SelectBoxItContainer",
                "class": "selectboxit-container " + e.theme.container + " " + ("container" === n ? r : "")
            }).append(e.dropdown), e
        },
        _createUnorderedList: function() {
            var e, o, s, i, n, r, a, l, d, c, u, p, h, b = this,
                f = "",
                m = b.originalElemId || "",
                x = t("<ul/>", {
                    id: m && m + "SelectBoxItOptions",
                    "class": "selectboxit-options",
                    tabindex: -1
                });
            if (b.options.showFirstOption || (b.selectItems.first().attr("disabled", "disabled"), b.selectItems = b.selectBox.find("option").slice(1)), b.selectItems.each(function(m) {
                    p = t(this), o = "", s = "", e = p.prop("disabled"), i = p.attr("data-icon") || "", n = p.attr("data-iconurl") || "", r = n ? "selectboxit-option-icon-url" : "", a = n ? "style=\"background-image:url('" + n + "');\"" : "", l = p.attr("data-selectedtext"), d = p.attr("data-text"), u = d ? d : p.text(), h = p.parent(), h.is("optgroup") && (o = "selectboxit-optgroup-option", 0 === p.index() && (s = '<span class="selectboxit-optgroup-header ' + h.first().attr("class") + '"data-disabled="true">' + h.first().attr("label") + "</span>")), p.attr("value", this.value), f += s + '<li data-id="' + m + '" data-val="' + this.value + '" data-disabled="' + e + '" class="' + o + " selectboxit-option " + (t(this).attr("class") || "") + '"><a class="selectboxit-option-anchor"><span class="selectboxit-option-icon-container"><i class="selectboxit-option-icon ' + i + " " + (r || b.theme.container) + '"' + a + "></i></span>" + (b.options.html ? u : b.htmlEscape(u)) + "</a></li>", c = p.attr("data-search"), b.textArray[m] = e ? "" : c ? c : u, this.selected && (b._setText(b.dropdownText, l || u), b.currentFocus = m)
                }), b.options.defaultText || b.selectBox.attr("data-text")) {
                var g = b.options.defaultText || b.selectBox.attr("data-text");
                b._setText(b.dropdownText, g), b.options.defaultText = g
            }
            return x.append(f), b.list = x, b.dropdownContainer.append(b.list), b.listItems = b.list.children("li"), b.listAnchors = b.list.find("a"), b.listItems.first().addClass("selectboxit-option-first"), b.listItems.last().addClass("selectboxit-option-last"), b.list.find("li[data-disabled='true']").not(".optgroupHeader").addClass(b.theme.disabled), b.dropdownImage.addClass(b.selectBox.attr("data-icon") || b.options.defaultIcon || b.listItems.eq(b.currentFocus).find("i").attr("class")), b.dropdownImage.attr("style", b.listItems.eq(b.currentFocus).find("i").attr("style")), b
        },
        _replaceSelectBox: function() {
            var e, o, i, n = this,
                r = n.originalElem.id || "",
                a = n.selectBox.attr("data-size"),
                l = n.listSize = a === s ? "auto" : "0" === a ? "auto" : +a;
            return n.selectBox.css("display", "none").after(n.dropdownContainer), n.dropdownContainer.appendTo("body").addClass("selectboxit-rendering"), e = n.dropdown.height(), n.downArrow = t("<i/>", {
                id: r && r + "SelectBoxItArrow",
                "class": "selectboxit-arrow",
                unselectable: "on"
            }), n.downArrowContainer = t("<span/>", {
                id: r && r + "SelectBoxItArrowContainer",
                "class": "selectboxit-arrow-container",
                unselectable: "on"
            }).append(n.downArrow), n.dropdown.append(n.downArrowContainer), n.listItems.removeClass("selectboxit-selected").eq(n.currentFocus).addClass("selectboxit-selected"), o = n.downArrowContainer.outerWidth(!0), i = n.dropdownImage.outerWidth(!0), n.options.autoWidth && (n.dropdown.css({
                width: "auto"
            }).css({
                width: n.list.outerWidth(!0) + o + i
            }), n.list.css({
                "min-width": n.dropdown.width()
            })), n.dropdownText.css({
                "max-width": n.dropdownContainer.outerWidth(!0) - (o + i)
            }), n.selectBox.after(n.dropdownContainer), n.dropdownContainer.removeClass("selectboxit-rendering"), "number" === t.type(l) && (n.maxHeight = n.listAnchors.outerHeight(!0) * l), n
        },
        _scrollToView: function(t) {
            var e = this,
                o = e.listItems.eq(e.currentFocus),
                s = e.list.scrollTop(),
                i = o.height(),
                n = o.position().top,
                r = Math.abs(n),
                a = e.list.height();
            return "search" === t ? i > a - n ? e.list.scrollTop(s + (n - (a - i))) : -1 > n && e.list.scrollTop(n - i) : "up" === t ? -1 > n && e.list.scrollTop(s - r) : "down" === t && i > a - n && e.list.scrollTop(s + (r - a + i)), e
        },
        _callbackSupport: function(e) {
            var o = this;
            return t.isFunction(e) && e.call(o, o.dropdown), o
        },
        _setText: function(t, e) {
            var o = this;
            return o.options.html ? t.html(e) : t.text(e), o
        },
        open: function(t) {
            var e = this,
                o = e.options.showEffect,
                s = e.options.showEffectSpeed,
                i = e.options.showEffectOptions,
                n = e.options["native"],
                r = e.isMobile;
            return !e.listItems.length || e.dropdown.hasClass(e.theme.disabled) ? e : (n || r || this.list.is(":visible") || (e.triggerEvent("open"), e._dynamicPositioning && e.options.dynamicPositioning && e._dynamicPositioning(), "none" === o ? e.list.show() : "show" === o || "slideDown" === o || "fadeIn" === o ? e.list[o](s) : e.list.show(o, i, s), e.list.promise().done(function() {
                e._scrollToView("search"), e.triggerEvent("opened")
            })), e._callbackSupport(t), e)
        },
        close: function(t) {
            var e = this,
                o = e.options.hideEffect,
                s = e.options.hideEffectSpeed,
                i = e.options.hideEffectOptions,
                n = e.options["native"],
                r = e.isMobile;
            return n || r || !e.list.is(":visible") || (e.triggerEvent("close"), "none" === o ? e.list.hide() : "hide" === o || "slideUp" === o || "fadeOut" === o ? e.list[o](s) : e.list.hide(o, i, s), e.list.promise().done(function() {
                e.triggerEvent("closed")
            })), e._callbackSupport(t), e
        },
        toggle: function() {
            var t = this,
                e = t.list.is(":visible");
            e ? t.close() : e || t.open()
        },
        _keyMappings: {
            38: "up",
            40: "down",
            13: "enter",
            8: "backspace",
            9: "tab",
            32: "space",
            27: "esc"
        },
        _keydownMethods: function() {
            var t = this,
                e = t.list.is(":visible") || !t.options.keydownOpen;
            return {
                down: function() {
                    t.moveDown && e && t.moveDown()
                },
                up: function() {
                    t.moveUp && e && t.moveUp()
                },
                enter: function() {
                    var e = t.listItems.eq(t.currentFocus);
                    t._update(e), "true" !== e.attr("data-preventclose") && t.close(), t.triggerEvent("enter")
                },
                tab: function() {
                    t.triggerEvent("tab-blur"), t.close()
                },
                backspace: function() {
                    t.triggerEvent("backspace")
                },
                esc: function() {
                    t.close()
                }
            }
        },
        _eventHandlers: function() {
            var e, o, s = this,
                i = s.options.nativeMousedown,
                n = s.options.customShowHideEvent,
                r = s.focusClass,
                a = s.hoverClass,
                l = s.openClass;
            return this.dropdown.on({
                "click.selectBoxIt": function() {
                    s.dropdown.trigger("focus", !0), s.originalElem.disabled || (s.triggerEvent("click"), i || n || s.toggle())
                },
                "mousedown.selectBoxIt": function() {
                    t(this).data("mdown", !0), s.triggerEvent("mousedown"), i && !n && s.toggle()
                },
                "mouseup.selectBoxIt": function() {
                    s.triggerEvent("mouseup")
                },
                "blur.selectBoxIt": function() {
                    s.blur && (s.triggerEvent("blur"), s.close(), t(this).removeClass(r))
                },
                "focus.selectBoxIt": function(e, o) {
                    var i = t(this).data("mdown");
                    t(this).removeData("mdown"), i || o || setTimeout(function() {
                        s.triggerEvent("tab-focus")
                    }, 0), o || (t(this).hasClass(s.theme.disabled) || t(this).addClass(r), s.triggerEvent("focus"))
                },
                "keydown.selectBoxIt": function(t) {
                    var e = s._keyMappings[t.keyCode],
                        o = s._keydownMethods()[e];
                    o && (o(), !s.options.keydownOpen || "up" !== e && "down" !== e || s.open()), o && "tab" !== e && t.preventDefault()
                },
                "keypress.selectBoxIt": function(t) {
                    var e = t.charCode || t.keyCode,
                        o = s._keyMappings[t.charCode || t.keyCode],
                        i = String.fromCharCode(e);
                    s.search && (!o || o && "space" === o) && s.search(i, !0, !0), "space" === o && t.preventDefault()
                },
                "mouseenter.selectBoxIt": function() {
                    s.triggerEvent("mouseenter")
                },
                "mouseleave.selectBoxIt": function() {
                    s.triggerEvent("mouseleave")
                }
            }), s.list.on({
                "mouseover.selectBoxIt": function() {
                    s.blur = !1
                },
                "mouseout.selectBoxIt": function() {
                    s.blur = !0
                },
                "focusin.selectBoxIt": function() {
                    s.dropdown.trigger("focus", !0)
                }
            }), s.list.on({
                "mousedown.selectBoxIt": function() {
                    s._update(t(this)), s.triggerEvent("option-click"), "false" === t(this).attr("data-disabled") && "true" !== t(this).attr("data-preventclose") && s.close(), setTimeout(function() {
                        s.dropdown.trigger("focus", !0)
                    }, 0)
                },
                "focusin.selectBoxIt": function() {
                    s.listItems.not(t(this)).removeAttr("data-active"), t(this).attr("data-active", "");
                    var e = s.list.is(":hidden");
                    (s.options.searchWhenHidden && e || s.options.aggressiveChange || e && s.options.selectWhenHidden) && s._update(t(this)), t(this).addClass(r)
                },
                "mouseup.selectBoxIt": function() {
                    i && !n && (s._update(t(this)), s.triggerEvent("option-mouseup"), "false" === t(this).attr("data-disabled") && "true" !== t(this).attr("data-preventclose") && s.close())
                },
                "mouseenter.selectBoxIt": function() {
                    "false" === t(this).attr("data-disabled") && (s.listItems.removeAttr("data-active"), t(this).addClass(r).attr("data-active", ""), s.listItems.not(t(this)).removeClass(r), t(this).addClass(r), s.currentFocus = +t(this).attr("data-id"))
                },
                "mouseleave.selectBoxIt": function() {
                    "false" === t(this).attr("data-disabled") && (s.listItems.not(t(this)).removeClass(r).removeAttr("data-active"), t(this).addClass(r), s.currentFocus = +t(this).attr("data-id"))
                },
                "blur.selectBoxIt": function() {
                    t(this).removeClass(r)
                }
            }, ".selectboxit-option"), s.list.on({
                "click.selectBoxIt": function(t) {
                    t.preventDefault()
                }
            }, "a"), s.selectBox.on({
                "change.selectBoxIt, internal-change.selectBoxIt": function(t, i) {
                    var n, r;
                    i || (n = s.list.find('li[data-val="' + s.originalElem.value + '"]'), n.length && (s.listItems.eq(s.currentFocus).removeClass(s.focusClass), s.currentFocus = +n.attr("data-id"))), n = s.listItems.eq(s.currentFocus), r = n.attr("data-selectedtext"), e = n.attr("data-text"), o = e ? e : n.find("a").text(), s._setText(s.dropdownText, r || o), s.dropdownText.attr("data-val", s.originalElem.value), n.find("i").attr("class") && (s.dropdownImage.attr("class", n.find("i").attr("class")).addClass("selectboxit-default-icon"), s.dropdownImage.attr("style", n.find("i").attr("style"))), s.triggerEvent("changed")
                },
                "disable.selectBoxIt": function() {
                    s.dropdown.addClass(s.theme.disabled)
                },
                "enable.selectBoxIt": function() {
                    s.dropdown.removeClass(s.theme.disabled)
                },
                "open.selectBoxIt": function() {
                    var t, e = s.list.find("li[data-val='" + s.dropdownText.attr("data-val") + "']");
                    e.length || (e = s.listItems.not("[data-disabled=true]").first()), s.currentFocus = +e.attr("data-id"), t = s.listItems.eq(s.currentFocus), s.dropdown.addClass(l).removeClass(a).addClass(r), s.listItems.removeClass(s.selectedClass).removeAttr("data-active").not(t).removeClass(r), t.addClass(s.selectedClass).addClass(r), s.options.hideCurrent && (s.listItems.show(), t.hide())
                },
                "close.selectBoxIt": function() {
                    s.dropdown.removeClass(l)
                },
                "blur.selectBoxIt": function() {
                    s.dropdown.removeClass(r)
                },
                "mouseenter.selectBoxIt": function() {
                    t(this).hasClass(s.theme.disabled) || s.dropdown.addClass(a)
                },
                "mouseleave.selectBoxIt": function() {
                    s.dropdown.removeClass(a)
                },
                destroy: function(t) {
                    t.preventDefault(), t.stopPropagation()
                }
            }), s
        },
        _update: function(t) {
            var e, o, s, i = this,
                n = i.options.defaultText || i.selectBox.attr("data-text"),
                r = i.listItems.eq(i.currentFocus);
            "false" === t.attr("data-disabled") && (e = i.listItems.eq(i.currentFocus).attr("data-selectedtext"), o = r.attr("data-text"), s = o ? o : r.text(), (n && i.options.html ? i.dropdownText.html() === n : i.dropdownText.text() === n) && i.selectBox.val() === t.attr("data-val") ? i.triggerEvent("change") : (i.selectBox.val(t.attr("data-val")), i.currentFocus = +t.attr("data-id"), i.originalElem.value !== i.dropdownText.attr("data-val") && i.triggerEvent("change")))
        },
        _addClasses: function(t) {
            var e = this,
                o = (e.focusClass = t.focus, e.hoverClass = t.hover, t.button),
                s = t.list,
                i = t.arrow,
                n = t.container;
            return e.openClass = t.open, e.selectedClass = "selectboxit-selected", e.downArrow.addClass(e.selectBox.attr("data-downarrow") || e.options.downArrowIcon || i), e.dropdownContainer.addClass(n), e.dropdown.addClass(o), e.list.addClass(s), e
        },
        refresh: function(t, e) {
            var o = this;
            return o._destroySelectBoxIt()._create(!0), e || o.triggerEvent("refresh"), o._callbackSupport(t), o
        },
        htmlEscape: function(t) {
            return String(t).replace(/&/g, "&amp;").replace(/"/g, "&quot;").replace(/'/g, "&#39;").replace(/</g, "&lt;").replace(/>/g, "&gt;")
        },
        triggerEvent: function(t) {
            var e = this,
                o = e.options.showFirstOption ? e.currentFocus : e.currentFocus - 1 >= 0 ? e.currentFocus : 0;
            return e.selectBox.trigger(t, {
                selectbox: e.selectBox,
                selectboxOption: e.selectItems.eq(o),
                dropdown: e.dropdown,
                dropdownOption: e.listItems.eq(e.currentFocus)
            }), e
        },
        _copyAttributes: function() {
            var t = this;
            return t._addSelectBoxAttributes && t._addSelectBoxAttributes(), t
        },
        _realOuterWidth: function(t) {
            if (t.is(":visible")) return t.outerWidth(!0);
            var e, o = t.clone();
            return o.css({
                visibility: "hidden",
                display: "block",
                position: "absolute"
            }).appendTo("body"), e = o.outerWidth(!0), o.remove(), e
        }
    });
    var i = t.selectBox.selectBoxIt.prototype;
    i.add = function(e, o) {
        this._populate(e, function(e) {
            var s, i, n = this,
                r = t.type(e),
                a = 0,
                l = [],
                d = n._isJSON(e),
                c = d && n._parseJSON(e);
            if (e && ("array" === r || d && c.data && "array" === t.type(c.data)) || "object" === r && e.data && "array" === t.type(e.data)) {
                for (n._isJSON(e) && (e = c), e.data && (e = e.data), i = e.length; i - 1 >= a; a += 1) s = e[a], t.isPlainObject(s) ? l.push(t("<option/>", s)) : "string" === t.type(s) && l.push(t("<option/>", {
                    text: s,
                    value: s
                }));
                n.selectBox.append(l)
            } else e && "string" === r && !n._isJSON(e) ? n.selectBox.append(e) : e && "object" === r ? n.selectBox.append(t("<option/>", e)) : e && n._isJSON(e) && t.isPlainObject(n._parseJSON(e)) && n.selectBox.append(t("<option/>", n._parseJSON(e)));
            return n.dropdown ? n.refresh(function() {
                n._callbackSupport(o)
            }, !0) : n._callbackSupport(o), n
        })
    }, i._parseJSON = function(e) {
        return JSON && JSON.parse && JSON.parse(e) || t.parseJSON(e)
    }, i._isJSON = function(t) {
        var e, o = this;
        try {
            return e = o._parseJSON(t), !0
        } catch (s) {
            return !1
        }
    }, i._populate = function(e, o) {
        var s = this;
        return e = t.isFunction(e) ? e.call() : e, s.isDeferred(e) ? e.done(function(t) {
            o.call(s, t)
        }) : o.call(s, e), s
    }, i._ariaAccessibility = function() {
        var e = this,
            o = t("label[for='" + e.originalElem.id + "']");
        return e.dropdownContainer.attr({
            role: "combobox",
            "aria-autocomplete": "list",
            "aria-haspopup": "true",
            "aria-expanded": "false",
            "aria-owns": e.list[0].id
        }), e.dropdownText.attr({
            "aria-live": "polite"
        }), e.dropdown.on({
            "disable.selectBoxIt": function() {
                e.dropdownContainer.attr("aria-disabled", "true")
            },
            "enable.selectBoxIt": function() {
                e.dropdownContainer.attr("aria-disabled", "false")
            }
        }), o.length && e.dropdownContainer.attr("aria-labelledby", o[0].id), e.list.attr({
            role: "listbox",
            "aria-hidden": "true"
        }), e.listItems.attr({
            role: "option"
        }), e.selectBox.on({
            "open.selectBoxIt": function() {
                e.list.attr("aria-hidden", "false"), e.dropdownContainer.attr("aria-expanded", "true")
            },
            "close.selectBoxIt": function() {
                e.list.attr("aria-hidden", "true"), e.dropdownContainer.attr("aria-expanded", "false")
            }
        }), e
    }, i._addSelectBoxAttributes = function() {
        var e = this;
        return e._addAttributes(e.selectBox.prop("attributes"), e.dropdown), e.selectItems.each(function(o) {
            e._addAttributes(t(this).prop("attributes"), e.listItems.eq(o))
        }), e
    }, i._addAttributes = function(e, o) {
        var s = this,
            i = s.options.copyAttributes;
        return e.length && t.each(e, function(e, s) {
            var n = s.name.toLowerCase(),
                r = s.value;
            "null" === r || -1 === t.inArray(n, i) && -1 === n.indexOf("data") || o.attr(n, r)
        }), s
    }, i.destroy = function(t) {
        var e = this;
        return e._destroySelectBoxIt(), e.widgetProto.destroy.call(e), e._callbackSupport(t), e
    }, i._destroySelectBoxIt = function() {
        var e = this;
        return e.dropdown.off(".selectBoxIt"), t.contains(e.dropdownContainer[0], e.originalElem) && e.dropdownContainer.before(e.selectBox), e.dropdownContainer.remove(), e.selectBox.removeAttr("style").attr("style", e.selectBoxStyles), e.triggerEvent("destroy"), e
    }, i.disable = function(t) {
        var e = this;
        return e.options.disabled || (e.close(), e.selectBox.attr("disabled", "disabled"), e.dropdown.removeAttr("tabindex").removeClass(e.theme.enabled).addClass(e.theme.disabled), e.setOption("disabled", !0), e.triggerEvent("disable")), e._callbackSupport(t), e
    }, i.disableOption = function(e, o) {
        var s, i, n, r = this,
            a = t.type(e);
        return "number" === a && (r.close(), s = r.selectBox.find("option").eq(e), r.triggerEvent("disable-option"), s.attr("disabled", "disabled"), r.listItems.eq(e).attr("data-disabled", "true").addClass(r.theme.disabled), r.currentFocus === e && (i = r.listItems.eq(r.currentFocus).nextAll("li").not("[data-disabled='true']").first().length, n = r.listItems.eq(r.currentFocus).prevAll("li").not("[data-disabled='true']").first().length, i ? r.moveDown() : n ? r.moveUp() : r.disable())), r._callbackSupport(o), r
    }, i._isDisabled = function() {
        var t = this;
        return t.originalElem.disabled && t.disable(), t
    }, i._dynamicPositioning = function() {
        var e = this;
        if ("number" === t.type(e.listSize)) e.list.css("max-height", e.maxHeight || "none");
        else {
            var o = e.dropdown.offset().top,
                s = e.list.data("max-height") || e.list.outerHeight(),
                i = e.dropdown.outerHeight(),
                n = e.options.viewport,
                r = n.height(),
                a = t.isWindow(n.get(0)) ? n.scrollTop() : n.offset().top,
                l = r + a >= o + i + s,
                d = !l;
            if (e.list.data("max-height") || e.list.data("max-height", e.list.outerHeight()), d)
                if (e.dropdown.offset().top - a >= s) e.list.css("max-height", s), e.list.css("top", e.dropdown.position().top - e.list.outerHeight());
                else {
                    var c = Math.abs(o + i + s - (r + a)),
                        u = Math.abs(e.dropdown.offset().top - a - s);
                    u > c ? (e.list.css("max-height", s - c - i / 2), e.list.css("top", "auto")) : (e.list.css("max-height", s - u - i / 2), e.list.css("top", e.dropdown.position().top - e.list.outerHeight()))
                } else e.list.css("max-height", s), e.list.css("top", "auto")
        }
        return e
    }, i.enable = function(t) {
        var e = this;
        return e.options.disabled && (e.triggerEvent("enable"), e.selectBox.removeAttr("disabled"), e.dropdown.attr("tabindex", 0).removeClass(e.theme.disabled).addClass(e.theme.enabled), e.setOption("disabled", !1), e._callbackSupport(t)), e
    }, i.enableOption = function(e, o) {
        var s, i = this,
            n = t.type(e);
        return "number" === n && (s = i.selectBox.find("option").eq(e), i.triggerEvent("enable-option"), s.removeAttr("disabled"), i.listItems.eq(e).attr("data-disabled", "false").removeClass(i.theme.disabled)), i._callbackSupport(o), i
    }, i.moveDown = function(t) {
        var e = this;
        e.currentFocus += 1;
        var o = "true" === e.listItems.eq(e.currentFocus).attr("data-disabled") ? !0 : !1,
            s = e.listItems.eq(e.currentFocus).nextAll("li").not("[data-disabled='true']").first().length;
        if (e.currentFocus === e.listItems.length) e.currentFocus -= 1;
        else {
            if (o && s) return e.listItems.eq(e.currentFocus - 1).blur(), void e.moveDown();
            o && !s ? e.currentFocus -= 1 : (e.listItems.eq(e.currentFocus - 1).blur().end().eq(e.currentFocus).focusin(), e._scrollToView("down"), e.triggerEvent("moveDown"))
        }
        return e._callbackSupport(t), e
    }, i.moveUp = function(t) {
        var e = this;
        e.currentFocus -= 1;
        var o = "true" === e.listItems.eq(e.currentFocus).attr("data-disabled") ? !0 : !1,
            s = e.listItems.eq(e.currentFocus).prevAll("li").not("[data-disabled='true']").first().length;
        if (-1 === e.currentFocus) e.currentFocus += 1;
        else {
            if (o && s) return e.listItems.eq(e.currentFocus + 1).blur(), void e.moveUp();
            o && !s ? e.currentFocus += 1 : (e.listItems.eq(this.currentFocus + 1).blur().end().eq(e.currentFocus).focusin(), e._scrollToView("up"), e.triggerEvent("moveUp"))
        }
        return e._callbackSupport(t), e
    }, i._setCurrentSearchOption = function(t) {
        var e = this;
        return (e.options.aggressiveChange || e.options.selectWhenHidden || e.listItems.eq(t).is(":visible")) && e.listItems.eq(t).data("disabled") !== !0 && (e.listItems.eq(e.currentFocus).blur(), e.currentIndex = t, e.currentFocus = t, e.listItems.eq(e.currentFocus).focusin(), e._scrollToView("search"), e.triggerEvent("search")), e
    }, i._searchAlgorithm = function(t, e) {
        var o, s, i, n, r = this,
            a = !1,
            l = r.textArray,
            d = r.currentText;
        for (o = t, i = l.length; i > o; o += 1) {
            for (n = l[o], s = 0; i > s; s += 1) - 1 !== l[s].search(e) && (a = !0, s = i);
            if (a || (r.currentText = r.currentText.charAt(r.currentText.length - 1).replace(/[|()\[{.+*?$\\]/g, "\\$0"), d = r.currentText), e = new RegExp(d, "gi"), d.length < 3) {
                if (e = new RegExp(d.charAt(0), "gi"), -1 !== n.charAt(0).search(e)) return r._setCurrentSearchOption(o), (n.substring(0, d.length).toLowerCase() !== d.toLowerCase() || r.options.similarSearch) && (r.currentIndex += 1), !1
            } else if (-1 !== n.search(e)) return r._setCurrentSearchOption(o), !1;
            if (n.toLowerCase() === r.currentText.toLowerCase()) return r._setCurrentSearchOption(o), r.currentText = "", !1
        }
        return !0
    }, i.search = function(t, e, o) {
        var s = this;
        o ? s.currentText += t.replace(/[|()\[{.+*?$\\]/g, "\\$0") : s.currentText = t.replace(/[|()\[{.+*?$\\]/g, "\\$0");
        var i = s._searchAlgorithm(s.currentIndex, new RegExp(s.currentText, "gi"));
        return i && s._searchAlgorithm(0, s.currentText), s._callbackSupport(e), s
    }, i._updateMobileText = function() {
        var t, e, o, s = this;
        t = s.selectBox.find("option").filter(":selected"), e = t.attr("data-text"), o = e ? e : t.text(), s._setText(s.dropdownText, o), s.list.find('li[data-val="' + t.val() + '"]').find("i").attr("class") && s.dropdownImage.attr("class", s.list.find('li[data-val="' + t.val() + '"]').find("i").attr("class")).addClass("selectboxit-default-icon")
    }, i._applyNativeSelect = function() {
        var t = this;
        return t.dropdownContainer.append(t.selectBox), t.dropdown.attr("tabindex", "-1"), t.selectBox.css({
            display: "block",
            visibility: "visible",
            width: t._realOuterWidth(t.dropdown),
            height: t.dropdown.outerHeight(),
            opacity: "0",
            position: "absolute",
            top: "0",
            left: "0",
            cursor: "pointer",
            "z-index": "999999",
            margin: t.dropdown.css("margin"),
            padding: "0",
            "-webkit-appearance": "menulist-button"
        }), t.originalElem.disabled && t.triggerEvent("disable"), this
    }, i._mobileEvents = function() {
        var t = this;
        t.selectBox.on({
            "changed.selectBoxIt": function() {
                t.hasChanged = !0, t._updateMobileText(), t.triggerEvent("option-click")
            },
            "mousedown.selectBoxIt": function() {
                t.hasChanged || !t.options.defaultText || t.originalElem.disabled || (t._updateMobileText(), t.triggerEvent("option-click"))
            },
            "enable.selectBoxIt": function() {
                t.selectBox.removeClass("selectboxit-rendering")
            },
            "disable.selectBoxIt": function() {
                t.selectBox.addClass("selectboxit-rendering")
            }
        })
    }, i._mobile = function() {
        var t = this;
        return t.isMobile && (t._applyNativeSelect(), t._mobileEvents()), this
    }, i.remove = function(e, o) {
        var s, i, n = this,
            r = t.type(e),
            a = 0,
            l = "";
        if ("array" === r) {
            for (i = e.length; i - 1 >= a; a += 1) s = e[a], "number" === t.type(s) && (l += l.length ? ", option:eq(" + s + ")" : "option:eq(" + s + ")");
            n.selectBox.find(l).remove()
        } else "number" === r ? n.selectBox.find("option").eq(e).remove() : n.selectBox.find("option").remove();
        return n.dropdown ? n.refresh(function() {
            n._callbackSupport(o)
        }, !0) : n._callbackSupport(o), n
    }, i.selectOption = function(e, o) {
        var s = this,
            i = t.type(e);
        return "number" === i ? s.selectBox.val(s.selectItems.eq(e).val()).change() : "string" === i && s.selectBox.val(e).change(), s._callbackSupport(o), s
    }, i.setOption = function(e, o, s) {
        var i = this;
        return "string" === t.type(e) && (i.options[e] = o), i.refresh(function() {
            i._callbackSupport(s)
        }, !0), i
    }, i.setOptions = function(e, o) {
        var s = this;
        return t.isPlainObject(e) && (s.options = t.extend({}, s.options, e)), s.refresh(function() {
            s._callbackSupport(o)
        }, !0), s
    }, i.wait = function(t, e) {
        var o = this;
        return o.widgetProto._delay.call(o, e, t), o
    }
});
! function(t, e, i) {
    ! function(t) {
        "use strict";
        "function" == typeof define && define.amd ? define(["jquery"], t) : jQuery && !jQuery.fn.qtip && t(jQuery)
    }(function(s) {
        "use strict";

        function o(t, e, i, o) {
            this.id = i, this.target = t, this.tooltip = k, this.elements = {
                target: t
            }, this._id = I + "-" + i, this.timers = {
                img: {}
            }, this.options = e, this.plugins = {}, this.cache = {
                event: {},
                target: s(),
                disabled: W,
                attr: o,
                onTooltip: W,
                lastClass: ""
            }, this.rendered = this.destroyed = this.disabled = this.waiting = this.hiddenDuringWait = this.positioning = this.triggering = W
        }

        function n(t) {
            return t === k || "object" !== s.type(t)
        }

        function r(t) {
            return !(s.isFunction(t) || t && t.attr || t.length || "object" === s.type(t) && (t.jquery || t.then))
        }

        function a(t) {
            var e, i, o, a;
            return n(t) ? W : (n(t.metadata) && (t.metadata = {
                type: t.metadata
            }), "content" in t && (e = t.content, n(e) || e.jquery || e.done ? e = t.content = {
                text: i = r(e) ? W : e
            } : i = e.text, "ajax" in e && (o = e.ajax, a = o && o.once !== W, delete e.ajax, e.text = function(t, e) {
                var n = i || s(this).attr(e.options.content.attr) || "Loading...",
                    r = s.ajax(s.extend({}, o, {
                        context: e
                    })).then(o.success, k, o.error).then(function(t) {
                        return t && a && e.set("content.text", t), t
                    }, function(t, i, s) {
                        e.destroyed || 0 === t.status || e.set("content.text", i + ": " + s)
                    });
                return a ? n : (e.set("content.text", n), r)
            }), "title" in e && (s.isPlainObject(e.title) && (e.button = e.title.button, e.title = e.title.text), r(e.title || W) && (e.title = W))), "position" in t && n(t.position) && (t.position = {
                my: t.position,
                at: t.position
            }), "show" in t && n(t.show) && (t.show = t.show.jquery ? {
                target: t.show
            } : t.show === z ? {
                ready: z
            } : {
                event: t.show
            }), "hide" in t && n(t.hide) && (t.hide = t.hide.jquery ? {
                target: t.hide
            } : {
                event: t.hide
            }), "style" in t && n(t.style) && (t.style = {
                classes: t.style
            }), s.each(R, function() {
                this.sanitize && this.sanitize(t)
            }), t)
        }

        function h(t, e) {
            for (var i, s = 0, o = t, n = e.split("."); o = o[n[s++]];) s < n.length && (i = o);
            return [i || t, n.pop()]
        }

        function l(t, e) {
            var i, s, o;
            for (i in this.checks)
                for (s in this.checks[i])(o = new RegExp(s, "i").exec(t)) && (e.push(o), ("builtin" === i || this.plugins[i]) && this.checks[i][s].apply(this.plugins[i] || this, e))
        }

        function c(t) {
            return Y.concat("").join(t ? "-" + t + " " : " ")
        }

        function d(t, e) {
            return e > 0 ? setTimeout(s.proxy(t, this), e) : void t.call(this)
        }

        function u(t) {
            this.tooltip.hasClass(K) || (clearTimeout(this.timers.show), clearTimeout(this.timers.hide), this.timers.show = d.call(this, function() {
                this.toggle(z, t)
            }, this.options.show.delay))
        }

        function p(t) {
            if (!this.tooltip.hasClass(K) && !this.destroyed) {
                var e = s(t.relatedTarget),
                    i = e.closest(X)[0] === this.tooltip[0],
                    o = e[0] === this.options.show.target[0];
                if (clearTimeout(this.timers.show), clearTimeout(this.timers.hide), this !== e[0] && "mouse" === this.options.position.target && i || this.options.hide.fixed && /mouse(out|leave|move)/.test(t.type) && (i || o)) try {
                    t.preventDefault(), t.stopImmediatePropagation()
                } catch (n) {} else this.timers.hide = d.call(this, function() {
                    this.toggle(W, t)
                }, this.options.hide.delay, this)
            }
        }

        function f(t) {
            !this.tooltip.hasClass(K) && this.options.hide.inactive && (clearTimeout(this.timers.inactive), this.timers.inactive = d.call(this, function() {
                this.hide(t)
            }, this.options.hide.inactive))
        }

        function g(t) {
            this.rendered && this.tooltip[0].offsetWidth > 0 && this.reposition(t)
        }

        function m(t, i, o) {
            s(e.body).delegate(t, (i.split ? i : i.join("." + I + " ")) + "." + I, function() {
                var t = x.api[s.attr(this, H)];
                t && !t.disabled && o.apply(t, arguments)
            })
        }

        function v(t, i, n) {
            var r, h, l, c, d, u = s(e.body),
                p = t[0] === e ? u : t,
                f = t.metadata ? t.metadata(n.metadata) : k,
                g = "html5" === n.metadata.type && f ? f[n.metadata.name] : k,
                m = t.data(n.metadata.name || "qtipopts");
            try {
                m = "string" == typeof m ? s.parseJSON(m) : m
            } catch (v) {}
            if (c = s.extend(z, {}, x.defaults, n, "object" == typeof m ? a(m) : k, a(g || f)), h = c.position, c.id = i, "boolean" == typeof c.content.text) {
                if (l = t.attr(c.content.attr), c.content.attr === W || !l) return W;
                c.content.text = l
            }
            if (h.container.length || (h.container = u), h.target === W && (h.target = p), c.show.target === W && (c.show.target = p), c.show.solo === z && (c.show.solo = h.container.closest("body")), c.hide.target === W && (c.hide.target = p), c.position.viewport === z && (c.position.viewport = h.container), h.container = h.container.eq(0), h.at = new T(h.at, z), h.my = new T(h.my), t.data(I))
                if (c.overwrite) t.qtip("destroy", !0);
                else if (c.overwrite === W) return W;
            return t.attr(N, i), c.suppress && (d = t.attr("title")) && t.removeAttr("title").attr(tt, d).attr("title", ""), r = new o(t, c, i, !!l), t.data(I, r), r
        }

        function y(t) {
            return t.charAt(0).toUpperCase() + t.slice(1)
        }

        function b(t, e) {
            var s, o, n = e.charAt(0).toUpperCase() + e.slice(1),
                r = (e + " " + gt.join(n + " ") + n).split(" "),
                a = 0;
            if (ft[e]) return t.css(ft[e]);
            for (; s = r[a++];)
                if ((o = t.css(s)) !== i) return ft[e] = s, o
        }

        function w(t, e) {
            return Math.ceil(parseFloat(b(t, e)))
        }

        function _(t, e) {
            this._ns = "tip", this.options = e, this.offset = e.offset, this.size = [e.width, e.height], this.init(this.qtip = t)
        }
        var x, C, T, q, j, z = !0,
            W = !1,
            k = null,
            A = "x",
            E = "y",
            M = "width",
            S = "height",
            L = "top",
            F = "left",
            P = "bottom",
            D = "right",
            O = "center",
            $ = "flipinvert",
            B = "shift",
            R = {},
            I = "qtip",
            N = "data-hasqtip",
            H = "data-qtip-id",
            Y = ["ui-widget", "ui-tooltip"],
            X = "." + I,
            U = "click dblclick mousedown mouseup mousemove mouseleave mouseenter".split(" "),
            Q = I + "-fixed",
            J = I + "-default",
            V = I + "-focus",
            G = I + "-hover",
            K = I + "-disabled",
            Z = "_replacedByqTip",
            tt = "oldtitle",
            et = {
                ie: function() {
                    for (var t = 4, i = e.createElement("div");
                        (i.innerHTML = "<!--[if gt IE " + t + "]><i></i><![endif]-->") && i.getElementsByTagName("i")[0]; t += 1);
                    return t > 4 ? t : 0 / 0
                }(),
                iOS: parseFloat(("" + (/CPU.*OS ([0-9_]{1,5})|(CPU like).*AppleWebKit.*Mobile/i.exec(navigator.userAgent) || [0, ""])[1]).replace("undefined", "3_2").replace("_", ".").replace("_", "")) || W
            };
        C = o.prototype, C._when = function(t) {
            return s.when.apply(s, t)
        }, C.render = function(t) {
            if (this.rendered || this.destroyed) return this;
            var e, i = this,
                o = this.options,
                n = this.cache,
                r = this.elements,
                a = o.content.text,
                h = o.content.title,
                l = o.content.button,
                c = o.position,
                d = ("." + this._id + " ", []);
            return s.attr(this.target[0], "aria-describedby", this._id), n.posClass = this._createPosClass((this.position = {
                my: c.my,
                at: c.at
            }).my), this.tooltip = r.tooltip = e = s("<div/>", {
                id: this._id,
                "class": [I, J, o.style.classes, n.posClass].join(" "),
                width: o.style.width || "",
                height: o.style.height || "",
                tracking: "mouse" === c.target && c.adjust.mouse,
                role: "alert",
                "aria-live": "polite",
                "aria-atomic": W,
                "aria-describedby": this._id + "-content",
                "aria-hidden": z
            }).toggleClass(K, this.disabled).attr(H, this.id).data(I, this).appendTo(c.container).append(r.content = s("<div />", {
                "class": I + "-content",
                id: this._id + "-content",
                "aria-atomic": z
            })), this.rendered = -1, this.positioning = z, h && (this._createTitle(), s.isFunction(h) || d.push(this._updateTitle(h, W))), l && this._createButton(), s.isFunction(a) || d.push(this._updateContent(a, W)), this.rendered = z, this._setWidget(), s.each(R, function(t) {
                var e;
                "render" === this.initialize && (e = this(i)) && (i.plugins[t] = e)
            }), this._unassignEvents(), this._assignEvents(), this._when(d).then(function() {
                i._trigger("render"), i.positioning = W, i.hiddenDuringWait || !o.show.ready && !t || i.toggle(z, n.event, W), i.hiddenDuringWait = W
            }), x.api[this.id] = this, this
        }, C.destroy = function(t) {
            function e() {
                if (!this.destroyed) {
                    this.destroyed = z;
                    var t, e = this.target,
                        i = e.attr(tt);
                    this.rendered && this.tooltip.stop(1, 0).find("*").remove().end().remove(), s.each(this.plugins, function() {
                        this.destroy && this.destroy()
                    });
                    for (t in this.timers) clearTimeout(this.timers[t]);
                    e.removeData(I).removeAttr(H).removeAttr(N).removeAttr("aria-describedby"), this.options.suppress && i && e.attr("title", i).removeAttr(tt), this._unassignEvents(), this.options = this.elements = this.cache = this.timers = this.plugins = this.mouse = k, delete x.api[this.id]
                }
            }
            return this.destroyed ? this.target : (t === z && "hide" !== this.triggering || !this.rendered ? e.call(this) : (this.tooltip.one("tooltiphidden", s.proxy(e, this)), !this.triggering && this.hide()), this.target)
        }, q = C.checks = {
            builtin: {
                "^id$": function(t, e, i, o) {
                    var n = i === z ? x.nextid : i,
                        r = I + "-" + n;
                    n !== W && n.length > 0 && !s("#" + r).length ? (this._id = r, this.rendered && (this.tooltip[0].id = this._id, this.elements.content[0].id = this._id + "-content", this.elements.title[0].id = this._id + "-title")) : t[e] = o
                },
                "^prerender": function(t, e, i) {
                    i && !this.rendered && this.render(this.options.show.ready)
                },
                "^content.text$": function(t, e, i) {
                    this._updateContent(i)
                },
                "^content.attr$": function(t, e, i, s) {
                    this.options.content.text === this.target.attr(s) && this._updateContent(this.target.attr(i))
                },
                "^content.title$": function(t, e, i) {
                    return i ? (i && !this.elements.title && this._createTitle(), void this._updateTitle(i)) : this._removeTitle()
                },
                "^content.button$": function(t, e, i) {
                    this._updateButton(i)
                },
                "^content.title.(text|button)$": function(t, e, i) {
                    this.set("content." + e, i)
                },
                "^position.(my|at)$": function(t, e, i) {
                    "string" == typeof i && (this.position[e] = t[e] = new T(i, "at" === e))
                },
                "^position.container$": function(t, e, i) {
                    this.rendered && this.tooltip.appendTo(i)
                },
                "^show.ready$": function(t, e, i) {
                    i && (!this.rendered && this.render(z) || this.toggle(z))
                },
                "^style.classes$": function(t, e, i, s) {
                    this.rendered && this.tooltip.removeClass(s).addClass(i)
                },
                "^style.(width|height)": function(t, e, i) {
                    this.rendered && this.tooltip.css(e, i)
                },
                "^style.widget|content.title": function() {
                    this.rendered && this._setWidget()
                },
                "^style.def": function(t, e, i) {
                    this.rendered && this.tooltip.toggleClass(J, !!i)
                },
                "^events.(render|show|move|hide|focus|blur)$": function(t, e, i) {
                    this.rendered && this.tooltip[(s.isFunction(i) ? "" : "un") + "bind"]("tooltip" + e, i)
                },
                "^(show|hide|position).(event|target|fixed|inactive|leave|distance|viewport|adjust)": function() {
                    if (this.rendered) {
                        var t = this.options.position;
                        this.tooltip.attr("tracking", "mouse" === t.target && t.adjust.mouse), this._unassignEvents(), this._assignEvents()
                    }
                }
            }
        }, C.get = function(t) {
            if (this.destroyed) return this;
            var e = h(this.options, t.toLowerCase()),
                i = e[0][e[1]];
            return i.precedance ? i.string() : i
        };
        var it = /^position\.(my|at|adjust|target|container|viewport)|style|content|show\.ready/i,
            st = /^prerender|show\.ready/i;
        C.set = function(t, e) {
            if (this.destroyed) return this;
            var i, o = this.rendered,
                n = W,
                r = this.options;
            return this.checks, "string" == typeof t ? (i = t, t = {}, t[i] = e) : t = s.extend({}, t), s.each(t, function(e, i) {
                if (o && st.test(e)) return void delete t[e];
                var a, l = h(r, e.toLowerCase());
                a = l[0][l[1]], l[0][l[1]] = i && i.nodeType ? s(i) : i, n = it.test(e) || n, t[e] = [l[0], l[1], i, a]
            }), a(r), this.positioning = z, s.each(t, s.proxy(l, this)), this.positioning = W, this.rendered && this.tooltip[0].offsetWidth > 0 && n && this.reposition("mouse" === r.position.target ? k : this.cache.event), this
        }, C._update = function(t, e) {
            var i = this,
                o = this.cache;
            return this.rendered && t ? (s.isFunction(t) && (t = t.call(this.elements.target, o.event, this) || ""), s.isFunction(t.then) ? (o.waiting = z, t.then(function(t) {
                return o.waiting = W, i._update(t, e)
            }, k, function(t) {
                return i._update(t, e)
            })) : t === W || !t && "" !== t ? W : (t.jquery && t.length > 0 ? e.empty().append(t.css({
                display: "block",
                visibility: "visible"
            })) : e.html(t), this._waitForContent(e).then(function(t) {
                i.rendered && i.tooltip[0].offsetWidth > 0 && i.reposition(o.event, !t.length)
            }))) : W
        }, C._waitForContent = function(t) {
            var e = this.cache;
            return e.waiting = z, (s.fn.imagesLoaded ? t.imagesLoaded() : s.Deferred().resolve([])).done(function() {
                e.waiting = W
            }).promise()
        }, C._updateContent = function(t, e) {
            this._update(t, this.elements.content, e)
        }, C._updateTitle = function(t, e) {
            this._update(t, this.elements.title, e) === W && this._removeTitle(W)
        }, C._createTitle = function() {
            var t = this.elements,
                e = this._id + "-title";
            t.titlebar && this._removeTitle(), t.titlebar = s("<div />", {
                "class": I + "-titlebar " + (this.options.style.widget ? c("header") : "")
            }).append(t.title = s("<div />", {
                id: e,
                "class": I + "-title",
                "aria-atomic": z
            })).insertBefore(t.content).delegate(".qtip-close", "mousedown keydown mouseup keyup mouseout", function(t) {
                s(this).toggleClass("ui-state-active ui-state-focus", "down" === t.type.substr(-4))
            }).delegate(".qtip-close", "mouseover mouseout", function(t) {
                s(this).toggleClass("ui-state-hover", "mouseover" === t.type)
            }), this.options.content.button && this._createButton()
        }, C._removeTitle = function(t) {
            var e = this.elements;
            e.title && (e.titlebar.remove(), e.titlebar = e.title = e.button = k, t !== W && this.reposition())
        }, C._createPosClass = function(t) {
            return I + "-pos-" + (t || this.options.position.my).abbrev()
        }, C.reposition = function(i, o) {
            if (!this.rendered || this.positioning || this.destroyed) return this;
            this.positioning = z;
            var n, r, a, h, l = this.cache,
                c = this.tooltip,
                d = this.options.position,
                u = d.target,
                p = d.my,
                f = d.at,
                g = d.viewport,
                m = d.container,
                v = d.adjust,
                y = v.method.split(" "),
                b = c.outerWidth(W),
                w = c.outerHeight(W),
                _ = 0,
                x = 0,
                C = c.css("position"),
                T = {
                    left: 0,
                    top: 0
                },
                q = c[0].offsetWidth > 0,
                j = i && "scroll" === i.type,
                k = s(t),
                A = m[0].ownerDocument,
                E = this.mouse;
            if (s.isArray(u) && 2 === u.length) f = {
                x: F,
                y: L
            }, T = {
                left: u[0],
                top: u[1]
            };
            else if ("mouse" === u) f = {
                x: F,
                y: L
            }, (!v.mouse || this.options.hide.distance) && l.origin && l.origin.pageX ? i = l.origin : !i || i && ("resize" === i.type || "scroll" === i.type) ? i = l.event : E && E.pageX && (i = E), "static" !== C && (T = m.offset()), A.body.offsetWidth !== (t.innerWidth || A.documentElement.clientWidth) && (r = s(e.body).offset()), T = {
                left: i.pageX - T.left + (r && r.left || 0),
                top: i.pageY - T.top + (r && r.top || 0)
            }, v.mouse && j && E && (T.left -= (E.scrollX || 0) - k.scrollLeft(), T.top -= (E.scrollY || 0) - k.scrollTop());
            else {
                if ("event" === u ? i && i.target && "scroll" !== i.type && "resize" !== i.type ? l.target = s(i.target) : i.target || (l.target = this.elements.target) : "event" !== u && (l.target = s(u.jquery ? u : this.elements.target)), u = l.target, u = s(u).eq(0), 0 === u.length) return this;
                u[0] === e || u[0] === t ? (_ = et.iOS ? t.innerWidth : u.width(), x = et.iOS ? t.innerHeight : u.height(), u[0] === t && (T = {
                    top: (g || u).scrollTop(),
                    left: (g || u).scrollLeft()
                })) : R.imagemap && u.is("area") ? n = R.imagemap(this, u, f, R.viewport ? y : W) : R.svg && u && u[0].ownerSVGElement ? n = R.svg(this, u, f, R.viewport ? y : W) : (_ = u.outerWidth(W), x = u.outerHeight(W), T = u.offset()), n && (_ = n.width, x = n.height, r = n.offset, T = n.position), T = this.reposition.offset(u, T, m), (et.iOS > 3.1 && et.iOS < 4.1 || et.iOS >= 4.3 && et.iOS < 4.33 || !et.iOS && "fixed" === C) && (T.left -= k.scrollLeft(), T.top -= k.scrollTop()), (!n || n && n.adjustable !== W) && (T.left += f.x === D ? _ : f.x === O ? _ / 2 : 0, T.top += f.y === P ? x : f.y === O ? x / 2 : 0)
            }
            return T.left += v.x + (p.x === D ? -b : p.x === O ? -b / 2 : 0), T.top += v.y + (p.y === P ? -w : p.y === O ? -w / 2 : 0), R.viewport ? (a = T.adjusted = R.viewport(this, T, d, _, x, b, w), r && a.left && (T.left += r.left), r && a.top && (T.top += r.top), a.my && (this.position.my = a.my)) : T.adjusted = {
                left: 0,
                top: 0
            }, l.posClass !== (h = this._createPosClass(this.position.my)) && c.removeClass(l.posClass).addClass(l.posClass = h), this._trigger("move", [T, g.elem || g], i) ? (delete T.adjusted, o === W || !q || isNaN(T.left) || isNaN(T.top) || "mouse" === u || !s.isFunction(d.effect) ? c.css(T) : s.isFunction(d.effect) && (d.effect.call(c, this, s.extend({}, T)), c.queue(function(t) {
                s(this).css({
                    opacity: "",
                    height: ""
                }), et.ie && this.style.removeAttribute("filter"), t()
            })), this.positioning = W, this) : this
        }, C.reposition.offset = function(t, i, o) {
            function n(t, e) {
                i.left += e * t.scrollLeft(), i.top += e * t.scrollTop()
            }
            if (!o[0]) return i;
            var r, a, h, l, c = s(t[0].ownerDocument),
                d = !!et.ie && "CSS1Compat" !== e.compatMode,
                u = o[0];
            do "static" !== (a = s.css(u, "position")) && ("fixed" === a ? (h = u.getBoundingClientRect(), n(c, -1)) : (h = s(u).position(), h.left += parseFloat(s.css(u, "borderLeftWidth")) || 0, h.top += parseFloat(s.css(u, "borderTopWidth")) || 0), i.left -= h.left + (parseFloat(s.css(u, "marginLeft")) || 0), i.top -= h.top + (parseFloat(s.css(u, "marginTop")) || 0), r || "hidden" === (l = s.css(u, "overflow")) || "visible" === l || (r = s(u))); while (u = u.offsetParent);
            return r && (r[0] !== c[0] || d) && n(r, 1), i
        };
        var ot = (T = C.reposition.Corner = function(t, e) {
            t = ("" + t).replace(/([A-Z])/, " $1").replace(/middle/gi, O).toLowerCase(), this.x = (t.match(/left|right/i) || t.match(/center/) || ["inherit"])[0].toLowerCase(), this.y = (t.match(/top|bottom|center/i) || ["inherit"])[0].toLowerCase(), this.forceY = !!e;
            var i = t.charAt(0);
            this.precedance = "t" === i || "b" === i ? E : A
        }).prototype;
        ot.invert = function(t, e) {
            this[t] = this[t] === F ? D : this[t] === D ? F : e || this[t]
        }, ot.string = function(t) {
            var e = this.x,
                i = this.y,
                s = e !== i ? "center" === e || "center" !== i && (this.precedance === E || this.forceY) ? [i, e] : [e, i] : [e];
            return t !== !1 ? s.join(" ") : s
        }, ot.abbrev = function() {
            var t = this.string(!1);
            return t[0].charAt(0) + (t[1] && t[1].charAt(0) || "")
        }, ot.clone = function() {
            return new T(this.string(), this.forceY)
        }, C.toggle = function(t, i) {
            var o = this.cache,
                n = this.options,
                r = this.tooltip;
            if (i) {
                if (/over|enter/.test(i.type) && o.event && /out|leave/.test(o.event.type) && n.show.target.add(i.target).length === n.show.target.length && r.has(i.relatedTarget).length) return this;
                o.event = s.event.fix(i)
            }
            if (this.waiting && !t && (this.hiddenDuringWait = z), !this.rendered) return t ? this.render(1) : this;
            if (this.destroyed || this.disabled) return this;
            var a, h, l, c = t ? "show" : "hide",
                d = this.options[c],
                u = (this.options[t ? "hide" : "show"], this.options.position),
                p = this.options.content,
                f = this.tooltip.css("width"),
                g = this.tooltip.is(":visible"),
                m = t || 1 === d.target.length,
                v = !i || d.target.length < 2 || o.target[0] === i.target;
            return (typeof t).search("boolean|number") && (t = !g), a = !r.is(":animated") && g === t && v, h = a ? k : !!this._trigger(c, [90]), this.destroyed ? this : (h !== W && t && this.focus(i), !h || a ? this : (s.attr(r[0], "aria-hidden", !t), t ? (this.mouse && (o.origin = s.event.fix(this.mouse)), s.isFunction(p.text) && this._updateContent(p.text, W), s.isFunction(p.title) && this._updateTitle(p.title, W), !j && "mouse" === u.target && u.adjust.mouse && (s(e).bind("mousemove." + I, this._storeMouse), j = z), f || r.css("width", r.outerWidth(W)), this.reposition(i, arguments[2]), f || r.css("width", ""), d.solo && ("string" == typeof d.solo ? s(d.solo) : s(X, d.solo)).not(r).not(d.target).qtip("hide", s.Event("tooltipsolo"))) : (clearTimeout(this.timers.show), delete o.origin, j && !s(X + '[tracking="true"]:visible', d.solo).not(r).length && (s(e).unbind("mousemove." + I), j = W), this.blur(i)), l = s.proxy(function() {
                t ? (et.ie && r[0].style.removeAttribute("filter"), r.css("overflow", ""), "string" == typeof d.autofocus && s(this.options.show.autofocus, r).focus(), this.options.show.target.trigger("qtip-" + this.id + "-inactive")) : r.css({
                    display: "",
                    visibility: "",
                    opacity: "",
                    left: "",
                    top: ""
                }), this._trigger(t ? "visible" : "hidden")
            }, this), d.effect === W || m === W ? (r[c](), l()) : s.isFunction(d.effect) ? (r.stop(1, 1), d.effect.call(r, this), r.queue("fx", function(t) {
                l(), t()
            })) : r.fadeTo(90, t ? 1 : 0, l), t && d.target.trigger("qtip-" + this.id + "-inactive"), this))
        }, C.show = function(t) {
            return this.toggle(z, t)
        }, C.hide = function(t) {
            return this.toggle(W, t)
        }, C.focus = function(t) {
            if (!this.rendered || this.destroyed) return this;
            var e = s(X),
                i = this.tooltip,
                o = parseInt(i[0].style.zIndex, 10),
                n = x.zindex + e.length;
            return i.hasClass(V) || this._trigger("focus", [n], t) && (o !== n && (e.each(function() {
                this.style.zIndex > o && (this.style.zIndex = this.style.zIndex - 1)
            }), e.filter("." + V).qtip("blur", t)), i.addClass(V)[0].style.zIndex = n), this
        }, C.blur = function(t) {
            return !this.rendered || this.destroyed ? this : (this.tooltip.removeClass(V), this._trigger("blur", [this.tooltip.css("zIndex")], t), this)
        }, C.disable = function(t) {
            return this.destroyed ? this : ("toggle" === t ? t = !(this.rendered ? this.tooltip.hasClass(K) : this.disabled) : "boolean" != typeof t && (t = z), this.rendered && this.tooltip.toggleClass(K, t).attr("aria-disabled", t), this.disabled = !!t, this)
        }, C.enable = function() {
            return this.disable(W)
        }, C._createButton = function() {
            var t = this,
                e = this.elements,
                i = e.tooltip,
                o = this.options.content.button,
                n = "string" == typeof o,
                r = n ? o : "Close tooltip";
            e.button && e.button.remove(), e.button = o.jquery ? o : s("<a />", {
                "class": "qtip-close " + (this.options.style.widget ? "" : I + "-icon"),
                title: r,
                "aria-label": r
            }).prepend(s("<span />", {
                "class": "ui-icon ui-icon-close",
                html: "&times;"
            })), e.button.appendTo(e.titlebar || i).attr("role", "button").click(function(e) {
                return i.hasClass(K) || t.hide(e), W
            })
        }, C._updateButton = function(t) {
            if (!this.rendered) return W;
            var e = this.elements.button;
            t ? this._createButton() : e.remove()
        }, C._setWidget = function() {
            var t = this.options.style.widget,
                e = this.elements,
                i = e.tooltip,
                s = i.hasClass(K);
            i.removeClass(K), K = t ? "ui-state-disabled" : "qtip-disabled", i.toggleClass(K, s), i.toggleClass("ui-helper-reset " + c(), t).toggleClass(J, this.options.style.def && !t), e.content && e.content.toggleClass(c("content"), t), e.titlebar && e.titlebar.toggleClass(c("header"), t), e.button && e.button.toggleClass(I + "-icon", !t)
        }, C._storeMouse = function(t) {
            return (this.mouse = s.event.fix(t)).type = "mousemove", this
        }, C._bind = function(t, e, i, o, n) {
            if (t && i && e.length) {
                var r = "." + this._id + (o ? "-" + o : "");
                return s(t).bind((e.split ? e : e.join(r + " ")) + r, s.proxy(i, n || this)), this
            }
        }, C._unbind = function(t, e) {
            return t && s(t).unbind("." + this._id + (e ? "-" + e : "")), this
        }, C._trigger = function(t, e, i) {
            var o = s.Event("tooltip" + t);
            return o.originalEvent = i && s.extend({}, i) || this.cache.event || k, this.triggering = t, this.tooltip.trigger(o, [this].concat(e || [])), this.triggering = W, !o.isDefaultPrevented()
        }, C._bindEvents = function(t, e, i, o, n, r) {
            var a = i.filter(o).add(o.filter(i)),
                h = [];
            a.length && (s.each(e, function(e, i) {
                var o = s.inArray(i, t);
                o > -1 && h.push(t.splice(o, 1)[0])
            }), h.length && (this._bind(a, h, function(t) {
                var e = this.rendered ? this.tooltip[0].offsetWidth > 0 : !1;
                (e ? r : n).call(this, t)
            }), i = i.not(a), o = o.not(a))), this._bind(i, t, n), this._bind(o, e, r)
        }, C._assignInitialEvents = function(t) {
            function e(t) {
                return this.disabled || this.destroyed ? W : (this.cache.event = t && s.event.fix(t), this.cache.target = t && s(t.target), clearTimeout(this.timers.show), void(this.timers.show = d.call(this, function() {
                    this.render("object" == typeof t || i.show.ready)
                }, i.prerender ? 0 : i.show.delay)))
            }
            var i = this.options,
                o = i.show.target,
                n = i.hide.target,
                r = i.show.event ? s.trim("" + i.show.event).split(" ") : [],
                a = i.hide.event ? s.trim("" + i.hide.event).split(" ") : [];
            this._bind(this.elements.target, ["remove", "removeqtip"], function() {
                this.destroy(!0)
            }, "destroy"), /mouse(over|enter)/i.test(i.show.event) && !/mouse(out|leave)/i.test(i.hide.event) && a.push("mouseleave"), this._bind(o, "mousemove", function(t) {
                this._storeMouse(t), this.cache.onTarget = z
            }), this._bindEvents(r, a, o, n, e, function() {
                return this.timers ? void clearTimeout(this.timers.show) : W
            }), (i.show.ready || i.prerender) && e.call(this, t)
        }, C._assignEvents = function() {
            var i = this,
                o = this.options,
                n = o.position,
                r = this.tooltip,
                a = o.show.target,
                h = o.hide.target,
                l = n.container,
                c = n.viewport,
                d = s(e),
                m = (s(e.body), s(t)),
                v = o.show.event ? s.trim("" + o.show.event).split(" ") : [],
                y = o.hide.event ? s.trim("" + o.hide.event).split(" ") : [];
            s.each(o.events, function(t, e) {
                i._bind(r, "toggle" === t ? ["tooltipshow", "tooltiphide"] : ["tooltip" + t], e, null, r)
            }), /mouse(out|leave)/i.test(o.hide.event) && "window" === o.hide.leave && this._bind(d, ["mouseout", "blur"], function(t) {
                /select|option/.test(t.target.nodeName) || t.relatedTarget || this.hide(t)
            }), o.hide.fixed ? h = h.add(r.addClass(Q)) : /mouse(over|enter)/i.test(o.show.event) && this._bind(h, "mouseleave", function() {
                clearTimeout(this.timers.show)
            }), ("" + o.hide.event).indexOf("unfocus") > -1 && this._bind(l.closest("html"), ["mousedown", "touchstart"], function(t) {
                var e = s(t.target),
                    i = this.rendered && !this.tooltip.hasClass(K) && this.tooltip[0].offsetWidth > 0,
                    o = e.parents(X).filter(this.tooltip[0]).length > 0;
                e[0] === this.target[0] || e[0] === this.tooltip[0] || o || this.target.has(e[0]).length || !i || this.hide(t)
            }), "number" == typeof o.hide.inactive && (this._bind(a, "qtip-" + this.id + "-inactive", f, "inactive"), this._bind(h.add(r), x.inactiveEvents, f)), this._bindEvents(v, y, a, h, u, p), this._bind(a.add(r), "mousemove", function(t) {
                if ("number" == typeof o.hide.distance) {
                    var e = this.cache.origin || {},
                        i = this.options.hide.distance,
                        s = Math.abs;
                    (s(t.pageX - e.pageX) >= i || s(t.pageY - e.pageY) >= i) && this.hide(t)
                }
                this._storeMouse(t)
            }), "mouse" === n.target && n.adjust.mouse && (o.hide.event && this._bind(a, ["mouseenter", "mouseleave"], function(t) {
                return this.cache ? void(this.cache.onTarget = "mouseenter" === t.type) : W
            }), this._bind(d, "mousemove", function(t) {
                this.rendered && this.cache.onTarget && !this.tooltip.hasClass(K) && this.tooltip[0].offsetWidth > 0 && this.reposition(t)
            })), (n.adjust.resize || c.length) && this._bind(s.event.special.resize ? c : m, "resize", g), n.adjust.scroll && this._bind(m.add(n.container), "scroll", g)
        }, C._unassignEvents = function() {
            var i = this.options,
                o = i.show.target,
                n = i.hide.target,
                r = s.grep([this.elements.target[0], this.rendered && this.tooltip[0], i.position.container[0], i.position.viewport[0], i.position.container.closest("html")[0], t, e], function(t) {
                    return "object" == typeof t
                });
            o && o.toArray && (r = r.concat(o.toArray())), n && n.toArray && (r = r.concat(n.toArray())), this._unbind(r)._unbind(r, "destroy")._unbind(r, "inactive")
        }, s(function() {
            m(X, ["mouseenter", "mouseleave"], function(t) {
                var e = "mouseenter" === t.type,
                    i = s(t.currentTarget),
                    o = s(t.relatedTarget || t.target),
                    n = this.options;
                e ? (this.focus(t), i.hasClass(Q) && !i.hasClass(K) && clearTimeout(this.timers.hide)) : "mouse" === n.position.target && n.position.adjust.mouse && n.hide.event && n.show.target && !o.closest(n.show.target[0]).length && this.hide(t), i.toggleClass(G, e)
            }), m("[" + H + "]", U, f)
        }), x = s.fn.qtip = function(t, e, o) {
            var n = ("" + t).toLowerCase(),
                r = k,
                h = s.makeArray(arguments).slice(1),
                l = h[h.length - 1],
                c = this[0] ? s.data(this[0], I) : k;
            return !arguments.length && c || "api" === n ? c : "string" == typeof t ? (this.each(function() {
                var t = s.data(this, I);
                if (!t) return z;
                if (l && l.timeStamp && (t.cache.event = l), !e || "option" !== n && "options" !== n) t[n] && t[n].apply(t, h);
                else {
                    if (o === i && !s.isPlainObject(e)) return r = t.get(e), W;
                    t.set(e, o)
                }
            }), r !== k ? r : this) : "object" != typeof t && arguments.length ? void 0 : (c = a(s.extend(z, {}, t)), this.each(function(t) {
                var e, i;
                return i = s.isArray(c.id) ? c.id[t] : c.id, i = !i || i === W || i.length < 1 || x.api[i] ? x.nextid++ : i, e = v(s(this), i, c), e === W ? z : (x.api[i] = e, s.each(R, function() {
                    "initialize" === this.initialize && this(e)
                }), void e._assignInitialEvents(l))
            }))
        }, s.qtip = o, x.api = {}, s.each({
            attr: function(t, e) {
                if (this.length) {
                    var i = this[0],
                        o = "title",
                        n = s.data(i, "qtip");
                    if (t === o && n && "object" == typeof n && n.options.suppress) return arguments.length < 2 ? s.attr(i, tt) : (n && n.options.content.attr === o && n.cache.attr && n.set("content.text", e), this.attr(tt, e))
                }
                return s.fn["attr" + Z].apply(this, arguments)
            },
            clone: function(t) {
                var e = (s([]), s.fn["clone" + Z].apply(this, arguments));
                return t || e.filter("[" + tt + "]").attr("title", function() {
                    return s.attr(this, tt)
                }).removeAttr(tt), e
            }
        }, function(t, e) {
            if (!e || s.fn[t + Z]) return z;
            var i = s.fn[t + Z] = s.fn[t];
            s.fn[t] = function() {
                return e.apply(this, arguments) || i.apply(this, arguments)
            }
        }), s.ui || (s["cleanData" + Z] = s.cleanData, s.cleanData = function(t) {
            for (var e, i = 0;
                (e = s(t[i])).length; i++)
                if (e.attr(N)) try {
                    e.triggerHandler("removeqtip")
                } catch (o) {}
                s["cleanData" + Z].apply(this, arguments)
        }), x.version = "2.2.1", x.nextid = 0, x.inactiveEvents = U, x.zindex = 15e3, x.defaults = {
            prerender: W,
            id: W,
            overwrite: z,
            suppress: z,
            content: {
                text: z,
                attr: "title",
                title: W,
                button: W
            },
            position: {
                my: "top left",
                at: "bottom right",
                target: W,
                container: W,
                viewport: W,
                adjust: {
                    x: 0,
                    y: 0,
                    mouse: z,
                    scroll: z,
                    resize: z,
                    method: "flipinvert flipinvert"
                },
                effect: function(t, e) {
                    s(this).animate(e, {
                        duration: 200,
                        queue: W
                    })
                }
            },
            show: {
                target: W,
                event: "mouseenter",
                effect: z,
                delay: 90,
                solo: W,
                ready: W,
                autofocus: W
            },
            hide: {
                target: W,
                event: "mouseleave",
                effect: z,
                delay: 0,
                fixed: W,
                inactive: W,
                leave: "window",
                distance: W
            },
            style: {
                classes: "",
                widget: W,
                width: W,
                height: W,
                def: z
            },
            events: {
                render: k,
                move: k,
                show: k,
                hide: k,
                toggle: k,
                visible: k,
                hidden: k,
                focus: k,
                blur: k
            }
        };
        var nt, rt = "margin",
            at = "border",
            ht = "color",
            lt = "background-color",
            ct = "transparent",
            dt = " !important",
            ut = !!e.createElement("canvas").getContext,
            pt = /rgba?\(0, 0, 0(, 0)?\)|transparent|#123456/i,
            ft = {},
            gt = ["Webkit", "O", "Moz", "ms"];
        if (ut) var mt = t.devicePixelRatio || 1,
            vt = function() {
                var t = e.createElement("canvas").getContext("2d");
                return t.backingStorePixelRatio || t.webkitBackingStorePixelRatio || t.mozBackingStorePixelRatio || t.msBackingStorePixelRatio || t.oBackingStorePixelRatio || 1
            }(),
            yt = mt / vt;
        else var bt = function(t, e, i) {
            return "<qtipvml:" + t + ' xmlns="urn:schemas-microsoft.com:vml" class="qtip-vml" ' + (e || "") + ' style="behavior: url(#default#VML); ' + (i || "") + '" />'
        };
        s.extend(_.prototype, {
            init: function(t) {
                var e, i;
                i = this.element = t.elements.tip = s("<div />", {
                    "class": I + "-tip"
                }).prependTo(t.tooltip), ut ? (e = s("<canvas />").appendTo(this.element)[0].getContext("2d"), e.lineJoin = "miter", e.miterLimit = 1e5, e.save()) : (e = bt("shape", 'coordorigin="0,0"', "position:absolute;"), this.element.html(e + e), t._bind(s("*", i).add(i), ["click", "mousedown"], function(t) {
                    t.stopPropagation()
                }, this._ns)), t._bind(t.tooltip, "tooltipmove", this.reposition, this._ns, this), this.create()
            },
            _swapDimensions: function() {
                this.size[0] = this.options.height, this.size[1] = this.options.width
            },
            _resetDimensions: function() {
                this.size[0] = this.options.width, this.size[1] = this.options.height
            },
            _useTitle: function(t) {
                var e = this.qtip.elements.titlebar;
                return e && (t.y === L || t.y === O && this.element.position().top + this.size[1] / 2 + this.options.offset < e.outerHeight(z))
            },
            _parseCorner: function(t) {
                var e = this.qtip.options.position.my;
                return t === W || e === W ? t = W : t === z ? t = new T(e.string()) : t.string || (t = new T(t), t.fixed = z), t
            },
            _parseWidth: function(t, e, i) {
                var s = this.qtip.elements,
                    o = at + y(e) + "Width";
                return (i ? w(i, o) : w(s.content, o) || w(this._useTitle(t) && s.titlebar || s.content, o) || w(s.tooltip, o)) || 0
            },
            _parseRadius: function(t) {
                var e = this.qtip.elements,
                    i = at + y(t.y) + y(t.x) + "Radius";
                return et.ie < 9 ? 0 : w(this._useTitle(t) && e.titlebar || e.content, i) || w(e.tooltip, i) || 0
            },
            _invalidColour: function(t, e, i) {
                var s = t.css(e);
                return !s || i && s === t.css(i) || pt.test(s) ? W : s
            },
            _parseColours: function(t) {
                var e = this.qtip.elements,
                    i = this.element.css("cssText", ""),
                    o = at + y(t[t.precedance]) + y(ht),
                    n = this._useTitle(t) && e.titlebar || e.content,
                    r = this._invalidColour,
                    a = [];
                return a[0] = r(i, lt) || r(n, lt) || r(e.content, lt) || r(e.tooltip, lt) || i.css(lt), a[1] = r(i, o, ht) || r(n, o, ht) || r(e.content, o, ht) || r(e.tooltip, o, ht) || e.tooltip.css(o), s("*", i).add(i).css("cssText", lt + ":" + ct + dt + ";" + at + ":0" + dt + ";"), a
            },
            _calculateSize: function(t) {
                var e, i, s, o = t.precedance === E,
                    n = this.options.width,
                    r = this.options.height,
                    a = "c" === t.abbrev(),
                    h = (o ? n : r) * (a ? .5 : 1),
                    l = Math.pow,
                    c = Math.round,
                    d = Math.sqrt(l(h, 2) + l(r, 2)),
                    u = [this.border / h * d, this.border / r * d];
                return u[2] = Math.sqrt(l(u[0], 2) - l(this.border, 2)), u[3] = Math.sqrt(l(u[1], 2) - l(this.border, 2)), e = d + u[2] + u[3] + (a ? 0 : u[0]), i = e / d, s = [c(i * n), c(i * r)], o ? s : s.reverse()
            },
            _calculateTip: function(t, e, i) {
                i = i || 1, e = e || this.size;
                var s = e[0] * i,
                    o = e[1] * i,
                    n = Math.ceil(s / 2),
                    r = Math.ceil(o / 2),
                    a = {
                        br: [0, 0, s, o, s, 0],
                        bl: [0, 0, s, 0, 0, o],
                        tr: [0, o, s, 0, s, o],
                        tl: [0, 0, 0, o, s, o],
                        tc: [0, o, n, 0, s, o],
                        bc: [0, 0, s, 0, n, o],
                        rc: [0, 0, s, r, 0, o],
                        lc: [s, 0, s, o, 0, r]
                    };
                return a.lt = a.br, a.rt = a.bl, a.lb = a.tr, a.rb = a.tl, a[t.abbrev()]
            },
            _drawCoords: function(t, e) {
                t.beginPath(), t.moveTo(e[0], e[1]), t.lineTo(e[2], e[3]), t.lineTo(e[4], e[5]), t.closePath()
            },
            create: function() {
                var t = this.corner = (ut || et.ie) && this._parseCorner(this.options.corner);
                return (this.enabled = !!this.corner && "c" !== this.corner.abbrev()) && (this.qtip.cache.corner = t.clone(), this.update()), this.element.toggle(this.enabled), this.corner
            },
            update: function(e, i) {
                if (!this.enabled) return this;
                var o, n, r, a, h, l, c, d, u = this.qtip.elements,
                    p = this.element,
                    f = p.children(),
                    g = this.options,
                    m = this.size,
                    v = g.mimic,
                    y = Math.round;
                e || (e = this.qtip.cache.corner || this.corner), v === W ? v = e : (v = new T(v), v.precedance = e.precedance, "inherit" === v.x ? v.x = e.x : "inherit" === v.y ? v.y = e.y : v.x === v.y && (v[e.precedance] = e[e.precedance])), n = v.precedance, e.precedance === A ? this._swapDimensions() : this._resetDimensions(), o = this.color = this._parseColours(e), o[1] !== ct ? (d = this.border = this._parseWidth(e, e[e.precedance]), g.border && 1 > d && !pt.test(o[1]) && (o[0] = o[1]), this.border = d = g.border !== z ? g.border : d) : this.border = d = 0, c = this.size = this._calculateSize(e), p.css({
                    width: c[0],
                    height: c[1],
                    lineHeight: c[1] + "px"
                }), l = e.precedance === E ? [y(v.x === F ? d : v.x === D ? c[0] - m[0] - d : (c[0] - m[0]) / 2), y(v.y === L ? c[1] - m[1] : 0)] : [y(v.x === F ? c[0] - m[0] : 0), y(v.y === L ? d : v.y === P ? c[1] - m[1] - d : (c[1] - m[1]) / 2)], ut ? (r = f[0].getContext("2d"), r.restore(), r.save(), r.clearRect(0, 0, 6e3, 6e3), a = this._calculateTip(v, m, yt), h = this._calculateTip(v, this.size, yt), f.attr(M, c[0] * yt).attr(S, c[1] * yt), f.css(M, c[0]).css(S, c[1]), this._drawCoords(r, h), r.fillStyle = o[1], r.fill(), r.translate(l[0] * yt, l[1] * yt), this._drawCoords(r, a), r.fillStyle = o[0], r.fill()) : (a = this._calculateTip(v), a = "m" + a[0] + "," + a[1] + " l" + a[2] + "," + a[3] + " " + a[4] + "," + a[5] + " xe", l[2] = d && /^(r|b)/i.test(e.string()) ? 8 === et.ie ? 2 : 1 : 0, f.css({
                    coordsize: c[0] + d + " " + (c[1] + d),
                    antialias: "" + (v.string().indexOf(O) > -1),
                    left: l[0] - l[2] * Number(n === A),
                    top: l[1] - l[2] * Number(n === E),
                    width: c[0] + d,
                    height: c[1] + d
                }).each(function(t) {
                    var e = s(this);
                    e[e.prop ? "prop" : "attr"]({
                        coordsize: c[0] + d + " " + (c[1] + d),
                        path: a,
                        fillcolor: o[0],
                        filled: !!t,
                        stroked: !t
                    }).toggle(!(!d && !t)), !t && e.html(bt("stroke", 'weight="' + 2 * d + 'px" color="' + o[1] + '" miterlimit="1000" joinstyle="miter"'))
                })), t.opera && setTimeout(function() {
                    u.tip.css({
                        display: "inline-block",
                        visibility: "visible"
                    })
                }, 1), i !== W && this.calculate(e, c)
            },
            calculate: function(t, e) {
                if (!this.enabled) return W;
                var i, o, n = this,
                    r = this.qtip.elements,
                    a = this.element,
                    h = this.options.offset,
                    l = (r.tooltip.hasClass("ui-widget"), {});
                return t = t || this.corner, i = t.precedance, e = e || this._calculateSize(t), o = [t.x, t.y], i === A && o.reverse(), s.each(o, function(s, o) {
                    var a, c, d;
                    o === O ? (a = i === E ? F : L, l[a] = "50%",
                        l[rt + "-" + a] = -Math.round(e[i === E ? 0 : 1] / 2) + h) : (a = n._parseWidth(t, o, r.tooltip), c = n._parseWidth(t, o, r.content), d = n._parseRadius(t), l[o] = Math.max(-n.border, s ? c : h + (d > a ? d : -a)))
                }), l[t[i]] -= e[i === A ? 0 : 1], a.css({
                    margin: "",
                    top: "",
                    bottom: "",
                    left: "",
                    right: ""
                }).css(l), l
            },
            reposition: function(t, e, s) {
                function o(t, e, i, s, o) {
                    t === B && l.precedance === e && c[s] && l[i] !== O ? l.precedance = l.precedance === A ? E : A : t !== B && c[s] && (l[e] = l[e] === O ? c[s] > 0 ? s : o : l[e] === s ? o : s)
                }

                function n(t, e, o) {
                    l[t] === O ? g[rt + "-" + e] = f[t] = r[rt + "-" + e] - c[e] : (a = r[o] !== i ? [c[e], -r[e]] : [-c[e], r[e]], (f[t] = Math.max(a[0], a[1])) > a[0] && (s[e] -= c[e], f[e] = W), g[r[o] !== i ? o : e] = f[t])
                }
                if (this.enabled) {
                    var r, a, h = e.cache,
                        l = this.corner.clone(),
                        c = s.adjusted,
                        d = e.options.position.adjust.method.split(" "),
                        u = d[0],
                        p = d[1] || d[0],
                        f = {
                            left: W,
                            top: W,
                            x: 0,
                            y: 0
                        },
                        g = {};
                    this.corner.fixed !== z && (o(u, A, E, F, D), o(p, E, A, L, P), (l.string() !== h.corner.string() || h.cornerTop !== c.top || h.cornerLeft !== c.left) && this.update(l, W)), r = this.calculate(l), r.right !== i && (r.left = -r.right), r.bottom !== i && (r.top = -r.bottom), r.user = this.offset, (f.left = u === B && !!c.left) && n(A, F, D), (f.top = p === B && !!c.top) && n(E, L, P), this.element.css(g).toggle(!(f.x && f.y || l.x === O && f.y || l.y === O && f.x)), s.left -= r.left.charAt ? r.user : u !== B || f.top || !f.left && !f.top ? r.left + this.border : 0, s.top -= r.top.charAt ? r.user : p !== B || f.left || !f.left && !f.top ? r.top + this.border : 0, h.cornerLeft = c.left, h.cornerTop = c.top, h.corner = l.clone()
                }
            },
            destroy: function() {
                this.qtip._unbind(this.qtip.tooltip, this._ns), this.qtip.elements.tip && this.qtip.elements.tip.find("*").remove().end().remove()
            }
        }), nt = R.tip = function(t) {
            return new _(t, t.options.style.tip)
        }, nt.initialize = "render", nt.sanitize = function(t) {
            if (t.style && "tip" in t.style) {
                var e = t.style.tip;
                "object" != typeof e && (e = t.style.tip = {
                    corner: e
                }), /string|boolean/i.test(typeof e.corner) || (e.corner = z)
            }
        }, q.tip = {
            "^position.my|style.tip.(corner|mimic|border)$": function() {
                this.create(), this.qtip.reposition()
            },
            "^style.tip.(height|width)$": function(t) {
                this.size = [t.width, t.height], this.update(), this.qtip.reposition()
            },
            "^content.title|style.(classes|widget)$": function() {
                this.update()
            }
        }, s.extend(z, x.defaults, {
            style: {
                tip: {
                    corner: z,
                    mimic: W,
                    width: 6,
                    height: 6,
                    border: z,
                    offset: 0
                }
            }
        }), R.viewport = function(i, s, o, n, r, a, h) {
            function l(t, e, i, o, n, r, a, h, l) {
                var c = s[n],
                    y = w[t],
                    b = _[t],
                    x = i === B,
                    C = y === n ? l : y === r ? -l : -l / 2,
                    T = b === n ? h : b === r ? -h : -h / 2,
                    q = m[n] + v[n] - (p ? 0 : u[n]),
                    j = q - c,
                    z = c + l - (a === M ? f : g) - q,
                    W = C - (w.precedance === t || y === w[e] ? T : 0) - (b === O ? h / 2 : 0);
                return x ? (W = (y === n ? 1 : -1) * C, s[n] += j > 0 ? j : z > 0 ? -z : 0, s[n] = Math.max(-u[n] + v[n], c - W, Math.min(Math.max(-u[n] + v[n] + (a === M ? f : g), c + W), s[n], "center" === y ? c - C : 1e9))) : (o *= i === $ ? 2 : 0, j > 0 && (y !== n || z > 0) ? (s[n] -= W + o, d.invert(t, n)) : z > 0 && (y !== r || j > 0) && (s[n] -= (y === O ? -W : W) + o, d.invert(t, r)), s[n] < m && -s[n] > z && (s[n] = c, d = w.clone())), s[n] - c
            }
            var c, d, u, p, f, g, m, v, y = o.target,
                b = i.elements.tooltip,
                w = o.my,
                _ = o.at,
                x = o.adjust,
                C = x.method.split(" "),
                T = C[0],
                q = C[1] || C[0],
                j = o.viewport,
                z = o.container,
                k = (i.cache, {
                    left: 0,
                    top: 0
                });
            return j.jquery && y[0] !== t && y[0] !== e.body && "none" !== x.method ? (u = z.offset() || k, p = "static" === z.css("position"), c = "fixed" === b.css("position"), f = j[0] === t ? j.width() : j.outerWidth(W), g = j[0] === t ? j.height() : j.outerHeight(W), m = {
                left: c ? 0 : j.scrollLeft(),
                top: c ? 0 : j.scrollTop()
            }, v = j.offset() || k, ("shift" !== T || "shift" !== q) && (d = w.clone()), k = {
                left: "none" !== T ? l(A, E, T, x.x, F, D, M, n, a) : 0,
                top: "none" !== q ? l(E, A, q, x.y, L, P, S, r, h) : 0,
                my: d
            }) : k
        }
    })
}(window, document);
window.mobilecheck = function() {
    var i = !1;
    return function(a) {
        (/(ipad|playbook|silk|android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) && (i = !0)
    }(navigator.userAgent || navigator.vendor || window.opera), i
};
! function(e) {
    "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof exports ? module.exports = e(require("jquery")) : e(jQuery)
}(function(e) {
    if (!e.support.cors && e.ajaxTransport && window.XDomainRequest) {
        var t = /^https?:\/\//i,
            o = /^get|post$/i,
            n = new RegExp("^" + location.protocol, "i");
        e.ajaxTransport("* text html xml json", function(r, s, a) {
            if (r.crossDomain && r.async && o.test(r.type) && t.test(r.url) && n.test(r.url)) {
                var i = null;
                return {
                    send: function(t, o) {
                        var n = "",
                            a = (s.dataType || "").toLowerCase();
                        i = new XDomainRequest, /^\d+$/.test(s.timeout) && (i.timeout = s.timeout), i.ontimeout = function() {
                            o(500, "timeout")
                        }, i.onload = function() {
                            var t = "Content-Length: " + i.responseText.length + "\r\nContent-Type: " + i.contentType,
                                n = {
                                    code: 200,
                                    message: "success"
                                },
                                r = {
                                    text: i.responseText
                                };
                            try {
                                if ("html" === a || /text\/html/i.test(i.contentType)) r.html = i.responseText;
                                else if ("json" === a || "text" !== a && /\/json/i.test(i.contentType)) try {
                                    r.json = e.parseJSON(i.responseText)
                                } catch (s) {
                                    n.code = 500, n.message = "parseerror"
                                } else if ("xml" === a || "text" !== a && /\/xml/i.test(i.contentType)) {
                                    var c = new ActiveXObject("Microsoft.XMLDOM");
                                    c.async = !1;
                                    try {
                                        c.loadXML(i.responseText)
                                    } catch (s) {
                                        c = void 0
                                    }
                                    if (!c || !c.documentElement || c.getElementsByTagName("parsererror").length) throw n.code = 500, n.message = "parseerror", "Invalid XML: " + i.responseText;
                                    r.xml = c
                                }
                            } catch (p) {
                                throw p
                            } finally {
                                o(n.code, n.message, r, t)
                            }
                        }, i.onprogress = function() {}, i.onerror = function() {
                            o(500, "error", {
                                text: i.responseText
                            })
                        }, s.data && (n = "string" === e.type(s.data) ? s.data : e.param(s.data)), i.open(r.type, r.url), i.send(n)
                    },
                    abort: function() {
                        i && i.abort()
                    }
                }
            }
        })
    }
});
! function(e) {
    "function" == typeof define && define.amd ? define(["jquery"], e) : e("object" == typeof module && module.exports ? require("jquery") : jQuery)
}(function(e) {
    function t(t) {
        var a = {},
            l = /^jQuery\d+$/;
        return e.each(t.attributes, function(e, t) {
            t.specified && !l.test(t.name) && (a[t.name] = t.value)
        }), a
    }

    function a(t, a) {
        var l = this,
            o = e(l);
        if (l.value == o.attr("placeholder") && o.hasClass(p.customClass))
            if (o.data("placeholder-password")) {
                if (o = o.hide().nextAll('input[type="password"]:first').show().attr("id", o.removeAttr("id").data("placeholder-id")), t === !0) return o[0].value = a;
                o.focus()
            } else l.value = "", o.removeClass(p.customClass), l == r() && l.select()
    }

    function l() {
        var l, r = this,
            o = e(r),
            n = this.id;
        if ("" === r.value) {
            if ("password" === r.type) {
                if (!o.data("placeholder-textinput")) {
                    try {
                        l = o.clone().attr({
                            type: "text"
                        })
                    } catch (s) {
                        l = e("<input>").attr(e.extend(t(this), {
                            type: "text"
                        }))
                    }
                    l.removeAttr("name").data({
                        "placeholder-password": o,
                        "placeholder-id": n
                    }).bind("focus.placeholder", a), o.data({
                        "placeholder-textinput": l,
                        "placeholder-id": n
                    }).before(l)
                }
                o = o.removeAttr("id").hide().prevAll('input[type="text"]:first').attr("id", n).show()
            }
            o.addClass(p.customClass), o[0].value = o.attr("placeholder")
        } else o.removeClass(p.customClass)
    }

    function r() {
        try {
            return document.activeElement
        } catch (e) {}
    }
    var o, n, s = "[object OperaMini]" == Object.prototype.toString.call(window.operamini),
        c = "placeholder" in document.createElement("input") && !s,
        u = "placeholder" in document.createElement("textarea") && !s,
        d = e.valHooks,
        i = e.propHooks;
    if (c && u) n = e.fn.placeholder = function() {
        return this
    }, n.input = n.textarea = !0;
    else {
        var p = {};
        n = e.fn.placeholder = function(t) {
            var r = {
                customClass: "placeholder"
            };
            p = e.extend({}, r, t);
            var o = this;
            return o.filter((c ? "textarea" : ":input") + "[placeholder]").not("." + p.customClass).bind({
                "focus.placeholder": a,
                "blur.placeholder": l
            }).data("placeholder-enabled", !0).trigger("blur.placeholder"), o
        }, n.input = c, n.textarea = u, o = {
            get: function(t) {
                var a = e(t),
                    l = a.data("placeholder-password");
                return l ? l[0].value : a.data("placeholder-enabled") && a.hasClass(p.customClass) ? "" : t.value
            },
            set: function(t, o) {
                var n = e(t),
                    s = n.data("placeholder-password");
                return s ? s[0].value = o : n.data("placeholder-enabled") ? ("" === o ? (t.value = o, t != r() && l.call(t)) : n.hasClass(p.customClass) ? a.call(t, !0, o) || (t.value = o) : t.value = o, n) : t.value = o
            }
        }, c || (d.input = o, i.value = o), u || (d.textarea = o, i.value = o), e(function() {
            e(document).delegate("form", "submit.placeholder", function() {
                var t = e("." + p.customClass, this).each(a);
                setTimeout(function() {
                    t.each(l)
                }, 10)
            })
        }), e(window).bind("beforeunload.placeholder", function() {
            e("." + p.customClass).each(function() {
                this.value = ""
            })
        })
    }
});
window.matchMedia || (window.matchMedia = function() {
        "use strict";
        var e = window.styleMedia || window.media;
        if (!e) {
            var t = document.createElement("style"),
                r = document.getElementsByTagName("script")[0],
                n = null;
            t.type = "text/css", t.id = "matchmediajs-test", r.parentNode.insertBefore(t, r), n = "getComputedStyle" in window && window.getComputedStyle(t, null) || t.currentStyle, e = {
                matchMedium: function(e) {
                    var r = "@media " + e + "{ #matchmediajs-test { width: 1px; } }";
                    return t.styleSheet ? t.styleSheet.cssText = r : t.textContent = r, "1px" === n.width
                }
            }
        }
        return function(t) {
            return {
                matches: e.matchMedium(t || "all"),
                media: t || "all"
            }
        }
    }()),
    function(e, t, r) {
        "use strict";

        function n(t) {
            "object" == typeof module && "object" == typeof module.exports ? module.exports = t : "function" == typeof define && define.amd && define("picturefill", function() {
                return t
            }), "object" == typeof e && (e.picturefill = t)
        }

        function i(e) {
            var t, r, n, i, s, c = e || {};
            t = c.elements || o.getAllElements();
            for (var l = 0, u = t.length; u > l; l++)
                if (r = t[l], n = r.parentNode, i = void 0, s = void 0, "IMG" === r.nodeName.toUpperCase() && (r[o.ns] || (r[o.ns] = {}), c.reevaluate || !r[o.ns].evaluated)) {
                    if (n && "PICTURE" === n.nodeName.toUpperCase()) {
                        if (o.removeVideoShim(n), i = o.getMatch(r, n), i === !1) continue
                    } else i = void 0;
                    (n && "PICTURE" === n.nodeName.toUpperCase() || !o.sizesSupported && r.srcset && a.test(r.srcset)) && o.dodgeSrcset(r), i ? (s = o.processSourceSet(i), o.applyBestCandidate(s, r)) : (s = o.processSourceSet(r), (void 0 === r.srcset || r[o.ns].srcset) && o.applyBestCandidate(s, r)), r[o.ns].evaluated = !0
                }
        }

        function s() {
            function r() {
                clearTimeout(n), n = setTimeout(a, 60)
            }
            o.initTypeDetects(), i();
            var n, s = setInterval(function() {
                    return i(), /^loaded|^i|^c/.test(t.readyState) ? void clearInterval(s) : void 0
                }, 250),
                a = function() {
                    i({
                        reevaluate: !0
                    })
                };
            e.addEventListener ? e.addEventListener("resize", r, !1) : e.attachEvent && e.attachEvent("onresize", r)
        }
        if (e.HTMLPictureElement) return void n(function() {});
        t.createElement("picture");
        var o = e.picturefill || {},
            a = /\s+\+?\d+(e\d+)?w/;
        o.ns = "picturefill",
            function() {
                o.srcsetSupported = "srcset" in r, o.sizesSupported = "sizes" in r, o.curSrcSupported = "currentSrc" in r
            }(), o.trim = function(e) {
                return e.trim ? e.trim() : e.replace(/^\s+|\s+$/g, "")
            }, o.makeUrl = function() {
                var e = t.createElement("a");
                return function(t) {
                    return e.href = t, e.href
                }
            }(), o.restrictsMixedContent = function() {
                return "https:" === e.location.protocol
            }, o.matchesMedia = function(t) {
                return e.matchMedia && e.matchMedia(t).matches
            }, o.getDpr = function() {
                return e.devicePixelRatio || 1
            }, o.getWidthFromLength = function(e) {
                var r;
                if (!e || e.indexOf("%") > -1 != 0 || !(parseFloat(e) > 0 || e.indexOf("calc(") > -1)) return !1;
                e = e.replace("vw", "%"), o.lengthEl || (o.lengthEl = t.createElement("div"), o.lengthEl.style.cssText = "border:0;display:block;font-size:1em;left:0;margin:0;padding:0;position:absolute;visibility:hidden", o.lengthEl.className = "helper-from-picturefill-js"), o.lengthEl.style.width = "0px";
                try {
                    o.lengthEl.style.width = e
                } catch (n) {}
                return t.body.appendChild(o.lengthEl), r = o.lengthEl.offsetWidth, 0 >= r && (r = !1), t.body.removeChild(o.lengthEl), r
            }, o.detectTypeSupport = function(t, r) {
                var n = new e.Image;
                return n.onerror = function() {
                    o.types[t] = !1, i()
                }, n.onload = function() {
                    o.types[t] = 1 === n.width, i()
                }, n.src = r, "pending"
            }, o.types = o.types || {}, o.initTypeDetects = function() {
                o.types["image/jpeg"] = !0, o.types["image/gif"] = !0, o.types["image/png"] = !0, o.types["image/svg+xml"] = t.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Image", "1.1"), o.types["image/webp"] = o.detectTypeSupport("image/webp", "data:image/webp;base64,UklGRh4AAABXRUJQVlA4TBEAAAAvAAAAAAfQ//73v/+BiOh/AAA=")
            }, o.verifyTypeSupport = function(e) {
                var t = e.getAttribute("type");
                if (null === t || "" === t) return !0;
                var r = o.types[t];
                return "string" == typeof r && "pending" !== r ? (o.types[t] = o.detectTypeSupport(t, r), "pending") : "function" == typeof r ? (r(), "pending") : r
            }, o.parseSize = function(e) {
                var t = /(\([^)]+\))?\s*(.+)/g.exec(e);
                return {
                    media: t && t[1],
                    length: t && t[2]
                }
            }, o.findWidthFromSourceSize = function(r) {
                for (var n, i = o.trim(r).split(/\s*,\s*/), s = 0, a = i.length; a > s; s++) {
                    var c = i[s],
                        l = o.parseSize(c),
                        u = l.length,
                        d = l.media;
                    if (u && (!d || o.matchesMedia(d)) && (n = o.getWidthFromLength(u))) break
                }
                return n || Math.max(e.innerWidth || 0, t.documentElement.clientWidth)
            }, o.parseSrcset = function(e) {
                for (var t = [];
                    "" !== e;) {
                    e = e.replace(/^\s+/g, "");
                    var r, n = e.search(/\s/g),
                        i = null;
                    if (-1 !== n) {
                        r = e.slice(0, n);
                        var s = r.slice(-1);
                        if (("," === s || "" === r) && (r = r.replace(/,+$/, ""), i = ""), e = e.slice(n + 1), null === i) {
                            var o = e.indexOf(","); - 1 !== o ? (i = e.slice(0, o), e = e.slice(o + 1)) : (i = e, e = "")
                        }
                    } else r = e, e = "";
                    (r || i) && t.push({
                        url: r,
                        descriptor: i
                    })
                }
                return t
            }, o.parseDescriptor = function(e, t) {
                var r, n = t || "100vw",
                    i = e && e.replace(/(^\s+|\s+$)/g, ""),
                    s = o.findWidthFromSourceSize(n);
                if (i)
                    for (var a = i.split(" "), c = a.length - 1; c >= 0; c--) {
                        var l = a[c],
                            u = l && l.slice(l.length - 1);
                        if ("h" !== u && "w" !== u || o.sizesSupported) {
                            if ("x" === u) {
                                var d = l && parseFloat(l, 10);
                                r = d && !isNaN(d) ? d : 1
                            }
                        } else r = parseFloat(parseInt(l, 10) / s)
                    }
                return r || 1
            }, o.getCandidatesFromSourceSet = function(e, t) {
                for (var r = o.parseSrcset(e), n = [], i = 0, s = r.length; s > i; i++) {
                    var a = r[i];
                    n.push({
                        url: a.url,
                        resolution: o.parseDescriptor(a.descriptor, t)
                    })
                }
                return n
            }, o.dodgeSrcset = function(e) {
                e.srcset && (e[o.ns].srcset = e.srcset, e.srcset = "", e.setAttribute("data-pfsrcset", e[o.ns].srcset))
            }, o.processSourceSet = function(e) {
                var t = e.getAttribute("srcset"),
                    r = e.getAttribute("sizes"),
                    n = [];
                return "IMG" === e.nodeName.toUpperCase() && e[o.ns] && e[o.ns].srcset && (t = e[o.ns].srcset), t && (n = o.getCandidatesFromSourceSet(t, r)), n
            }, o.backfaceVisibilityFix = function(e) {
                var t = e.style || {},
                    r = "webkitBackfaceVisibility" in t,
                    n = t.zoom;
                r && (t.zoom = ".999", r = e.offsetWidth, t.zoom = n)
            }, o.setIntrinsicSize = function() {
                var r = {},
                    n = function(e, t, r) {
                        t && e.setAttribute("width", parseInt(t / r, 10))
                    };
                return function(i, s) {
                    var a;
                    i[o.ns] && !e.pfStopIntrinsicSize && (void 0 === i[o.ns].dims && (i[o.ns].dims = i.getAttribute("width") || i.getAttribute("height")), i[o.ns].dims || (s.url in r ? n(i, r[s.url], s.resolution) : (a = t.createElement("img"), a.onload = function() {
                        if (r[s.url] = a.width, !r[s.url]) try {
                            t.body.appendChild(a), r[s.url] = a.width || a.offsetWidth, t.body.removeChild(a)
                        } catch (e) {}
                        i.src === s.url && n(i, r[s.url], s.resolution), i = null, a.onload = null, a = null
                    }, a.src = s.url)))
                }
            }(), o.applyBestCandidate = function(e, t) {
                var r, n, i;
                e.sort(o.ascendingSort), n = e.length, i = e[n - 1];
                for (var s = 0; n > s; s++)
                    if (r = e[s], r.resolution >= o.getDpr()) {
                        i = r;
                        break
                    }
                i && (i.url = o.makeUrl(i.url), t.src !== i.url && (o.restrictsMixedContent() && "http:" === i.url.substr(0, "http:".length).toLowerCase() ? void 0 !== window.console && console.warn("Blocked mixed content image " + i.url) : (t.src = i.url, o.curSrcSupported || (t.currentSrc = t.src), o.backfaceVisibilityFix(t))), o.setIntrinsicSize(t, i))
            }, o.ascendingSort = function(e, t) {
                return e.resolution - t.resolution
            }, o.removeVideoShim = function(e) {
                var t = e.getElementsByTagName("video");
                if (t.length) {
                    for (var r = t[0], n = r.getElementsByTagName("source"); n.length;) e.insertBefore(n[0], r);
                    r.parentNode.removeChild(r)
                }
            }, o.getAllElements = function() {
                for (var e = [], r = t.getElementsByTagName("img"), n = 0, i = r.length; i > n; n++) {
                    var s = r[n];
                    ("PICTURE" === s.parentNode.nodeName.toUpperCase() || null !== s.getAttribute("srcset") || s[o.ns] && null !== s[o.ns].srcset) && e.push(s)
                }
                return e
            }, o.getMatch = function(e, t) {
                for (var r, n = t.childNodes, i = 0, s = n.length; s > i; i++) {
                    var a = n[i];
                    if (1 === a.nodeType) {
                        if (a === e) return r;
                        if ("SOURCE" === a.nodeName.toUpperCase()) {
                            null !== a.getAttribute("src") && void 0 !== typeof console && console.warn("The `src` attribute is invalid on `picture` `source` element; instead, use `srcset`.");
                            var c = a.getAttribute("media");
                            if (a.getAttribute("srcset") && (!c || o.matchesMedia(c))) {
                                var l = o.verifyTypeSupport(a);
                                if (l === !0) {
                                    r = a;
                                    break
                                }
                                if ("pending" === l) return !1
                            }
                        }
                    }
                }
                return r
            }, s(), i._ = o, n(i)
    }(window, window.document, new window.Image);
! function(t, i, s) {
    "use strict";
    var e, o = i.event;
    o.special.smartresize = {
        setup: function() {
            i(this).bind("resize", o.special.smartresize.handler)
        },
        teardown: function() {
            i(this).unbind("resize", o.special.smartresize.handler)
        },
        handler: function(t, s) {
            var o = this,
                n = arguments;
            t.type = "smartresize", e && clearTimeout(e), e = setTimeout(function() {
                i.event.dispatch.apply(o, n)
            }, "execAsap" === s ? 0 : 100)
        }
    }, i.fn.smartresize = function(t) {
        return t ? this.bind("smartresize", t) : this.trigger("smartresize", ["execAsap"])
    }, i.Mason = function(t, s) {
        this.element = i(s), this._create(t), this._init()
    }, i.Mason.settings = {
        isResizable: !0,
        isAnimated: !1,
        animationOptions: {
            queue: !1,
            duration: 500
        },
        gutterWidth: 0,
        isRTL: !1,
        isFitWidth: !1,
        containerStyle: {
            position: "relative"
        },
        trackGutterWidth: !1,
        gutterElement: null
    }, i.Mason.prototype = {
        _filterFindBricks: function(t) {
            var i = this.options.itemSelector;
            return i ? t.filter(i).add(t.find(i)) : t
        },
        _getBricks: function(t) {
            var i = this._filterFindBricks(t).css({
                position: "absolute"
            }).addClass("masonry-brick");
            return i
        },
        _create: function(s) {
            this.options = i.extend(!0, {}, i.Mason.settings, s), this.styleQueue = [];
            var e = this.element[0].style;
            this.originalStyle = {
                height: e.height || ""
            };
            var o = this.options.containerStyle;
            for (var n in o) this.originalStyle[n] = e[n] || "";
            this.element.css(o), this.horizontalDirection = this.options.isRTL ? "right" : "left", this.offset = {
                x: parseInt(this.element.css("padding-" + this.horizontalDirection), 10),
                y: parseInt(this.element.css("padding-top"), 10)
            }, this.isFluid = this.options.columnWidth && "function" == typeof this.options.columnWidth;
            var h = this;
            setTimeout(function() {
                h.element.addClass("masonry")
            }, 0), this.options.isResizable && i(t).bind("smartresize.masonry", function() {
                h.resize()
            }), this._updateGutterWidth(), this.reloadItems()
        },
        _init: function(t) {
            this._getColumns(), this._reLayout(t)
        },
        option: function(t, s) {
            i.isPlainObject(t) && (this.options = i.extend(!0, this.options, t))
        },
        layout: function(t, i) {
            for (var s = 0, e = t.length; e > s; s++) this._placeBrick(t[s]);
            var o = {};
            if (o.height = Math.max.apply(Math, this.colYs), this.options.isFitWidth) {
                var n = 0;
                for (s = this.cols; --s && 0 === this.colYs[s];) n++;
                o.width = (this.cols - n) * this.columnWidth - this.options.gutterWidth
            }
            this.styleQueue.push({
                $el: this.element,
                style: o
            });
            var h, r = this.isLaidOut && this.options.isAnimated ? "animate" : "css",
                a = this.options.animationOptions;
            for (s = 0, e = this.styleQueue.length; e > s; s++) h = this.styleQueue[s], h.$el[r](h.style, a);
            this.styleQueue = [], i && i.call(t), this.isLaidOut = !0
        },
        _getColumns: function() {
            var t = this.options.isFitWidth ? this.element.parent() : this.element,
                i = t.width();
            this.columnWidth = this.isFluid ? this.options.columnWidth(i) : this.options.columnWidth || this.$bricks.outerWidth(!0) || i, this.columnWidth += this.options.gutterWidth, this.cols = Math.floor((i + this.options.gutterWidth) / this.columnWidth), this.cols = Math.max(this.cols, 1)
        },
        _placeBrick: function(t) {
            var s, e, o, n, h, r = i(t);
            if (s = Math.ceil(r.outerWidth(!0) / this.columnWidth), s = Math.min(s, this.cols), 1 === s) o = this.colYs;
            else
                for (e = this.cols + 1 - s, o = [], h = 0; e > h; h++) n = this.colYs.slice(h, h + s), o[h] = Math.max.apply(Math, n);
            for (var a = Math.min.apply(Math, o), l = 0, c = 0, u = o.length; u > c; c++)
                if (o[c] === a) {
                    l = c;
                    break
                }
            var d = {
                top: a + this.offset.y
            };
            d[this.horizontalDirection] = this.columnWidth * l + this.offset.x, this.styleQueue.push({
                $el: r,
                style: d
            });
            var p = a + r.outerHeight(!0),
                m = this.cols + 1 - u;
            for (c = 0; m > c; c++) this.colYs[l + c] = p
        },
        resize: function() {
            this.options.trackGutterWidth && this._updateGutterWidth();
            var t = this.cols;
            this._getColumns(), (this.isFluid || this.cols !== t || this.options.trackGutterWidth) && this._reLayout()
        },
        _reLayout: function(t) {
            var i = this.cols;
            for (this.colYs = []; i--;) this.colYs.push(0);
            this.layout(this.$bricks, t)
        },
        reloadItems: function() {
            this.$bricks = this._getBricks(this.element.children())
        },
        reload: function(t) {
            this.reloadItems(), this._init(t)
        },
        appended: function(t, i, s) {
            if (i) {
                this._filterFindBricks(t).css({
                    top: this.element.height()
                });
                var e = this;
                setTimeout(function() {
                    e._appended(t, s)
                }, 1)
            } else this._appended(t, s)
        },
        _appended: function(t, i) {
            var s = this._getBricks(t);
            this.$bricks = this.$bricks.add(s), this.layout(s, i)
        },
        remove: function(t) {
            this.$bricks = this.$bricks.not(t), t.remove()
        },
        destroy: function() {
            this.$bricks.removeClass("masonry-brick").each(function() {
                this.style.position = "", this.style.top = "", this.style.left = ""
            });
            var s = this.element[0].style;
            for (var e in this.originalStyle) s[e] = this.originalStyle[e];
            this.element.unbind(".masonry").removeClass("masonry").removeData("masonry"), i(t).unbind(".masonry")
        },
        _updateGutterWidth: function() {
            this.options.gutterWidth = this.options.gutterElement ? i(this.options.gutterElement).width() - 1 : this.options.gutterWidth
        }
    };
    var n = function(i) {
        t.console && t.console.error(i)
    };
    i.fn.masonry = function(t) {
        if ("string" == typeof t) {
            var s = Array.prototype.slice.call(arguments, 1);
            this.each(function() {
                var e = i.data(this, "masonry");
                return e ? i.isFunction(e[t]) && "_" !== t.charAt(0) ? void e[t].apply(e, s) : void n("no such method '" + t + "' for masonry instance") : void n("cannot call methods on masonry prior to initialization; attempted to call method '" + t + "'")
            })
        } else this.each(function() {
            var s = i.data(this, "masonry");
            s ? (s.option(t || {}), s._init()) : i.data(this, "masonry", new i.Mason(t, this))
        });
        return this
    }
}(window, jQuery),
function(t, i, s) {
    i.extend(!0, i.Mason.settings, {
        layoutPriorities: {
            upperPosition: 1,
            shelfOrder: 1
        }
    }), i.Mason.prototype._placeBrick = function(t) {
        var e, o, n, h, r = i(t),
            a = this.horizontalDirection;
        if (e = Math.ceil(r.outerWidth(!0) / (this.columnWidth + this.options.gutterWidth)), e = Math.min(e, this.cols), 1 === e) n = this.colYs;
        else {
            o = this.cols + 1 - e, n = [];
            for (var l = 0; o > l; l++) h = this.colYs.slice(l, l + e), n[l] = Math.max.apply(Math, h)
        }
        var c = Math.min.apply(Math, n),
            u = {
                top: 0
            };
        u[a] = 0;
        var d = this.styleQueue.slice(-1)[0];
        if (d !== s) {
            var p = d.$el.outerWidth(!0),
                m = d.style[a] - this.offset.x;
            u[a] = m + p, u.top = d.style.top;
            var f = u[a] + e * this.columnWidth,
                y = this.cols * this.columnWidth;
            f > y && (u[a] = 0)
        }
        for (var v = this.options.layoutPriorities, g = [], W = 0, M = n.length; M > W; W++) {
            var _ = Math.abs(u[a] - this.columnWidth * W),
                b = Math.abs(u.top - n[W]),
                k = Math.pow(_, 2) + Math.pow(b, 2),
                z = Math.round(Math.sqrt(k)),
                x = v.shelfOrder * z,
                $ = v.upperPosition * (n[W] - c);
            g[W] = $ + x
        }
        var w, F = Math.min.apply(null, g);
        for (W = 0, M = g.length; M > W; W++)
            if (g[W] === F) {
                w = W;
                break
            }
        var Q = {
            top: n[w] + this.offset.y
        };
        Q[a] = this.columnWidth * w + this.offset.x, this.styleQueue.push({
            $el: r,
            style: Q
        });
        var Y = n[w] + r.outerHeight(!0),
            B = this.cols + 1 - M;
        for (W = 0; B > W; W++) this.colYs[w + W] = Y
    }
}(window, jQuery);
! function(n) {
    var o = function() {
        n.events.raiseTrackingEvent("Print", "Submit", [location.protocol, "//", location.host, location.pathname].join(""))
    };
    null === window.onbeforeprint ? window.onbeforeprint = o : window.matchMedia && window.matchMedia("print").addListener(function(n) {
        n.matches && o()
    })
}(window.CrunchSS = window.CrunchSS || {});
! function(e, t) {
    function a(e, a, o) {
        t(document).trigger("trackingEvent", {
            category: e,
            action: a,
            label: o
        }), window.ga("send", "event", e, a, o), s(e, a, o), l(e, a, o)
    }

    function o() {
        var e, o, c, l = t(this),
            m = l.data("event-category"),
            u = l.data("event-action"),
            s = l.data("event-label"),
            v = l.data("event-label-dynamic");
        if (!m) throw new Error("`data-event-category` attribute expected");
        if (!u) throw new Error("`data-event-action` attribute expected");
        if (!s && !v) throw new Error("`data-event-label` attribute expected");
        if (e = r[m], o = i[u], c = v || n[s], !e) throw new Error('"' + m + '" is not a recognized event category');
        if (!o) throw new Error('"' + u + '" is not a recognized event action');
        if (!c) throw new Error('"' + s + '" is not a recognized event label');
        a(e, o, c)
    }
    var r = {
            Contact: "Contact",
            Login: "Login",
            Callback: "Callback",
            Calculator: "Calculator",
            Demo: "Demo",
            Chat: "Chat",
            Social: "Social",
            Partner: "Partner",
            ProspectNewsletter: "ProspectNewsletter",
            Job: "Job",
            Resource: "Resource"
        },
        i = {
            Select: "Select",
            Submit: "Submit",
            TryAgain: "TryAgain",
            FormSuccess: "FormSuccess",
            FormFailed: "FormFailed",
            Click: "Click",
            ClickMailTo: "ClickMailTo",
            ClickChooseTime: "ClickChooseTime",
            ClickBusinessLimited: "ClickBusinessLimited",
            ClickBusinessSoleTrader: "ClickBusinessSoleTrader",
            ClickBusinessUmbrella: "ClickBusinessUmbrella",
            ClickCallbackLink: "ClickCallbackLink",
            ClickIncomePerHour: "ClickIncomePerHour",
            ClickIncomePerDay: "ClickIncomePerDay",
            ClickIncomePerYear: "ClickIncomePerYear",
            Recalculate: "Recalculate",
            Open: "Open",
            Minimise: "Minimise",
            ChatNow: "ChatNow",
            Share: "Share"
        },
        n = {
            BlogPost: "BlogPost",
            FormSwitcher: "FormSwitcher",
            Header: "Header",
            HomepageFooter: "HomepageFooter",
            SignupPage: "SignupPage",
            HomepageHero: "HomepageHero",
            Modal: "Modal",
            BlogWidget: "BlogWidget",
            Timer: "Timer",
            Manual: "Manual",
            ChatOffline: "ChatOffline",
            Twitter: "Twitter",
            Facebook: "Facebook",
            LinkedIn: "LinkedIn",
            GooglePlus: "G+",
            Blog: "Blog",
            YouTube: "YouTube",
            Search: "Search",
            Sector: "Sector",
            Location: "Location",
            StandardPrice: "StandardPrice",
            PlusPrice: "PlusPrice",
            PremiumPrice: "PremiumPrice",
            Uber: "Uber",
            Tradepoint: "Tradepoint",
            Partner: "Partner",
            ZeroPrice: "ZeroPrice",
            IR35Calculator: "IR35Calculator",
            DemoLogin: "DemoLogin",
            VideoSlider: "VideoSlider"
        },
        c = function(e) {
            return e.length < 2 ? "0" + e : e
        },
        l = function(e, t, a) {
            t === i.FormSuccess && (e === r.Demo ? window._fbq.push(["track", "6035516396974", {
                value: "0.01",
                currency: "GBP"
            }]) : "Business expenses guide" === a && window._fbq.push(["track", "6035516868774", {
                value: "0.01",
                currency: "GBP"
            }]))
        },
        m = function() {
            return (new Date).getTime().toString(36) + Math.random().toString(36).slice(2)
        },
        u = function(e, t, a, o) {
            return t !== i.FormSuccess ? null : e === r.Callback ? {
                emv_value: 10,
                emv_text1: r.Callback
            } : e === r.Contact ? a === n.IR35Calculator ? {
                emv_value: 10,
                emv_text1: "IR35 contact"
            } : a === n.Partner ? {
                emv_value: 0,
                emv_text1: "Partner contact"
            } : {
                emv_value: 10,
                emv_text1: r.Contact
            } : e === r.Demo ? {
                emv_value: 10,
                emv_text1: r.Demo
            } : e === r.Resource ? -1 !== o.indexOf("guides") || -1 !== o.indexOf("ir35-calculator") ? {
                emv_value: 1,
                emv_text1: "Guide"
            } : -1 !== o.indexOf("invoice-templates") ? {
                emv_value: 1,
                emv_text1: "Invoice template"
            } : {
                emv_value: 10,
                emv_text1: r.Contact
            } : null
        },
        s = function(e, a, o) {
            var r = u(e, a, o, location.pathname);
            if (r) {
                var n = new Date,
                    l = [n.getDate(), n.getMonth() + 1, n.getFullYear()].map(String).map(c).join("-");
                t("<img />", {
                    src: ["https://p7tre.emv3.com/P?", "emv_client_id=1101068541&", "emv_conversionflag=Y&", "emv_pagename=" + [location.protocol, "//", location.host, location.pathname].join("") + "&", "emv_value=" + r.emv_value + "&", "emv_currency=GBP&", "emv_transid=" + m() + "&", "emv_text1=" + r.emv_text1 + "&", "emv_text2=" + i.FormSuccess + "&", "emv_date1=" + l + "&", "emv_random=" + m()].join(""),
                    alt: ""
                }).css("border", "0").css("position", "fixed").css("z-index", "-999999").prependTo(t("body"))
            }
        };
    t(document).on("click", ".track-click", o), e.events = {
        actions: i,
        categories: r,
        getSmartFocusEventValueAndLabel: u,
        handleDeclarativeEvent: o,
        labels: n,
        raiseTrackingEvent: a
    }
}(window.CrunchSS = window.CrunchSS || {}, jQuery);

function create_cookie(e, o, t, n) {
    var r = new Date;
    r.setTime(r.getTime() + 24 * t * 60 * 60 * 1e3);
    var i = r.toUTCString();
    document.cookie = e + "=" + o + ";expires=" + i + ";path=" + n + ";"
}

function read_cookie(e) {
    for (var o = "", t = "", n = e + "=", r = document.cookie.split(";"), i = r.length, c = 0; i > c; c++)
        if (t = jQuery.trim(r[c]), 0 === t.indexOf(n)) {
            o = t.substring(n.length, t.length);
            break
        }
    return o
}

function delete_cookie(e) {
    document.cookie = e + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/"
}! function(e, o) {
    "use strict";

    function t(e) {
        var o, t, n, r = {},
            i = e.substring(1).split("&");
        for (o = 0, t = i.length; t > o; o++) n = i[o].split("="), n[0].length > 0 && (r[n[0]] = decodeURIComponent(n[1]));
        return r
    }

    function n() {
        var e, o, t = [/\.yahoo\.co/i, /\.google\./i, /\.live\.com/i, /search\.msn\./i, /\.aol\./i, /\.bing\.com/i, /\.ask\.com/i];
        for (e = 0, o = t.length; o > e; e++)
            if (t[e].test(document.referrer)) return !0;
        return !1
    }

    function r(e) {
        return e.hasOwnProperty("key") && e.hasOwnProperty("cre") && e.hasOwnProperty("cam") && e.hasOwnProperty("adg") ? "PPC" : n() ? "Organic" : document.referrer ? "Referral" : "Direct"
    }

    function i(e) {
        function o(e) {
            return 10 > e ? "0" + e : e
        }
        return e.toISOString ? e.toISOString() : e.getUTCFullYear() + "-" + o(e.getUTCMonth() + 1) + "-" + o(e.getUTCDate()) + "T" + o(e.getUTCHours()) + ":" + o(e.getUTCMinutes()) + ":" + o(e.getUTCSeconds()) + "." + (e.getUTCMilliseconds() / 1e3).toFixed(3).slice(2, 5) + "Z"
    }

    function c() {
        var e = read_cookie("referrerCookie");
        return e.length < 1 ? null : JSON.parse(decodeURIComponent(e))
    }

    function a(e) {
        create_cookie("referrerCookie", encodeURIComponent(JSON.stringify(e)), 365, "/")
    }

    function l() {
        return i(new Date)
    }

    function u(e, o) {
        e[o] || (e[o] = l())
    }

    function s(e, o, t) {
        e[o] || (e[o] = t)
    }

    function f(e) {
        var o = /^[^\/]*\/\/([^\/]*)/.exec(e);
        return o && 2 === o.length ? o[1] : void 0
    }

    function d(o, t) {
        var n = e.cookies.getReferrerCookie();
        s(n, o, t), a(n)
    }
    var g, m = t(window.location.search),
        p = r(m),
        C = "PPC" === p ? 1 : 0,
        k = m.cam,
        v = window.CrunchSS.events.categories,
        h = window.CrunchSS.events.labels,
        w = window.CrunchSS.events.actions;
    window.JSON && JSON.parse && JSON.stringify && (e.cookies = {
        getReferrerCookie: c,
        saveGenericField: d
    }, g = e.cookies.getReferrerCookie(), g ? g.ppcCount = g.ppcCount + C : g = {}, s(g, "channel", p), s(g, "landingPage", window.location.toString()), s(g, "landingTimestamp", l()), s(g, "referringUrl", document.referrer), s(g, "memberOfRemarketing", "69455557" === k), s(g, "ppcCount", C), s(g, "ppcCampaign", k), s(g, "ppcKeyword", m.key), s(g, "ppcAdGroup", m.adg), s(g, "ppcPlacement", m.pla), s(g, "ppcCreativeNumber", m.cre), s(g, "affiliate", m.aid), g.adNetwork || "PPC" !== p || (g.adNetwork = f(document.referrer)), a(g), e.cookies.handleTrackingEvent = function(o, t) {
        var n = e.cookies.getReferrerCookie();
        if (n) {
            if (t.action === w.ChatNow && t.category === v.Chat) u(n, "eventChat");
            else if (t.action === w.Submit) {
                if (t.category === v.Contact) t.label === h.Partner ? u(n, "eventProfessionalServices") : t.label === h.IR35Calculator ? u(n, "eventIR35Calculator") : u(n, "eventContactRequest");
                else if (t.category === v.Callback) t.label === h.Uber ? u(n, "eventUber") : u(n, "eventCallbackRequest");
                else if (t.category === v.Demo) u(n, "eventDemo");
                else if (t.category === v.Resource) {
                    var r = document.location.pathname;
                    /\/guides\//.test(r) ? u(n, "eventGuide") : /\/invoice-templates\//.test(r) && u(n, "eventInvoiceTemplate")
                }
            } else t.action === w.ClickMailTo && u(n, "eventMailto");
            a(n)
        }
    }, e.cookies.handleFormEvent = function(o, n) {
        var r = e.cookies.getReferrerCookie(),
            i = t(n);
        s(r, "formId", i.formId), s(r, "formUrl", i.formUrl), s(r, "productLabel", i.productLabel), a(r)
    }, o(document).on("trackingEvent", e.cookies.handleTrackingEvent), o(document).on("success", e.cookies.handleFormEvent))
}(window.CrunchSS = window.CrunchSS || {}, jQuery);
! function(e) {
    e.poll = function(e, t, n, i, r) {
        function u(e, t, r) {
            return r && setTimeout(function() {
                o.reject({
                    timeout: !0,
                    message: "Timed out after " + r + "ms"
                }), s = !0
            }, r), e().then(function() {
                var r;
                if (!s) return t.apply(this, arguments) ? void o.resolve.apply(this, arguments) : (r = n.apply(this, arguments)) ? void o.reject(r) : void setTimeout(function() {
                    u(e, t)
                }, i)
            }).fail(function() {
                o.reject.apply(o, arguments)
            }), o.promise()
        }
        var o = $.Deferred(),
            s = !1;
        return u(e, t, r)
    }
}(window.CrunchSS = window.CrunchSS || {});
! function(e, r) {
    function a(e) {
        e += "";
        for (var r = e.split("."), a = r[0], s = r.length > 1 ? "." + r[1] : "", o = /(\d+)(\d{3})/; o.test(a);) a = a.replace(o, "$1,$2");
        return a + s
    }

    function s(e, a) {
        r.each(a, function() {
            var r = e.find("input[name=" + this.field + "], textarea[name=" + this.field + "]");
            r.addClass("error").next(".error-message").html(this.message)
        })
    }

    function o(e) {
        return "" !== e && jQuery.isNumeric(e) ? (e = e.replace("£", "").replace("&pound;", "").replace(/,/g, ""), e = parseFloat(e).toFixed(2), e = e.replace(".00", ""), e = a(e), e = "£" + e) : e
    }

    function t(e) {
        e.val(o(e.val()))
    }

    function l(e) {
        return e.replace("£", "").replace("&pound;", "").replace(/,/g, "")
    }

    function n(e) {
        e.val(l(e.val()))
    }

    function c(e) {
        var a = e.val();
        "" === a && (a = e.attr("placeholder"));
        var s = e.attr("data-hiddenField"),
            o = r("#" + s);
        o.val(a), n(o)
    }

    function i(e) {
        return e = parseFloat(e).toFixed(2), e = e.replace(".00", "")
    }

    function u(e, a, s) {
        var o = e,
            t = r("#calculatorOutput");
        t.find("section").stop(!0, !0), t.find(".active").fadeOut(100, function() {
            r(this).removeClass("active"), a && r("#calculatorOutput " + o + " h2").html(a), s && r("#calculatorOutput " + o + " .resultsText").html(s), r("#calculatorOutput " + o).fadeIn(300).addClass("active")
        })
    }

    function d(e) {
        var a, o = e.attr("data-hiddenField");
        if ("undefined" != typeof o ? a = r("#" + o).val() : (a = e.val(), "" === a && (a = e.attr("placeholder"))), !jQuery.isNumeric(a)) {
            s(r(".calculator-form"), [{
                field: e.attr("id"),
                message: "Please only include numbers"
            }]);
            var t;
            return "cIncome" === e.attr("id") ? t = "income" : "cFees" === e.attr("id") ? t = "fees" : "cExpenses" === e.attr("id") ? t = "expenses" : "cWeeks" === e.attr("id") && (t = "weeks"), e.closest(".field").find(".error-message").addClass(t), !1
        }
        return !0
    }

    function m(e, r, a) {
        return "perYear" === r && e > M || "perDay" === r && 5 * e * a > M ? W.IncomeValidationResult.IncomeOverMax : "perYear" === r && P > e || "perDay" === r && P > 5 * e * a ? W.IncomeValidationResult.IncomeLessThanMinimum : W.IncomeValidationResult.Success
    }

    function f(e) {
        var o;
        e === W.IncomeValidationResult.IncomeOverMax ? (o = "Total <b>annual</b> income cannot exceed &pound;" + a(M), r(".calculator-form.newError .error-message.income").empty().removeClass("income"), s(r(".calculator-form"), [{
            field: "cIncome",
            message: o
        }, {
            field: "cWeeks",
            message: o
        }]), r("#cIncome").addClass("income").closest(".field").find(".error-message").addClass("income"), r("#cWeeks").addClass("error income").closest(".field").find(".error-message").addClass("income")) : e === W.IncomeValidationResult.IncomeLessThanMinimum && (o = "Total <b>annual</b> income cannot be less than &pound;" + a(P), r(".calculator-form.newError .error-message.income").empty().removeClass("income"), s(r(".calculator-form"), [{
            field: "cIncome",
            message: o
        }, {
            field: "cWeeks",
            message: o
        }]), r("#cIncome").addClass("income").closest(".field").find(".error-message").addClass("income"), r("#cWeeks").addClass("error income").closest(".field").find(".error-message").addClass("income"))
    }

    function v(e, a, s) {
        "" === a && (a = r(".dd-selected-value").val()), "" === s && (s = r("input#cWeeks").attr("placeholder"));
        var o = m(e, a, Number(s));
        return f(o), o === W.IncomeValidationResult.Success
    }

    function p(e) {
        var s = r("#cIncome"),
            o = r("#cWeeks");
        "perYear" === e ? (r("#calculatorInput").find(".field.weeks").hide(), s.attr("placeholder", "£" + a(H)), t(s), c(s)) : (r("#calculatorInput").find(".field.weeks").show(), s.attr("placeholder", "£" + a(F)), t(s), c(s)), r(".calculator-form.newError .error-message.weeks").empty().removeClass("weeks"), o.removeClass("error"), r(".calculator-form.newError .error-message.income").empty().removeClass("income"), s.removeClass("error"), v(r("#cIncomeHidden").val(), r("#cIncomeDropdown").val(), o.val())
    }

    function h(e) {
        return !(Y > e || e > B)
    }

    function g() {
        r(".calculator-form.newError .error-message.fees").empty().removeClass("fees"), s(r(".calculator-form"), [{
            field: "cFees",
            message: "Total fees should not exceed &pound;" + a(B)
        }]), r("#cFeesHidden").closest(".field").find(".error-message").addClass("fees")
    }

    function C() {
        r(".calculator-form.newError .error-message.expenses").empty().removeClass("expenses"), s(r(".calculator-form"), [{
            field: "cExpenses",
            message: "Total annual expenses cannot exceed &pound;" + a(L)
        }]), r("#cExpensesHidden").closest(".field").find(".error-message").addClass("expenses")
    }

    function w(e) {
        return h(e) ? !0 : (g(), !1)
    }

    function I(e) {
        return !(N > e || e > L)
    }

    function k(e) {
        return I(e) ? !0 : (C(), !1)
    }

    function y(e, r) {
        return !("perDay" === e && (Q > r || r > q))
    }

    function b() {
        r(".calculator-form.newError .error-message.weeks").empty().removeClass("weeks"), s(r(".calculator-form"), [{
            field: "cWeeks",
            message: "Sorry, you must work between " + a(Q) + " and " + a(q) + " weeks"
        }]), r("#cWeeks").closest(".field").find(".error-message").addClass("weeks")
    }

    function x(e, a) {
        return "" === e && (e = r(".dd-selected-value").val()), "" === a && (a = r("input#cWeeks").attr("placeholder")), y(e, Number(a)) ? !0 : (b(), !1)
    }

    function E(e, a, s, o, t, l, n) {
        var c, i = r.Deferred();
        if (c = m(a, s, l), c !== W.IncomeValidationResult.Success) return i.reject({
            result: W.CalculationResult.IncomeIsInvalid,
            validationResult: c
        }), i.promise();
        if (c = h(o), !c) return i.reject({
            result: W.CalculationResult.FeesAreInvalid,
            validationResult: c
        }), i.promise();
        if (c = I(t), !c) return i.reject({
            result: W.CalculationResult.ExpensesAreInvalid,
            validationResult: c
        }), i.promise();
        if (c = y(s, l), !c) return i.reject({
            result: W.CalculationResult.WeeksAreInvalid,
            validationResult: c
        }), i.promise();
        var u = "companyType=" + e + "&incomePeriod=" + s + "&income=" + a + "&expenses=" + t + "&fees=" + o;
        return "perDay" === s && (u = u + "&workingWeeks=" + l), r.getJSON("https://calculator-elb.crunch.co.uk/calculator?" + u, n).done(function(e) {
            i.resolve({
                result: W.CalculationResult.Success,
                data: e
            })
        }).fail(function(e) {
            var r = e.responseJSON;
            i.reject({
                result: W.CalculationResult.RequestError,
                data: r
            })
        }), i.promise()
    }

    function R(o) {
        var t = !0,
            l = r("#cBusinessTypeHidden").val(),
            n = r("#cIncomeHidden").val(),
            c = r("#cIncomeDropdown").val(),
            d = r("#cFeesHidden").val(),
            m = r("#cExpensesHidden").val(),
            v = r("#cWeeks").val(),
            p = e.events,
            h = r(".validate"),
            w = r("#calculator-results");
        "" === c && (c = r(".dd-selected-value").val()), "" === c && (c = S), "" === v && (v = r("input#cWeeks").attr("placeholder")), h.each(function() {
            var e, a = r(this).attr("data-hiddenField");
            "undefined" != typeof a ? e = r("#" + a).val() : (e = r(this).val(), "" === e && (e = r(this).attr("placeholder"))), jQuery.isNumeric(e) || (t = !1, s(r(".calculator-form"), [{
                field: r(this).attr("id"),
                message: "Please only include numbers"
            }]))
        }), t && (o || h.each(function() {
            "" === r(this).val() && r(this).val(r(this).attr("placeholder"))
        }), E(l, n, c, d, m, v, function() {
            u("#calculator-results-other", J, ae)
        }).done(function(e) {
            var s = e.data,
                t = i(s.profit),
                l = i(s.savings),
                n = i(s.takeHome);
            if (o || (r("#calculateButton").val("Re-calculate"), p.raiseTrackingEvent(p.categories.Calculator, p.actions.FormSuccess, p.labels.FormSwitcher)), r(".error-message").empty(), s.profit > V) u("#calculator-results-other", _, X);
            else if (0 >= l && A > t) u("#calculator-results-other", $, K);
            else if (0 >= l && t >= A) u("#calculator-results-other", z, Z);
            else {
                var c = w.find("#takehome .value");
                c.text(a(n)), n !== Math.floor(n) && n > 9999 && c.closest(".numbers").addClass("smallValue"), w.find("#savings .value").text(a(l)), u("#calculator-results")
            }
        }).fail(function(e) {
            e.result !== W.CalculationResult.Success && (p.raiseTrackingEvent(p.categories.Calculator, p.actions.FormFailed, p.labels.FormSwitcher), e.result === W.CalculationResult.IncomeIsInvalid ? f(e.validationResult) : e.result === W.CalculationResult.FeesAreInvalid ? g() : e.result === W.CalculationResult.ExpensesAreInvalid ? C() : e.result === W.CalculationResult.WeeksAreInvalid ? b() : e.result === W.CalculationResult.RequestError && u("#calculator-results-other", G, re))
        }))
    }
    var W = {
            IncomeValidationResult: {
                Success: 0,
                IncomeOverMax: -1,
                IncomeLessThanMinimum: -2
            },
            CalculationResult: {
                Success: 0,
                IncomeIsInvalid: -1,
                FeesAreInvalid: -2,
                ExpensesAreInvalid: -3,
                WeeksAreInvalid: -4,
                RequestError: -5
            }
        },
        F = 350,
        H = 7e4,
        S = "perDay",
        T = 1500,
        D = 6e3,
        O = 40,
        j = "2015/16",
        A = 3e4,
        V = 3e5,
        P = 12e3,
        M = 16e5,
        N = 0,
        L = 13e5,
        Y = 0,
        B = 5e3,
        Q = 1,
        q = 52,
        J = "Calculating...",
        $ = "Hmm, it seems we need<br/> a little more info.",
        _ = "Wow - look at all<br/>that profit!",
        z = "Looks like business is booming!",
        U = "How we calculate the figures",
        G = "Oops, something went wrong",
        K = "Let's talk about your options. Our team<br/>of experts is available 8:30am-8:30pm",
        X = "We're not right for businesses with profits of over £" + a(V) + ". However we recommend <a href='http://www.srctaxation.co.uk/' target='_blank'>SRC Chartered Accountants</a>.",
        Z = "Things are going great for you - if you want to free up some of your time to grow further, <a href='/pricing/'>we can help with bookkeeping</a>.",
        ee = "This is an estimated figure based on current tax thresholds for " + j + " and the information provided. Savings include your Crunch subscription.",
        re = "It seems the ones and zeros aren't adding up somewhere. Please try again or, if you'd prefer, how about we just talk the old fashioned way? Contact details below.",
        ae = "<img src='/wp-content/themes/crunch2015/library/images/crunch-loader.gif' class='loadingImg' alt='loading...' />";
    e.calculator = {
        addCommas: a,
        formatCurrency: o,
        formatOutput: i,
        removeCurrency: l,
        validateWeeks: y,
        validateIncome: m,
        validateFees: h,
        validateExpenses: I,
        calculate: E,
        types: W
    }, r(document).ready(function(r) {
        var s = r("#cIncome"),
            o = r("#cFees"),
            l = r("#cExpenses"),
            i = r("#cWeeks"),
            m = r("#THP_calculator");
        if (r("#calculatorInput").length) {
            var f = r(".calculator-form .currency"),
                h = r("#cIncomeDropdown");
            s.attr("placeholder", "£" + a(F)), o.attr("placeholder", "£" + a(T)), l.attr("placeholder", "£" + a(D)), i.attr("placeholder", O), f.each(function() {
                t(r(this)), c(r(this))
            }), h.selectBoxIt({
                autoWidth: !1,
                copyClasses: "container"
            }), h.on("change", function() {
                var e, a = r(this).val(),
                    s = window.CrunchSS.events;
                if ("perYear" === a) e = s.actions.ClickIncomePerYear;
                else {
                    if ("perDay" !== a) throw new Error('Unrecognised dropdown option "' + a + '".');
                    e = s.actions.ClickIncomePerDay
                }
                s.raiseTrackingEvent(s.categories.Calculator, e, s.labels.FormSwitcher), p(h.val())
            }), r("body").on("keypress", ".numericOnly", function(e) {
                return 8 !== e.which && 0 !== e.which && (e.which < 48 || e.which > 57) ? !1 : void 0
            }), f.focus(function() {
                var e = r(this).caret();
                n(r(this)), r(this).caret(e)
            }), f.change(function() {
                t(r(this)), c(r(this))
            }), f.blur(function() {
                t(r(this)), c(r(this))
            }), r(".businessType a").click(function(e) {
                e.preventDefault();
                var a = r(this).data("type");
                r("input#cBusinessTypeHidden").val(a), r(".businessType a").removeClass("current"), r(this).addClass("current")
            }), R(!0), r("#calcForm").submit(function(a) {
                a.preventDefault(), a.stopImmediatePropagation(), R(a), e.scrollCheck(r(this).find(".error").first())
            }), r("#expanation").find("a").click(function(e) {
                e.preventDefault(), u("#calculator-results-other", U, ee)
            }), s.change(function() {
                i.hasClass("income") && i.removeClass("income").removeClass("error"), r(".calculator-form.newError .error-message.income").empty().removeClass("income"), r(this).removeClass("error"), d(r(this)), d(r("#cWeeks")), v(r("#cIncomeHidden").val(), h.val(), r("#cWeeks").val())
            }), s.blur(function() {
                i.hasClass("income") && i.removeClass("income").removeClass("error"), r(".calculator-form.newError .error-message.income").empty().removeClass("income"), r(this).removeClass("error"), d(r(this)), d(r("#cWeeks")), v(r("#cIncomeHidden").val(), h.val(), r("#cWeeks").val())
            }), o.change(function() {
                r(".calculator-form.newError .error-message.fees").empty().removeClass("fees"), r(this).removeClass("error"), d(r(this)), w(r("#cFeesHidden").val())
            }), o.blur(function() {
                r(".calculator-form.newError .error-message.fees").empty().removeClass("fees"), r(this).removeClass("error"), d(r(this)), w(r("#cFeesHidden").val())
            }), l.change(function() {
                r(".calculator-form.newError .error-message.expenses").empty().removeClass("expenses"), r(this).removeClass("error"), d(r(this)), k(r("#cExpensesHidden").val())
            }), l.blur(function() {
                r(".calculator-form.newError .error-message.expenses").empty().removeClass("expenses"), r(this).removeClass("error"), d(r(this)), k(r("#cExpensesHidden").val())
            }), i.change(function() {
                var e = r("#cWeeks");
                r(".calculator-form.newError .error-message.weeks").empty().removeClass("weeks"), r(this).removeClass("error"), r(".calculator-form.newError .error-message.income").empty().removeClass("income"), s.removeClass("error"), d(r(this)), d(r("#cIncome")), x(h.val(), e.val()), v(r("#cIncomeHidden").val(), h.val(), e.val())
            }), i.blur(function() {
                var e = r("#cWeeks");
                r(".calculator-form.newError .error-message.weeks").empty().removeClass("weeks"), r(this).removeClass("error"), r(".calculator-form.newError .error-message.income").empty().removeClass("income"), s.removeClass("error"), d(r(this)), d(r("#cIncome")), x(h.val(), e.val()), v(r("#cIncomeHidden").val(), h.val(), e.val())
            }), [].slice.call(m.find(".tab-switcher")).forEach(function(e) {
                e.onclick = function() {
                    if (!(m.offset().top >= r(window).scrollTop())) {
                        var e = m.offset().top - r(".header").outerHeight() + parseInt(m.css("padding-top"));
                        r("html, body").animate({
                            scrollTop: e
                        })
                    }
                }
            })
        }
        s.placeholder(), o.placeholder(), l.placeholder(), i.placeholder()
    })
}(window.CrunchSS = window.CrunchSS || {}, jQuery);
! function(e, t) {
    function o(e) {
        if (e.length) {
            var o, i, l, h, n = t(window),
                r = n.scrollTop(),
                s = r + n.height(),
                a = 40,
                c = 40,
                g = 0,
                u = 0,
                w = 0,
                d = 0,
                p = 0;
            e.parents(".modal-shell").length ? (w = e.position().top, d = w + e.height(), o = t(".modal-shell"), p = o.scrollTop(), w += p) : (w = e.offset().top, d = w + e.height(), o = t("html, body"), p = r, t("header.header").length && (g += t("header.header").outerHeight(!0)), t("#wpadminbar").length && (g += t("#wpadminbar").outerHeight(!0)), t(".js-sticky-row.sticky-active").length && (a = 0, g += t(".js-sticky-row").outerHeight(!0)), e.hasClass("active-container") && (a = 0, c = 0, i = e.prevAll(".row:visible:first"), l = e.next(".js-drawer-open"), h = e.nextAll(".row:visible:first"), i.length || (i = e), h.length || (h = e.parents("li").next(), h.length || (h = e)), g += i.outerHeight(!0), d += l.outerHeight(!0) + h.outerHeight(!0)), t(".mobile-footer").length && (u += t(".mobile-footer").outerHeight(!0))), w -= g + a, d += u + c, (p > w || d >= s) && (d >= s && w >= p + (d - s) && (w = p + (d - s)), o.on("mousedown wheel DOMMouseScroll mousewheel touchmove", function() {
                o.stop()
            }), o.animate({
                scrollTop: w
            }, 700, function() {
                e.focus()
            }))
        }
    }
    e.scrollCheck = o
}(window.CrunchSS = window.CrunchSS || {}, jQuery);
! function(r, e) {
    r.AjaxFormWrapper = function(r) {
        this.$form = r, this.$submitButtons = r.find("input[type=submit],button[type=submit]"), this.fieldErrors = [], this.errorMessage = null, this.trackingCategory = null, this.trackingLabel = null, this.isRetry = !1
    }, r.AjaxFormWrapper.prototype.validateAndSubmit = function() {
        var r = this;
        return this.fetchFormProperties(), this.formEvent(this.isRetry ? "form-try-again" : "form-submit"), this.resetErrors(), this.disableSubmit(), this.validate(), this.hasErrors() ? (this.isRetry = !0, this.handleErrors(), void this.enableSubmit()) : void this.submitForm().always(function(e) {
            r.hasErrors() ? r.handleErrors() : r.handleSuccess(e)
        })
    }, r.AjaxFormWrapper.prototype.urlEncodeFormProperties = function(r, t) {
        var i, s = window.location.href;
        return "Resource" === t.eventCategory && t.eventLabel && (s += "#" + t.eventLabel.replace(/[^A-Z,0-9]/gi, "-")), i = r.serialize(), i += "&formUrl=" + encodeURIComponent(s), i += "&productLabel=" + encodeURIComponent(e("#product_label").val() || "general")
    }, r.AjaxFormWrapper.prototype.fetchFormProperties = function() {
        var e = this.$form.data();
        this.formValues = r.AjaxFormWrapper.prototype.urlEncodeFormProperties(this.$form, e), this.targetUrl = this.$form.attr("action"), this.successRedirect = e.successRedirect || null, this.trackingCategory = e.eventCategory || null, this.trackingLabel = e.eventLabel || null
    }, r.AjaxFormWrapper.prototype.handleErrors = function() {
        var r = this;
        r.enableSubmit(), this.formEvent("fail"), e.each(this.fieldErrors, function() {
            var e = r.$form.find("input[name=" + this.field + "], textarea[name=" + this.field + "]");
            e.addClass("error").next(".error-message").html(this.message)
        }), this.errorMessage && r.$form.find(".general-error.error-message").text(this.errorMessage).show(), CrunchSS.scrollCheck(this.$form.find(".error").first())
    }, r.AjaxFormWrapper.prototype.formEvent = function(e) {
        if (this.$form.trigger(e, this.formValues), null !== this.trackingCategory) {
            var t = "";
            "success" === e ? t = "FormSuccess" : "fail" === e ? t = "FormFailed" : "form-submit" === e ? t = "Submit" : "form-try-again" === e && (t = "TryAgain"), "undefined" != typeof r.events && "undefined" != typeof r.events.raiseTrackingEvent && r.events.raiseTrackingEvent(this.trackingCategory, t, this.trackingLabel)
        }
    }, r.AjaxFormWrapper.prototype.handleSuccess = function() {
        this.enableSubmit(), this.$form.find(".form-content").hide();
        var r, e = this.$form.find("#shall-we-call-yes:checked");
        r = this.$form.find(e.length > 0 && "1" === e.val() ? ".success-message-cbr" : ".success-message"), r.show(), CrunchSS.scrollCheck(r), this.formEvent("success"), this.successRedirect && (window.location.href = this.successRedirect)
    }, r.AjaxFormWrapper.prototype.resetErrors = function() {
        this.fieldErrors.length = 0, this.$form.find(".error-message").empty(), this.$form.find(".error").removeClass("error"), this.errorMessage = null
    }, r.AjaxFormWrapper.prototype.disableSubmit = function() {
        this.$submitButtons.addClass("disabled").prop("disabled", !0)
    }, r.AjaxFormWrapper.prototype.enableSubmit = function() {
        this.$submitButtons.removeClass("disabled").prop("disabled", !1)
    }, r.AjaxFormWrapper.prototype.validate = function() {
        var r = this;
        this.$form.find("input[minlength]").each(function() {
            if (e(this).val().length < e(this).attr("minlength")) {
                var t = e(this).attr("name");
                r.addFieldError(t, "Please enter more than " + e(this).attr("minlength") + " characters")
            }
        }), this.$form.find(".required").each(function() {
            if ("" === e.trim(e(this).val())) {
                var t = e(this).attr("name");
                r.addFieldError(t, "Please fill out this field")
            }
        });
        var t = this.$form.find("input[name=email], input[type=email]");
        if (t.length && t.val().length && !/(.+)@(.+){2,}\.(.+){2,}/.test(t.val())) {
            var i = t.attr("name");
            this.addFieldError(i, "Please enter a valid email address")
        }
        this.$form.find("input[name=phone], input[type=tel]").each(function() {
            if (e(this).val().length && !/\d/.test(r.$form.find("input[name=phone], input[type=tel]").val())) {
                var t = e(this).attr("name");
                r.addFieldError(t, "Please enter a valid phone number")
            }
        })
    }, r.AjaxFormWrapper.prototype.hasErrors = function() {
        return this.fieldErrors.length > 0 || this.errorMessage
    }, r.AjaxFormWrapper.prototype.addFieldError = function(r, e) {
        this.fieldErrors.push({
            field: r,
            message: e
        })
    }, r.AjaxFormWrapper.prototype.isOnline = function() {
        return navigator.onLine !== !1
    }, r.AjaxFormWrapper.prototype.submitForm = function() {
        var r = this;
        return this.isOnline() ? e.ajax({
            type: "post",
            dataType: "json",
            data: this.formValues,
            url: this.targetUrl
        }).fail(function(t) {
            var i;
            try {
                i = JSON.parse(t.responseText)
            } catch (s) {
                r.errorMessage = "Sorry, there was an error"
            }
            i && (i.message && (r.errorMessage = i.message), e.each(i.fieldErrors, function(e, t) {
                r.addFieldError(t.fieldName, t.fieldResponse)
            })), r.hasErrors() || (r.errorMessage = "Sorry, there was an error"), r.isRetry = !0
        }) : (r.errorMessage = "Oops! You're not connected to the internet", e.Deferred().reject().promise())
    }, e(document).ready(function() {
        e("form.contact-form").each(function() {
            var t = e(this),
                i = new r.AjaxFormWrapper(t);
            t.on("submit", function(r) {
                r.preventDefault(), i.validateAndSubmit()
            })
        })
    })
}(window.CrunchSS = window.CrunchSS || {}, jQuery);
! function(e, t) {
    "use strict";

    function s(s) {
        e.AjaxFormWrapper.call(this, s), this.$button = s.find("#demoSubmit"), this.$buttonText = this.$button.find("span"), this.$progressBar = this.$button.find(".progress-bar"), this.$formInner = t("#demoFormBoxInner"), this.$successBox = t("#demoFormBoxSuccess"), this.$successButton = t("#demoFormBoxSuccessButton"), this.$currentUserBox = t("#demoFormCurrentUser"), this.$errorMessage = this.$formInner.find(".general-error"), this.$successImage1 = t("#demoFormBoxSuccessImg1"), this.$successImage2 = t("#demoFormBoxSuccessImg2"), this.buttonTexts = ["Crunching...", "Doing the numbers...", "Installing humans..."], this.progressWidths = ["10%", "33%", "75%"], this.pendingTimeouts = []
    }
    var o = function(e, t) {
        function s() {
            this.constructor = e
        }
        s.prototype = t.prototype, e.prototype = new s
    };
    o(s, e.AjaxFormWrapper), s.prototype.clearAllTimeouts = function() {
        t.each(this.pendingTimeouts, function(e, t) {
            clearTimeout(t)
        })
    }, s.prototype.enableSubmit = function() {
        this.clearAllTimeouts(), this.$button.removeClass("disabled progress").prop("disabled", !1), this.$buttonText.removeClass("fadeCounter fadeCounterSlow").text("Get Started"), this.$progressBar.css("width", "0")
    }, s.prototype.disableSubmit = function() {
        function e(e) {
            0 === e ? (t.$button.addClass("disabled progress").prop("disabled", !0), t.$buttonText.addClass("fadeCounter")) : 2 === e && t.$buttonText.removeClass("fadeCounter").addClass("fadeCounterSlow"), t.$buttonText.text(t.buttonTexts[e]), t.$progressBar.css("width", t.progressWidths[e])
        }
        var t = this;
        this.clearAllTimeouts(), e(0), this.pendingTimeouts.push(setTimeout(function() {
            e(1)
        }, 2e3)), this.pendingTimeouts.push(setTimeout(function() {
            e(2)
        }, 4e3))
    }, s.prototype.showNewDemoSuccess = function() {
        var e = this;
        this.$formInner.hide(), this.$successBox.addClass("slideLeft").show(), setTimeout(function() {
            e.$successImage1.addClass("fadeIn")
        }, 500), setTimeout(function() {
            e.$successImage2.addClass("fadeIn")
        }, 750)
    }, s.prototype.showDemoCreationError = function() {
        this.enableSubmit(), this.$errorMessage.html("Sorry, an error occurred and the form failed to send properly. <br> Please try again in a moment.").show(), e.events.raiseTrackingEvent("Demo", "Error", "SignupPage")
    }, s.prototype.showCurrentSubscriber = function() {
        this.$formInner.hide(), this.$currentUserBox.addClass("slideLeft").show()
    }, s.prototype.handleSuccess = function(s) {
        function o() {
            return t.ajax("/api/auto-login", {
                method: "POST",
                data: s.demo
            })
        }

        function r(e) {
            return "READY" === e.status
        }

        function n(e) {
            return "IN_PROGRESS" !== e.status && "READY" !== e.status ? {
                demo: e,
                error: "Unexpected status " + e.status
            } : !1
        }

        function i(e) {
            var s = t("<button>", {
                    html: u.$successButton.html(),
                    "class": u.$successButton.attr("class")
                }).data(u.$successButton.data()),
                o = t("<form>", {
                    action: e.redirectUrl,
                    method: "post",
                    html: s
                }).on("submit", function() {
                    s.prop("disabled", !0)
                });
            u.$successButton.replaceWith(o), u.$successBox.is(":hidden") && u.showNewDemoSuccess()
        }

        function a(t) {
            if (t.timeout) u.showNewDemoSuccess(), e.poll(o, r, n, 1e3, 6e4).then(i).fail(a);
            else {
                if (!t.demo) return void u.showDemoCreationError();
                switch (t.demo.status) {
                    case "USE_EMAIL":
                        u.showNewDemoSuccess();
                        break;
                    default:
                        u.showDemoCreationError()
                }
            }
        }
        var u = this;
        switch (s.demo.status) {
            case "READY":
            case "IN_PROGRESS":
                e.poll(o, r, n, 1e3, 1e4).then(i).fail(a), this.formEvent("success");
                break;
            case "USE_EMAIL":
                this.showNewDemoSuccess(), this.formEvent("success");
                break;
            default:
                this.showDemoCreationError(), this.formEvent("fail")
        }
    }, s.prototype.handleErrors = function() {
        var s = t.grep(this.fieldErrors, function(e) {
            return "email" === e.field && /already registered/.test(e.message)
        });
        s.length ? (this.showCurrentSubscriber(), e.events.raiseTrackingEvent("Demo", "ActiveUser", "SignupPage")) : this.fieldErrors.length > 0 ? e.AjaxFormWrapper.prototype.handleErrors.call(this, arguments) : this.showDemoCreationError()
    }, e.DemoFormWrapper = s, t(function() {
        t("form.demo-form").each(function() {
            var e = t(this),
                s = new window.CrunchSS.DemoFormWrapper(e),
                o = t("#name"),
                r = t("#email"),
                n = t("#phone");
            e.on("submit", function(e) {
                e.preventDefault(), s.validateAndSubmit()
            }), o.placeholder(), r.placeholder(), n.placeholder()
        })
    })
}(window.CrunchSS = window.CrunchSS || {}, jQuery);
! function(t, n) {
    function e(t, n) {
        n = n || {};
        var e = window.CrunchSS.events,
            a = e.categories.Callback,
            c = e.categories.Contact;
        e.raiseTrackingEvent({
            callback: a,
            contact: c,
            chat: c
        }[t], e.actions.Select, n.eventLabel || e.labels.Modal)
    }

    function a(t, n) {
        function e(t, n) {
            return n ? {
                get: function() {
                    return t.data(n)
                },
                set: function(e) {
                    t.data(n, e)
                }
            } : t.is("input") ? {
                get: function() {
                    return t.val()
                },
                set: function(n) {
                    t.val(n)
                }
            } : {
                get: function() {
                    return t.text()
                },
                set: function(n) {
                    t.text(n)
                }
            }
        }

        function a(t, a, c, o) {
            var i = "original" + t + "text",
                s = e(a, o),
                d = s.get();
            c && c !== d && (n.data(i, s.get()), s.set(c)), c || (d = n.data(i), "undefined" != typeof d && s.set(d))
        }
        var c = t.data();
        a("heading", n.find("h2"), c.headingText), a("subHeading", n.find(".js-subheading"), c.subHeadingText), a("successMessage", n.find(".success-message"), c.successMessage), a("button", n.find("#callback_send"), c.buttonText), a("eventLabel", n.find("form"), c.eventLabel, "eventLabel")
    }
    t.attachModals = function() {
        var t = ["callback", "contact", "chat"];
        n.each(t, function(t, c) {
            var o = "show-" + c + "-form-modal",
                i = n("#" + c + "-form-modal"),
                s = n("a." + o);
            s = s.add(n('footer a[href="#0modal=' + c + '"]')), s.on("click", function(t) {
                t.preventDefault(), a(n(this), i), i.modalWindow("show"), e(c, n(this).data())
            }), i.on("success", function() {
                n(this).find(".hide-on-form-success").hide()
            }), window.location.search.indexOf("modal=" + c) > -1 && (i.modalWindow("show"), e(c))
        })
    }, n(document).ready(t.attachModals)
}(window.CrunchSS = window.CrunchSS || {}, jQuery);
! function(e) {
    var i = {
        _getWrapper: function(e) {
            return e.closest(".modal-wrapper")
        },
        _getShell: function(e) {
            return e.closest(".modal-shell")
        },
        _isiOSDevice: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i)
        },
        _animate: function() {
            return e(this).get(0).style.removeAttribute && e(this).get(0).style.removeAttribute("filter"), e(this).toggleClass("ani-end")
        },
        _hasInit: function(e) {
            return 1 === i._getWrapper(e).length
        },
        _init: function(t) {
            var n, a, o = t.data("modaloptions");
            i._isiOSDevice() && t.parents("#container").length > 0 && e("#outer-wrapper").append(t), t.before('<div class="modal-overlay"></div>'), n = t.wrap('<div class="modal-wrapper"><div class="modal-shell"></div></div>').parent(), n.after('<div class="modal-align"></div>'), n.addClass("decoration"), t.addClass("modal-content"), t.css("display", "block"), o.closeButton && i.addCloseButton(n), o.verticalAlign && (n.css("vertical-align", o.verticalAlign), "undefined" != typeof o.verticalMargin && n.css("margin-" + o.verticalAlign, o.verticalMargin)), o.closeOnClickOverlay && window.Modernizr && !window.Modernizr.touch && i.addClickHandler(t), i.lockFocus(t), i._isiOSDevice() && (a = i._getWrapper(t), a.css({
                position: "absolute"
            }), n.css("-webkit-overflow-scrolling", "auto"))
        },
        show: function(t) {
            var n, a;
            i._hasInit(t) || i._init(t), n = i._getWrapper(t), n.prev(".modal-overlay").show(0, i._animate), n.show(0, i._animate), i._isiOSDevice() && (a = i._getShell(t), a.css("z-index", Number(a.parent().css("z-index")) + 1), a.css("-webkit-overflow-scrolling", "touch"), e(window).scrollTop(0))
        },
        hide: function(e) {
            var t = i._getWrapper(e),
                n = i._getShell(e);
            i._animate.call(t.prev(".modal-overlay")).delay(300).hide(1), i._animate.call(t).delay(300).hide(1), i._isiOSDevice() && n.css("-webkit-overflow-scrolling", "auto")
        },
        addCloseButton: function(e) {
            e.prepend('<a class=close-btn button"><span class="icon-Cross-63"></span></a>'), e.find(".close-btn").click(function() {
                i.hide(e)
            })
        },
        addClickHandler: function(t) {
            var n = i._getShell(t)[0];
            i._getWrapper(t).on("click", function(a) {
                0 === e(a.target).closest(n).length && t.is(":visible") && i.hide(t)
            })
        },
        lockFocus: function(i) {
            e(document).on("focusin", function(e) {
                i.is(":visible") && !i.get(0).contains(e.target) && (e.stopPropagation(), i.find("input").filter(":visible:first").focus())
            })
        }
    };
    e.fn.modalWindow = function(t, n) {
        return this.each(function() {
            var a, o = e(this);
            a = "object" == typeof n ? e.extend({}, e.fn.modalWindow.defaults, n) : e.fn.modalWindow.defaults, o.data("modaloptions", a), i[t] && i[t](o)
        })
    }, e.fn.modalWindow.defaults = {
        closeButton: !0,
        verticalAlign: "middle",
        verticalMargin: "0",
        closeOnClickOverlay: !0
    }
}(jQuery);
! function() {
    var e = window._fbq || (window._fbq = []);
    if (!e.loaded) {
        var n = document.createElement("script");
        n.async = !0, n.src = "//connect.facebook.net/en_US/fbds.js";
        var d = document.getElementsByTagName("script")[0];
        d.parentNode.insertBefore(n, d), e.loaded = !0
    }
    e.push(["addPixelId", "969105406443943"])
}(), window._fbq = window._fbq || [], window._fbq.push(["track", "PixelInitialized", {}]);
! function(t) {
    var e = function() {
            var t, e, n = window._ictt || [];
            n.push(["_setIgrp", "873"]), n.push(["_setDgrp", ["12248", ["InfinityNumber12248"]]]), n.push(["_enableGAIntegration", {
                gua: !0,
                ga: !1
            }]), n.push(["_includeExternal", [{
                from: "custRef",
                to: "_setCustomVar"
            }]]), n.push(["_track"]), window._ictt = n, t = document.createElement("script"), t.type = "text/javascript", t.async = !0, t.src = ("https:" === document.location.protocol ? "https://" : "http://") + "assets.infinity-tracking.net/nas/js/nas.v1.min.js", e = document.getElementsByTagName("script")[0], e.parentNode.insertBefore(t, e)
        },
        n = "03333 118 000",
        a = "InfinityNumber12248 lead-gen",
        r = "8:30am - 8:30pm",
        s = "true" === read_cookie("CrunchClient"),
        i = /^\/blog\//.test(location.pathname),
        c = !s && !i;
    s && (n = "03333 118 001", r = "9am - 5:30pm"), i && (n = null), c && e(), t(document).ready(function() {
        var e = t(".telephone-number"),
            s = window.mobilecheck();
        c && e.addClass(a), t(".opening-hours").html(r), e.each(function() {
            var e = t(this),
                a = n || e.text(),
                r = a.replace(/[ \t\r]+/g, "");
            s ? e.html('<a href="tel:' + r + '">' + a + "</a>").addClass("clickable") : e.html(a)
        })
    })
}(jQuery);
$(window).load(function() {
    var e, n = $("#liveagent-settings");
    if (window.liveagent) {
        if (!n.length) throw new Error("Expected to find a #liveagent-settings element");
        e = n.data(), liveagent.setChatWindowHeight(600), liveagent.setChatWindowWidth(400), liveagent.init(e.liveagentUrl, e.liveagentKeyOne, e.liveagentKeyTwo), window._laq || (window._laq = []), window._laq.push(function() {
            var e = document.getElementById("liveagent_online"),
                n = document.getElementById("liveagent_offline"),
                t = document.getElementById("chatStatusOnline"),
                i = document.getElementById("chatStatusOffline");
            e && liveagent.showWhenOnline("573D00000008OIK", e), n && liveagent.showWhenOffline("573D00000008OIK", n), t && liveagent.showWhenOnline("573D00000008OIK", t), i && liveagent.showWhenOffline("573D00000008OIK", i)
        })
    }
});
! function(e, t) {
    function a() {
        return t("#chatStatusOnline").is(":visible")
    }

    function n() {
        C.setItem("storageClosed", !0), g = !0
    }

    function s() {
        d.raiseTrackingEvent(m, o, a() ? r : d.labels.ChatOffline)
    }

    function i() {
        t("#chatHeader").hasClass("is-open") ? (n(), o = d.actions.Minimise, r === d.labels.Timer ? (s(), r = d.labels.Manual) : (r = d.labels.Manual, s()), t("#chatHeader").removeClass("is-open"), t("#chatToggler").addClass("up").removeClass("down"), t("#chatInner").hide()) : (o = d.actions.Open, r === d.labels.Timer ? s() : (r = d.labels.Manual, s()), t("#chatHeader").addClass("is-open"), t("#chatToggler").addClass("down").removeClass("up"), t("#chatInner").show())
    }

    function l(e) {
        for (var t = location.search.substring(1), a = t.split("&"), n = 0; n < a.length; n++) {
            var s = a[n].split("="),
                i = s[0],
                l = s[1];
            if (i === e) return l
        }
        return ""
    }
    var r, o, c, u = 20,
        h = 90,
        g = null,
        d = e.events,
        m = d.categories.Chat,
        v = 0,
        C = {
            data: {},
            setItem: function(e, t) {
                try {
                    sessionStorage.setItem(e, t)
                } catch (a) {
                    this.data[e] = t
                }
            },
            getItem: function(e) {
                var t = sessionStorage.getItem(e);
                return null === t ? e in this.data ? this.data[e] : null : t
            }
        };
    null === C.getItem("storageClosed") ? g = !1 : n(), setInterval(function() {
        c = null === C.getItem("timeTotal") ? 0 : C.getItem("timeTotal"), c = Number(c), c += 2, C.setItem("timeTotal", c)
    }, 2e3), setInterval(function() {
        v = Number(v), v += 2, c > h && v > u && g === !1 && !t("#chatHeader").hasClass("is-open") && a() && (n(), r = d.labels.Timer, i())
    }, 2e3), t("#chatHeader").on("click", function() {
        i()
    });
    var f = l("live_chat");
    "open" === f && i(), t("#liveagentChatButton").click(function(e) {
        e.preventDefault(), liveagent.startChat("573D00000008OIK"), o = d.actions.ChatNow, eventValue = 12, t(this).data("isSignupPage") ? d.raiseTrackingEvent("Chat", "ChatNow", "SignupPage") : s()
    })
}(window.CrunchSS = window.CrunchSS || {}, jQuery);
! function(n, e) {
    function i() {
        return e(".mobile-nav").is(":visible")
    }

    function o() {
        e(".mobile-nav").show(), e(".mobile-nav-bg").show(), e(".mobile-nav-fade").fadeIn(), e("#outer-wrapper").addClass("with-mobile-nav")
    }

    function a() {
        e(".mobile-nav").hide(), e(".mobile-nav-bg").hide(), e(".mobile-nav-fade").fadeOut(), e("#outer-wrapper").removeClass("with-mobile-nav")
    }
    e(document).ready(function() {
        e(".mobile-nav ul.menu > li > a").after('<div class="arrow down small"></div>'), e(".mobile-nav-link, .mobile-nav-bg").on("click", function(n) {
            n.preventDefault(), i() ? a() : o()
        }), e(".mobile-nav ul.menu > li > a").on("click", function(n) {
            n.preventDefault();
            var i = e(".mobile-nav ul.menu > li > a").not(this);
            i.removeClass("current_page_ancestor"), i.siblings("ul.sub-menu").hide(), e(this).siblings("ul.sub-menu").stop(!0, !0).slideToggle("fast", function() {}), e(this).toggleClass("current_page_ancestor")
        }), e(".mobile-nav ul.sub-menu a").on("click", function() {
            a()
        })
    })
}(window.CrunchSS = window.CrunchSS || {}, jQuery);
! function(c) {
    "use strict";
    c(function() {
        var e = c(".cc-cookies"),
            o = "cc_cookie";
        "accepted" !== read_cookie(o) && e.show().on("click", ".cc-cookie-accept", function() {
            return create_cookie(o, "accepted", 365, "/"), e.hide(), !1
        })
    })
}(jQuery);
! function(e, o) {
    function n(e) {
        var o = e.children(".download-or-signup-button");
        if (0 === o.length) return null;
        var n = o.eq(0),
            r = n.data("tracking-cookie");
        return r ? r : null
    }

    function r() {
        var o = u.find(".resource-download.requires-details");
        o.each(function() {
            var o = e(this);
            o.attr("href", o.data("href"))
        })
    }

    function i() {
        s.hide().filter(".existing-user").show()
    }

    function t() {
        s.hide().filter(".new-user").show()
    }

    function a(e) {
        var a;
        u = e, o.resourceDownloads.isInitialized = !0, a = n(u.find(".resource-download.requires-details")), s = u.find(".download-or-signup-button").children(), read_cookie(a) ? (r(), i()) : t()
    }

    function d(e, o) {
        create_cookie(e, o, 30, "/")
    }

    function c(e, o, n) {
        u.find(".thank-you-copy").show(), o.find(".success-message").replaceWith(n), n.click(function() {
            o.show(), e.modalWindow("hide")
        })
    }

    function l(t) {
        var a, l, s = e(this),
            f = n(s);
        if (!f || read_cookie(f)) return !0;
        if (t.preventDefault(), a = u.find(".modal-content.resource-download"), l = a.find(".contact-form"), l.data("event-label", s.closest("[data-event-label-dynamic]").data("event-label-dynamic")), 0 === l.length) throw new Error("Expected the modal to contain a contact form, but could not find one.");
        l.on("success", function(n) {
            var t;
            return d(f, n.Email), i(), r(e("body")), l.formValues = o.AjaxFormWrapper.prototype.urlEncodeFormProperties(l, l.data()), o.cookies.handleFormEvent("success", l.formValues), t = s.clone().removeClass("requires-details").text("Download"), c(a, l, t), !1
        }), a.modalWindow("show")
    }
    var s, u;
    e(document).on("click", ".resource-download.requires-details", l), e(document).ready(function() {
        o.resourceDownloads.isInitialized || a(e("body"))
    }), o.resourceDownloads = {
        initialize: a,
        interceptDownload: l
    }
}(jQuery, window.CrunchSS = window.CrunchSS || {});
! function(n, i) {
    "use strict";

    function t(n, t, e) {
        var o, c = n.find(t);
        c.each(function() {
            o = i(this).find(e), o.css("height", "auto");
            var n = 0;
            o.each(function() {
                var t = parseInt(i(this).outerHeight());
                t > n && (n = t)
            }), o.css("height", n)
        })
    }
    var e = ".tile",
        o = ".tiles",
        c = i(o),
        u = ".feature-package",
        r = ".feature-row",
        h = i(r),
        s = i(document);
    c.length && (t(s, o, e), c.find("img").on("load", function() {
        t(s, o, e)
    }), i(window).on("resize", function() {
        t(s, o, e)
    })), h.length && (t(s, r, u), i(window).on("resize", function() {
        t(s, r, u)
    })), n.setHeights = t
}(window.CrunchSS = window.CrunchSS || {}, jQuery);
! function(e, n) {
    "use strict";

    function o(n) {
        function o() {
            if (s && s.length && "footerAccordion" === s.attr("id")) {
                var n = parseInt(e("#footer").css("min-height")),
                    o = e("#footer > .group").outerHeight(!0);
                o > n ? (e("#container").css("margin-bottom", -Math.abs(o)), e("#footer, .push").css("height", o)) : (e("#container, #footer, .push").attr("style", ""), e("#container, #footer, .push").removeAttr("style"))
            }
        }

        function t(e, n) {
            e.removeClass("expand-arrow").addClass("collapse-arrow"), n.removeClass("collapsed").addClass("expanded").slideDown("fast", function() {
                o()
            })
        }

        function a(e, n, t) {
            e.removeClass("collapse-arrow").addClass("expand-arrow"), n.removeClass("expanded").addClass("collapsed").slideUp("fast", function() {
                t && t === !0 && o()
            })
        }
        var s, r = n.data("accordion-parent"),
            l = n.find(".expand-collapse-arrow"),
            c = n.next(".expandable-content");
        r && (s = n.closest(r)), c.hasClass("collapsed") ? (s && s.length && a(s.find(".collapse-arrow").not(l), s.find(".expandable-content").not(c), !1), t(l, c)) : a(l, c, !0), e(window).on("resize", function() {
            o()
        })
    }
    n.expandCollapse = o, e(function() {
        e("#footer").length && e(".expand-collapse-button").on("click", function(n) {
            n.preventDefault(), o(e(this)), CrunchSS.scrollCheck(e(this).next(".expandable-content").find("ul"))
        })
    })
}(jQuery, window.CrunchSS = window.CrunchSS || {});

function updateViewportDimensions() {
    var t = window,
        e = document,
        i = e.documentElement,
        n = e.getElementsByTagName("body")[0],
        o = t.innerWidth || i.clientWidth || n.clientWidth,
        a = t.innerHeight || i.clientHeight || n.clientHeight;
    return {
        width: o,
        height: a
    }
}

function loadGravatars() {
    viewport = updateViewportDimensions(), viewport.width >= 768 && jQuery(".comment img[data-gravatar]").each(function() {
        jQuery(this).attr("src", jQuery(this).attr("data-gravatar"))
    })
}

function hideTabs(t) {
    t(".tabs-wrapper").length && t(".tabs-wrapper").each(function() {
        t(this).find(".tab").hide(), t(this).find(".tab").first().show(), t(this).find(".tab-buttons a").removeClass("current"), t(this).find(".tab-buttons a").first().addClass("current"), t(this).find(".tab-link").length && (t(this).find(".tab-link a").removeClass("current"), t(this).find(".tab-link a").first().addClass("current"))
    })
}

function returnCurrentBreakpoint() {
    if (window.getComputedStyle) {
        var t = window.getComputedStyle(document.body, ":after").getPropertyValue("content");
        return t = t.replace(/["']/g, "")
    }
}

function lazyLoadImages(t) {
    var e;
    e = t ? t.find(".image-holder.lazy-load:visible") : $(document).find(".image-holder.lazy-load:visible").not(".lazy-load-explicit"), e.each(function() {
        var t = $(this).find("img");
        $.each(t, function() {
            this.src = "" === this.src ? $(this).data("src") : this.src
        })
    })
}

function debounce(t, e, i) {
    var n;
    return function() {
        var o = this,
            a = arguments,
            r = function() {
                n = null, i || t.apply(o, a)
            },
            s = i && !n;
        clearTimeout(n), n = setTimeout(r, e), s && t.apply(o, a)
    }
}
var viewport = updateViewportDimensions(),
    waitForFinalEvent = function() {
        var t = {};
        return function(e, i, n) {
            n || (n = "Don't call this twice without a uniqueId"), t[n] && clearTimeout(t[n]), t[n] = setTimeout(e, i)
        }
    }();
$("body").addClass("true" === read_cookie("CrunchClient") ? "crunch-client" : "new-visitor"), jQuery(document).ready(function(t) {
    function e() {
        t(".header, .header-right").addClass("toggle-nav")
    }

    function i() {
        t(".header, .header-right").removeClass("toggle-nav")
    }

    function n() {
        t(".mobile-footer").addClass("hide")
    }

    function o() {
        t(".mobile-footer").removeClass("hide")
    }
    if (loadGravatars(), hideTabs(t), t(".tab-buttons, .tab-link, .tab-switcher").on("click", "a", function(e) {
            var i, n = t(this),
                o = n.parent(),
                a = n.closest(".tabs-wrapper"),
                r = n.data("tab");
            e.preventDefault(), a.find(".tab-buttons a").removeClass("current"), t("a[href='#" + r + "']").addClass("current"), a.find(".tab").hide(), a.find("#" + r).show(), o.hasClass("tab-link") && (i = o.offset().top - t("header").outerHeight(!0), t(document).scrollTop() > i && t("html, body").animate({
                scrollTop: i
            }))
        }), t(".tooltip-opener").each(function() {
            t(this).qtip({
                content: {
                    text: t(this).next(".tooltip-content")
                },
                position: {
                    my: "bottom right",
                    at: "top center",
                    target: t(this)
                },
                style: {
                    tip: {
                        corner: !0,
                        width: 18,
                        height: 15,
                        offset: 10
                    }
                }
            })
        }), t.fn.isOnScreen = function(e, i) {
            if (!this.length) return !1;
            var n, o, a, r, s, l, d;
            return (null === e || "undefined" == typeof e) && (e = .01), (null === i || "undefined" == typeof i) && (i = .01), n = t(window), o = {
                top: n.scrollTop(),
                left: n.scrollLeft()
            }, o.right = o.left + n.width(), o.bottom = o.top + n.height(), a = this.outerHeight(!0), r = this.outerWidth(), r && a ? (s = this.offset(), s.right = s.left + r, s.bottom = s.top + a, (l = !(o.right < s.left || o.left > s.right || o.bottom < s.top || o.top > s.bottom)) ? (d = {
                top: Math.min(1, (s.bottom - o.top) / a),
                bottom: Math.min(1, (o.bottom - s.top) / a),
                left: Math.min(1, (s.right - o.left) / r),
                right: Math.min(1, (o.right - s.left) / r)
            }, d.left * d.right >= e && d.top * d.bottom >= i) : !1) : !1
        }, window.mobilecheck() && (t("input:not([type=submit]), textarea, select").bind("blur", function() {
            i(), o()
        }).bind("click", function() {
            e(), n()
        }).bind("focus", function() {
            e(), n()
        }), t(window).scroll(function() {
            t("input:not([type=submit]):focus").isOnScreen() === !0 || t("textarea:focus").isOnScreen() === !0 ? (e(), n()) : (i(), o())
        })), t(".side-nav").length) {
        var a = t(".side-nav").outerHeight(!0),
            r = t("header.header").outerHeight(!0),
            s = t(".scroll-to-top-side-button");
        t(window).scroll(function() {
            t(window).scrollTop() > a - r ? s.show() : s.hide()
        }), s.on("click", function(e) {
            e.preventDefault(), s.addClass("fully-hidden"), t("html,body").animate({
                scrollTop: 0
            }, 500, function() {
                s.removeClass("fully-hidden")
            })
        })
    }
    t(".image-holder.lazy-load").length > 0 && (lazyLoadImages(), t(window).on("resize", function() {
        lazyLoadImages()
    }))
});
! function(e, s) {
    function n(e) {
        return e.find(".current-slide").length > 0
    }

    function r(e) {
        var n = e.closest(".slider").find(".slider-panel"),
            r = 0,
            d = 0;
        return n.each(function() {
            r = parseInt(s(this).css("height")), r > d && (d = r)
        }), d + "px"
    }

    function d() {
        return window.Modernizr && !window.Modernizr.csstransitions
    }

    function i() {
        s(".js-drawer-content:not(.js-drawer-open)").children(".slider").css("height", "")
    }

    function l(e) {
        var s = r(e);
        e.css("height", s), i(e)
    }

    function a(e) {
        var n = s(e.target).closest(".slider"),
            r = n.find(".slider-panel.current-slide"),
            i = r.prev(".slider-panel"),
            a = n.find(".slider-nav.prev"),
            t = n.find(".slider-nav.next");
        e.preventDefault(), i.length > 0 && (r.addClass("right-slide"), r.removeClass("current-slide"), r = i.removeClass("left-slide").addClass("current-slide"), t.removeClass("disabled"), i = r.prev(".slider-panel"), i.length > 0 ? i.addClass("left-slide") : a.addClass("disabled"), d() && l(n))
    }

    function t(e) {
        var n = s(e.target).closest(".slider"),
            r = n.find(".slider-panel.current-slide"),
            i = r.next(".slider-panel"),
            a = n.find(".slider-nav.prev"),
            t = n.find(".slider-nav.next");
        e.preventDefault(), i.length > 0 && (r.addClass("left-slide"), r.removeClass("current-slide"), r = i.removeClass("right-slide").addClass("current-slide"), a.removeClass("disabled"), i = r.next(".slider-panel"), i.length > 0 ? i.addClass("right-slide") : t.addClass("disabled"), d() && l(n))
    }

    function c(e) {
        l(e), n(e) || (e.children(".slider-panel").length > 1 && (s('<div class="slider-nav prev"><span href="#" class="slider-arrow icon icon-Large-arrow-left-55"></span></div>').appendTo(e).on("click", a), s('<div class="slider-nav next"><span href="#" class="slider-arrow icon icon-large-arrow-right-54"></span></div>').appendTo(e).on("click", t)), e.children(".slider-panel:eq(0)").addClass("current-slide"), e.children(".slider-panel:not(:eq(0))").addClass("right-slide"), e.find(".slider-nav.prev").addClass("disabled"))
    }
    s(document).ready(function(e) {
        e(".slider").length && e(window).on("resize", function() {
            l(e(".js-drawer-open .slider"))
        })
    }), e.initSlider = c, e.adjustSliderHeight = l
}(window.CrunchSS = window.CrunchSS || {}, jQuery);
! function(i, e) {
    e(document).ready(function(e) {
        function n(i) {
            return i.isOnScreen(.8, .8)
        }

        function t(i, t) {
            n(e(t)) && (i.play(), S = !0)
        }

        function o(i) {
            var n = e(".video-slider-heading"),
                t = e(".video-slider-sub-heading"),
                o = n.text(),
                d = t.text(),
                a = i.data("heading"),
                s = i.data("sub-heading");
            a !== o && a !== d && e(".video-slider").find("header").fadeTo(500, 0, function() {
                n.text(a), t.text(s), e(this).fadeTo(500, 1)
            })
        }

        function d(i) {
            i.hasClass("active") || (e(".video-nav").find("a").removeClass("active"), i.addClass("active"))
        }

        function a(i) {
            i.removeClass("active disabled"), e(".video-slider-link").blur()
        }

        function s(i, e) {
            var n = i.data("placeholder");
            e.attr("src", n)
        }

        function l(i) {
            e(i).siblings(".video-placeholder").fadeOut(600)
        }

        function c(i) {
            e(i).siblings(".video-placeholder").fadeIn(300)
        }

        function r(i) {
            y = setTimeout(function() {
                i.play(), e(".loading-text").show()
            }, 4e3)
        }

        function u(i, n) {
            p = setTimeout(function() {
                e(i).siblings(".loading-overlay").show(), r(n)
            }, 2e3)
        }

        function f() {
            clearTimeout(y), e(".loading-text").hide()
        }

        function v(i) {
            clearTimeout(p), f(), e(i).siblings(".loading-overlay").fadeOut()
        }

        function h(i) {
            e(i).fadeTo(0, 0)
        }

        function w(i) {
            e(i).fadeTo(600, 1)
        }
        if (e(".video-slider").length) {
            var b, m, g, p, y, T = e(window),
                S = !1,
                x = e(".video-slider"),
                C = e(".video-nav").find(".video-slider-link"),
                k = x.find(".video-placeholder"),
                W = x.find(".wistia_embed"),
                E = W.attr("id");
            window.Wistia && W.length && T.load(function() {
                if (window.Wistia.options(E, {
                        playlistLinks: "auto",
                        transitionTime: 2e3
                    }), !/^wistia_\S+$/.test(E)) /*throw new Error("Unexpected id found for wistia embed container")*/;
                b = E.split("wistia_")[1], m = window.Wistia.embed(b), u(W, m), m.ready(function() {
                    S === !1 && T.bind("scroll.videoSlider", function() {
                        S === !0 ? T.unbind("scroll.videoSlider") : t(m, W)
                    }), T.bind("resize", function() {
                        waitForFinalEvent(i.resizeWistiaVideo(m, W))
                    }), C.bind("click", debounce(function() {
                        e(this).hasClass("active") || (f(), u(W, m), c(W), h(W), d(e(this)), o(e(this)), setTimeout(function() {
                            m.play()
                        }, 400))
                    }, 500, !0)), wistiaEmbeds.bind("play", function() {
                        b = m.hashedId(), g = e("a[href=#wistia_" + b + "]"), w(W), d(g), o(g), v(e(W), m), l(e(W))
                    }), wistiaEmbeds.bind("end", function() {
                        h(W), s(g, k), c(e(W)), a(C)
                    }), t(m, W), i.resizeWistiaVideo(m, W), W.bind("click", function() {
                        m.play()
                    }), W.find("video").removeAttr("controls"), a(C), v(e(W))
                })
            })
        }
    })
}(window.CrunchSS = window.CrunchSS || {}, jQuery);
! function(t, o) {
    o(document).ready(function(t) {
        function o() {
            e = t("header.header").outerHeight(!0), c = h[0].getBoundingClientRect().width, p = y.offset().top + y.outerHeight(!0), r = w.outerHeight(!0), l = Math.round(g.position().top) + 1, u = p - e, d = l - e - r, a = l - r, f = e + j
        }

        function s() {
            w.css("position", "relative"), w.css("top", 0), w.css("width", "auto"), k.css("marginTop", 0), w.removeClass("sticky-active")
        }

        function i() {
            o(), t(document).scrollTop() > u ? (t(document).scrollTop() > d ? (w.css("position", "absolute"), w.css("top", a)) : (w.css("position", "fixed"), w.css("top", f), w.addClass("sticky-active")), w.css("width", c), k.css("marginTop", r)) : s()
        }

        function n() {
            "default-screen" !== returnCurrentBreakpoint() ? i() : s()
        }
        if (t(".js-sticky-container").length) {
            var e, c, r, u, d, a, f, p, l, h = t(".js-sticky-container"),
                w = t(".js-sticky-row"),
                y = t(".js-sticky-before"),
                k = t(".js-sticky-body"),
                g = t(".js-sticky-footer"),
                j = 0;
            t(window).scroll(function() {
                n()
            }), t(window).on("resize", function() {
                n()
            }), n()
        }
    })
}(window.CrunchSS = window.CrunchSS || {}, jQuery);
! function(e, i) {
    i.fn.scrollStopped = function(e) {
        i(this).scroll(function() {
            var t = this,
                n = i(t);
            n.data("scrollTimeout") && clearTimeout(n.data("scrollTimeout")), n.data("scrollTimeout", setTimeout(e, 1e3, t))
        })
    }, i.fn.resizeStopped = function(e) {
        i(this).on("resize", function() {
            var t = this,
                n = i(t);
            n.data("resizeTimeout") && clearTimeout(n.data("resizeTimeout")), n.data("resizeTimeout", setTimeout(e, 1e3, t))
        })
    }, i(document).ready(function(i) {
        function t(t) {
            var n, a, s = t,
                c = s.next(".js-drawer-content"),
                l = t.find(".show-hide");
            o = !0, i(".js-drawer-content").not(c).removeClass("js-drawer-open"), i(".active-container").not(s).removeClass("active-container"), s.toggleClass("active-container"), n = i(".active-container .icon"), a = i(".icon-active"), a.removeClass("icon-active"), setTimeout(function() {
                a.addClass("icon-inactive")
            }, 0), n.removeClass("icon-inactive"), setTimeout(function() {
                n.addClass("icon-active")
            }, 0), c.toggleClass("js-drawer-open"), l.length && (l.find("a").toggle(), i(".js-drawer-switch .show-hide").not(l).find("a.hide-content").hide().addBack().find("a.show-content").show()), lazyLoadImages(c), e.initSlider(c.find(".feature-slides.slider")), s.hasClass("js-drawer-switch-open") ? s.removeClass("js-drawer-switch-open") : s.hasClass("active-container") && setTimeout(function() {
                e.scrollCheck(s)
            }, 450)
        }

        function n(i) {
            var t = i,
                n = t.closest(".list-body").find("ul");
            e.scrollCheck(n.closest(".package-heading")), n.is(":visible") ? setTimeout(function() {
                n.slideToggle(500, "linear")
            }, 200) : n.slideToggle(500, "linear"), t.find("a").toggle()
        }
        var o = !1;
        if (i(".pricing-page").length) {
            var a = i(".js-drawer-switch-open");
            i(".js-drawer-switch").on("click", function(e) {
                e.preventDefault(), t(i(this))
            }), i(".list-body .show-hide.toggle-list").on("click", function(e) {
                e.preventDefault(), n(i(this))
            }), a.length && i(window).on("load", function() {
                o || t(a)
            }), i(".show-callback-form-modal").on("click", function() {
                var e = i(this).data("package");
                i('input[name="formId"]').val(e ? "pricing-callback-" + e : "callback-request")
            })
        }
    })
}(window.CrunchSS = window.CrunchSS || {}, jQuery);
! function(e, a) {
    "use strict";

    function l(e) {
        function a() {
            r.hide(), t.removeClass("required"), o.addClass("required"), i.val("contact-form"), c.data("event-category", "Contact")
        }

        function l() {
            r.show(), t.addClass("required"), o.removeClass("required"), i.val("callback-request"), c.data("event-category", "Callback")
        }

        function n() {
            var e = d.filter(":checked"),
                n = e.attr("value");
            "0" === n && a(), "1" === n && l(), d.parent().removeClass("current"), e.parent().addClass("current")
        }
        var c = e.find("form#callbackToggleForm"),
            r = c.find(".field-callback"),
            t = c.find("#callback"),
            i = c.find("input[name=formId]"),
            d = c.find("input[name=shall-we-call]"),
            o = c.find("textarea[name=message]"),
            s = c.find('label[for="shall-we-call-yes"]'),
            u = c.find('label[for="shall-we-call-no"]');
        n(), e.find("input[name=shall-we-call]").on("click", function() {
            n()
        }), e.find(".set-shall-we-call-yes").click(function() {
            s.addClass("current").click(), u.removeClass("current"), l()
        })
    }
    a.toggleForm = {
        initialize: l
    }, e(function() {
        l(e(document))
    })
}(jQuery, window.CrunchSS = window.CrunchSS || {});
! function(i, e) {
    function n(i, n) {
        var t, o = ["table-header-group", "table-footer-group"];
        t = e.inArray(e(n).parent().parent().css("display"), o) > -1 ? e(n).closest(".col-reorder").parent().width() : e(n).parent().width(), i.hasData(function() {
            i.width(t, {
                constrain: !0
            })
        })
    }
    e(document).ready(function(i) {
        window.Wistia && i(".wistia_embed:not(.media-element)").each(function() {
            var e, t, o = this;
            if (!/^wistia_\S+$/.test(o.id)) /*throw new Error("Unexpected id found for wistia embed container")*/;
            e = o.id.split("wistia_")[1], t = window.Wistia.embed(e, {
                container: o.id
            }), n(t, o), i(window).bind("resize", function() {
                waitForFinalEvent(n(t, o))
            }), t.ready(function() {
                i(o).siblings(".video-placeholder").fadeTo(0, 0), i(o).removeClass("video-not-ready")
            })
        })
    }), i.resizeWistiaVideo = n
}(window.CrunchSS = window.CrunchSS || {}, jQuery);
! function(e, t) {
    t(function() {
        var o = t(".blog-feed"),
            n = t(".blog-feed-container");
        o.each(function() {
            e.fetchBlogFeedAndPopulatePlaceholders(o, n)
        })
    }), e.fetchBlogFeedAndPopulatePlaceholders = function(e, o) {
        var n = e.data("blog-category"),
            a = e.data("blog-tags"),
            r = t.get("/blog/api/?category=" + n + "&tags=" + a, "json");
        r.done(function(n) {
            var a = n.data;
            if ("undefined" == typeof a || 0 === a.length) throw o.remove(), new Error("No Blog Data");
            t(".blog-feed-post-inner").fadeIn(), t.each(e.find(".blog-feed-post-fillable"), function(e, o) {
                var n = a[e],
                    r = t(o);
                if (n) {
                    var f = r.find(".blog-feed-description"),
                        i = r.find(".blog-feed-post-featured-content"),
                        d = t(i.length ? i : o),
                        l = r.find(".blog-feed-post-inner").filter(function(e, t) {
                            return -1 === t.className.indexOf("blog-feed-post-featured-image")
                        }),
                        g = function() {
                            return d.innerHeight() - parseFloat(d.css("paddingTop")) - parseFloat(d.css("paddingBottom"))
                        };
                    r.find(".blog-feed-title a").html(n.post_title).attr("href", n.post_url), n.image_url && (r.find(".blog-feed-post-featured-image").wrap('<a href=""></a>').css("background-image", "url(" + n.image_url + ")"), r.find(".blog-feed-post-featured-image-container a").attr("href", n.post_url));
                    var c = function() {
                        var e = n.post_content,
                            t = g();
                        for (f.html(e); l.outerHeight() > t;) {
                            var o = e.lastIndexOf(" ");
                            if (-1 === o) {
                                f.html("");
                                break
                            }
                            e = e.slice(0, o), f.html(e)
                        }
                    };
                    c(), t(window).resize(debounce(c, 10))
                } else r.empty()
            })
        }), r.fail(function() {
            throw o.remove(), new Error("Blog feed fetch failed")
        })
    }
}(window.CrunchSS = window.CrunchSS || {}, jQuery);
! function(e, t) {
    function r(e) {
        return 1 === t(".question:eq(" + parseInt(e) + ")").find('input[type="radio"]:checked').length
    }

    function i() {
        var r = e.ir35Calculator.validateSlide(z);
        r ? (t("#prevSlide").show(), +z !== E - 1 ? (t(".question:eq(" + parseInt(z) + ")").fadeOut("fast"), t(".question:eq(" + parseInt(z + 1) + ")").fadeIn(), z++, e.ir35Calculator.adjustProgress(z)) : (+z === E - 1 && z++, t("#nextSlide").hide(), t("#ir35ProgressContainer").hide(), t("#ir35GetResults").fadeIn(), I.hide())) : e.ir35Calculator.addErrorClass()
    }

    function a() {
        y.removeClass(), e.ir35Calculator.hideResults(), e.ir35Calculator.showCalculatorForm(), z > 0 && (t(".question:eq(" + parseInt(z) + ")").fadeOut("fast"), t(".question:eq(" + parseInt(z - 1) + ")").fadeIn(), z--, e.ir35Calculator.adjustProgress(z)), 0 === z && t("#prevSlide").hide()
    }

    function o() {
        t(".question:eq(" + parseInt(z) + ")").find("label").addClass("error"), setTimeout(function() {
            t(".question:eq(" + parseInt(z) + ")").find("label").removeClass("error")
        }, 500)
    }

    function n() {
        t("#ir35GetResults").hide(), x.hide()
    }

    function s() {
        t(".question input[type='radio']:checked").removeAttr("checked"), t(".question label.active").removeClass("active")
    }

    function l() {
        t("#decisionText").text("")
    }

    function u(e) {
        return e >= F ? (q = B, y.removeClass().addClass("success-alert"), S = "pass") : e >= P ? (q = T, y.removeClass().addClass("information-alert"), S = "warning") : (q = Q, y.removeClass().addClass("error-alert"), S = "fail"), S
    }

    function c() {
        return y.removeClass(), x.hide(), D.hide(), e.ir35Calculator.showCalculatorForm(), e.ir35Calculator.showFirstQuestion(), e.ir35Calculator.resetDecision(), e.ir35Calculator.resetAnswers(), t("#prevSlide").hide(), z = 0, e.ir35Calculator.adjustProgress(z), e.scrollCheck(t("#questionnaireNumber")), e.ir35Calculator.calculateScore()
    }

    function d() {
        var e = 0;
        return t(".question :radio:checked").each(function() {
            e += parseInt(t(this).val())
        }), e
    }

    function C() {
        R.show(), t(".slide-nav").show(), I.show(), A.show()
    }

    function f() {
        t(".slide:not(:first)").hide(), t(".slide:first").show()
    }

    function h() {
        y.append(q), x.fadeIn(), R.hide(), t(".slide-nav, #ir35GetResults").hide(), e.scrollCheck(y)
    }

    function v() {
        return t(".slide").size()
    }

    function p(e) {
        e += 1, E = v(), j = Math.floor(e / E * 100), t("#progressBar").css("width", j + "%"), t("#progressInteger").html(e + "/" + E), t("#questionnaireNumber").html(e + "/" + E)
    }

    function w() {
        b.each(function() {
            t(this).css({
                "z-index": G
            }), G--
        })
    }

    function g() {
        t("input:radio").not(":checked").parent().removeClass("active"), t("input:radio:checked").parent().addClass("active")
    }

    function m() {
        setTimeout(function() {
            i()
        }, 300)
    }
    var S, q, k = t("#crunchIr35Calculator"),
        I = t("#ir35Questions"),
        b = t(".question"),
        y = t("#decisionText"),
        x = t("#ir35Decision"),
        R = t("#ir35ProgressContainer"),
        D = t("#ir35Introduction"),
        A = t("#ir35Form"),
        F = 17,
        P = 1,
        j = 0,
        B = "Based on your answers, you are at low risk of being inside IR35.",
        Q = "Based on your answers, you are at high risk of being <b>inside IR35</b> and we would suggest you seek further advice from an expert.",
        T = "Based on your answers we were unable to determine your IR35 status and we would suggest you seek further advice from an expert.",
        G = 2e3,
        z = 0,
        E = 0;
    e.ir35Calculator = {
        nextSlide: i,
        prevSlide: a,
        addErrorClass: o,
        hideResults: n,
        resetAnswers: s,
        resetDecision: l,
        getDecision: u,
        startCalculator: c,
        calculateScore: d,
        showCalculatorForm: C,
        showFirstQuestion: f,
        showResult: h,
        adjustProgress: p,
        getSlideCount: v,
        amendSlideZindex: w,
        toggleActiveClass: g,
        validateSlide: r,
        delayAnswerSubmit: m
    }, t(document).ready(function() {
        k.length && (e.ir35Calculator.showFirstQuestion(), e.ir35Calculator.amendSlideZindex(), E = e.ir35Calculator.getSlideCount(), e.ir35Calculator.adjustProgress(0), t("#startTest").on("click", function(t) {
            t.preventDefault(), e.ir35Calculator.startCalculator()
        }), t("#restart").on("click", function(t) {
            t.preventDefault(), e.ir35Calculator.startCalculator()
        }), t("#nextSlide").on("click", function() {
            e.ir35Calculator.nextSlide()
        }), t("#prevSlide").on("click", function() {
            e.ir35Calculator.prevSlide()
        }), t("#calculateButton").on("click", function(t) {
            t.preventDefault();
            var r = e.ir35Calculator.calculateScore();
            e.ir35Calculator.getDecision(r), e.ir35Calculator.showResult()
        }), t("input:radio:checked").parent().addClass("active"), t(".question label").on("click", function() {
            e.ir35Calculator.toggleActiveClass()
        }), t(".question label").on("change", function() {
            e.ir35Calculator.delayAnswerSubmit()
        }))
    })
}(window.CrunchSS = window.CrunchSS || {}, jQuery);
! function(t, e) {
    "use strict";

    function o(t, e) {
        var o = "Callback" === e.category && "FormSuccess" === e.action || "Demo" === e.category && "FormSuccess" === e.action || "Chat" === e.category && "ChatNow" === e.action || "Contact" === e.category && "FormSuccess" === e.action && !/^Partner/.test(e.label);
        o && window.optimizely.push(["trackEvent", e.category + e.action])
    }
    window.optimizely = window.optimizely || [], t.optimzelyEvents = {
        raiseOptimizelyEvent: o
    }, e(document).on("trackingEvent", t.optimzelyEvents.raiseOptimizelyEvent)
}(window.CrunchSS = window.CrunchSS || {}, jQuery);
$(function() {
    $("#neardesk_map_placeholder").length && (window.neardeskMapsSettings = window.neardeskMapsSettings || [], window.neardeskMapsSettings.push({
        mapPlaceholder: "#neardesk_map_placeholder",
        searchBox: "#neardesk_map_search",
        useGeoLocation: window.mobilecheck()
    }))
});
! function(e, t) {
    function n(e) {
        function n(e, t, n) {
            e.masonry({
                itemSelector: t,
                isAnimated: !1,
                gutterElement: n,
                trackGutterWidth: !0,
                layoutPriorities: {
                    shelfOrder: 1
                }
            })
        }

        function i(e, t) {
            var n = " ";
            return e && e.length ? e.split(n).shift() : t && t.length ? t.split(n).shift() : ""
        }
        var r, a, s = e.find("#reviews-placeholder"),
            d = e.find("#reviews-wrapper-template").html(),
            o = e.find("#reviews-item-template").html(),
            l = e.find("#reviews-error-template").html(),
            m = e.find("#reviews-star-template").html(),
            c = e.find("#reviews-empty-star-template").html(),
            p = {
                minRating: 5,
                count: 4,
                maxRating: 5
            },
            f = "https://api.reviews.co.uk/merchant/reviews?store=crunch-accounting",
            g = 110;
        if (s.length) {
            r = t.extend(p, s.data()), a = f + "&min_rating=" + encodeURIComponent(r.minRating) + "&per_page=" + encodeURIComponent(r.count) + "&max_rating=" + encodeURIComponent(r.maxRating) + "&order=desc";
            var h = function() {
                s.replaceWith(function() {
                    return t(l).hide().fadeIn()
                })
            };
            t.get(a).then(function(e) {
                if (!e.reviews) return h();
                var r = t(d),
                    a = r.find(".review-items");
                t.each(e.reviews, function(e, n) {
                    var r, s = t(o),
                        d = s.find(".comment"),
                        l = s.find(".comment-long"),
                        p = s.find(".timeago"),
                        f = i(t.trim(n.reviewer.first_name), t.trim(n.reviewer.last_name)),
                        h = "",
                        u = n.comments,
                        v = n.timeago;
                    for (s.find(".author").text(f), u.length > g ? (h = u.substr(0, g) + "&hellip;", t('<a class="change-size-trigger" href="#">Read More</a>').insertAfter(l).on("click", function(e) {
                            e.preventDefault(), d.toggleClass("hidden"), l.toggleClass("hidden"), s.toggleClass("expanded"), t(this).text(s.hasClass("expanded") ? "Show Less" : "Read More"), a.masonry("resize")
                        })) : h = u, d.append(h), l.append(u), v.length && p.append(v), r = 0; r < n.rating; r++) 1 === r && s.find(".rating-value").append(n.rating), s.find(".stars-container").append(t(m));
                    for (r = n.rating; 5 > r; r++) s.find(".stars-container").append(t(c));
                    a.append(s), e % 2 === 0 && a.append('<div class="js-grid-gutter"></div>')
                }), s.replaceWith(function() {
                    return r.hide().fadeIn()
                }), n(a, ".js-grid-item", ".js-grid-gutter")
            }).fail(h)
        }
    }
    var i = t(document);
    n(i), e.reviews = {
        initialize: n
    }
}(window.CrunchSS = window.CrunchSS || {}, jQuery);
! function() {
    function e() {
        for (var e, n = /\+/g, d = /([^&=]+)=?([^&]*)/g, i = function(e) {
                return decodeURIComponent(e.replace(n, " "))
            }, o = window.location.search.substring(1), c = {}; null !== (e = d.exec(o));) c[i(e[1])] = i(e[2]);
        return c
    }
    var n = e(),
        d = "",
        i = "";
    if ("undefined" != typeof n.c2c && (d = n.c2c), "undefined" != typeof n.aid ? d = n.aid : "undefined" != typeof n.a_aid && (d = n.a_aid), "undefined" != typeof n.bid ? i = n.bid : "undefined" != typeof n.a_bid && (i = n.a_bid), d) {
        var o = new Image;
        o.src = src = "https://affiliatenetwork.crunch.co.uk/track/click/" + encodeURIComponent(d) + "/" + encodeURIComponent(i), o.width = 1, o.height = 1, document.body.appendChild(o)
    }
}();
var Konami = function(t) {
    var e = {
        addEvent: function(t, e, o, n) {
            t.addEventListener ? t.addEventListener(e, o, !1) : t.attachEvent && (t["e" + e + o] = o, t[e + o] = function() {
                t["e" + e + o](window.event, n)
            }, t.attachEvent("on" + e, t[e + o]))
        },
        input: "",
        pattern: "38384040373937396665",
        load: function(t) {
            this.addEvent(document, "keydown", function(o, n) {
                return n && (e = n), e.input += o ? o.keyCode : event.keyCode, e.input.length > e.pattern.length && (e.input = e.input.substr(e.input.length - e.pattern.length)), e.input == e.pattern ? (e.code(t), e.input = "", o.preventDefault(), !1) : void 0
            }, this), this.iphone.load(t)
        },
        code: function(t) {
            window.location = t
        },
        iphone: {
            start_x: 0,
            start_y: 0,
            stop_x: 0,
            stop_y: 0,
            tap: !1,
            capture: !1,
            orig_keys: "",
            keys: ["UP", "UP", "DOWN", "DOWN", "LEFT", "RIGHT", "LEFT", "RIGHT", "TAP", "TAP"],
            code: function(t) {
                e.code(t)
            },
            load: function(t) {
                this.orig_keys = this.keys, e.addEvent(document, "touchmove", function(t) {
                    if (1 == t.touches.length && 1 == e.iphone.capture) {
                        var o = t.touches[0];
                        e.iphone.stop_x = o.pageX, e.iphone.stop_y = o.pageY, e.iphone.tap = !1, e.iphone.capture = !1, e.iphone.check_direction()
                    }
                }), e.addEvent(document, "touchend", function(o) {
                    1 == e.iphone.tap && e.iphone.check_direction(t)
                }, !1), e.addEvent(document, "touchstart", function(t) {
                    e.iphone.start_x = t.changedTouches[0].pageX, e.iphone.start_y = t.changedTouches[0].pageY, e.iphone.tap = !0, e.iphone.capture = !0
                })
            },
            check_direction: function(t) {
                x_magnitude = Math.abs(this.start_x - this.stop_x), y_magnitude = Math.abs(this.start_y - this.stop_y), x = this.start_x - this.stop_x < 0 ? "RIGHT" : "LEFT", y = this.start_y - this.stop_y < 0 ? "DOWN" : "UP", result = x_magnitude > y_magnitude ? x : y, result = 1 == this.tap ? "TAP" : result, result == this.keys[0] && (this.keys = this.keys.slice(1, this.keys.length)), 0 == this.keys.length && (this.keys = this.orig_keys, this.code(t))
            }
        }
    };
    return "string" == typeof t && e.load(t), "function" == typeof t && (e.code = t, e.load()), e
};
! function() {
    {
        var t = "position: fixed;bottom: -80px;left:20px;width:50px;height:80px;z-index:100;",
            e = "width:100%;overflow:hidden;background:url(/blog/wp-content/themes/crunchv2a/library/images/robot-astronaut.png) no-repeat 0 0;background-size: 200%;height:80px;",
            o = '<div id="joseRobotContainer" style="' + t + '" title="Hi, i\'m José"><div id="joseRobot" style="' + e + '"></div></div>';
        new Konami(function() {
            $("body").append(o), $("#joseRobotContainer").animate({
                bottom: 20
            }, 2e3, function() {
                $("#joseRobot").css({
                    backgroundPosition: "100% 0"
                }), window.setTimeout(function() {
                    $("#joseRobot").css({
                        backgroundPosition: "0 0"
                    }), $("#joseRobotContainer").animate({
                        bottom: -80
                    }, 2e3, function() {
                        $("#joseRobotContainer").remove()
                    })
                }, 1e3)
            })
        })
    }
}();
//# sourceMappingURL=maps/app-48d75e445a.js.map